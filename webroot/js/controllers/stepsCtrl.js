app.factory("Data", ['$http',
    function ($http) { // This service connects to our REST API

        var obj = {};
        obj.get = function (q) {
            return $http.get(q).then(function (results) {
                return results.data;
            });
        };
        obj.post = function (q, object) {
            return $http.post(q, object).then(function (results) {
                return results.data;
            });
        };
        obj.put = function (q, object) {
            return $http.put(q, object).then(function (results) {
                return results.data;
            });
        };
        obj.delete = function (q) {
            return $http.delete(q).then(function (results) {
                return results.data;
            });
        };

        return obj;
    }]);

app.controller('stepsCtrl', function ($scope, Data, $rootScope) {
    $scope.steps = [
        {
            templateUrl: 'step1',
            hasForm: true,
            title: 'Dane i szczegóły'
        },
        {
            templateUrl: 'step2',
            hasForm: true,
            title: 'Dodawanie zdjęć'
        }
    ];

    $scope.data = {};

    $scope.bindModels = function () {
        var phase = $scope.$root.$$phase;
        if (phase == '$apply' || phase == '$digest') {

        } else {
            $scope.$apply();
        }
    };

    $scope.files = [];

    $scope.previewFiles = function() {

        var files   = document.querySelector('input[type=file]').files;

        function readAndPreview(file) {

            // Make sure `file.name` matches our extensions criteria
            if ( /\.(jpe?g|png|gif)$/i.test(file.name) ) {
                var reader = new FileReader();

                reader.addEventListener("load", function () {
                    file = {name: file.name, link: this.result};
                    $scope.files.push(file);
                    $scope.bindModels();
                }, false);

                reader.readAsDataURL(file);
            }

        }

        if (files) {
            [].forEach.call(files, readAndPreview);
        }
    
    };
    
    $scope.saveproduct = function () {
        Data.post('saveproduct', {data: $scope.data, files: $scope.files}).then(function (response) {
            $scope.categories = response;
        });
    };

    $scope.delete_file = function (index) {
        $scope.files.splice(index, 1);
        $scope.bindModels();
    };

    Data.get('getCategories').then(function (response) {
        $scope.categories = response;
    });

});