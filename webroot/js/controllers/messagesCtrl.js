app.factory("Data", ['$http',
    function ($http) { // This service connects to our REST API

        var obj = {};
        obj.get = function (q) {
            return $http.get(q).then(function (results) {
                return results.data;
            });
        };
        obj.post = function (q, object) {
            return $http.post(q, object).then(function (results) {
                return results.data;
            });
        };
        obj.put = function (q, object) {
            return $http.put(q, object).then(function (results) {
                return results.data;
            });
        };
        obj.delete = function (q) {
            return $http.delete(q).then(function (results) {
                return results.data;
            });
        };

        return obj;
    }]);

app.controller('messagesCtrl', function ($scope, $rootScope, Data, browser) {
    if (browser() == 'chrome') {
        if (document.URL.search('users') != -1) {
            $scope.path = 'users/';
        } else {
            $scope.path = 'users/';
        }
    } else {
        if (document.URL.search('users') != -1) {
            $scope.path = '../users/';
        } else {
            $scope.path = 'users/';
        }
    }

    $(document).ready(function () {
        Data.get($scope.path + 'getMyId').then(function (response) {
            $scope.myId = response;
        });
    });
    

    Data.get($scope.path + 'getCurrentRecipient').then(function (response) {
        if (response != 'null') {
            $scope.setCurrentRecipient(response);
        }
    });

    Data.get($scope.path + 'getWorkers').then(function (response) {
        $scope.contacts_list = response;
    });


    $scope.refresh_current_recipient = function () {
        Data.get($scope.path + 'getNewMessagesCount/' + $scope.current_recipient_id).then(function (response) {
            if (response.new_messages_count != $scope.new_messages_count) {
                Data.get($scope.path + 'getMessages/' + $scope.current_recipient_id).then(function (response) {
                    $scope.new_messages_count = response.new_messages_count;
                    $scope.current_messages = response.messages;
                });
            }
        });
    };

    $('.message-container').click(function () {
        if($scope.new_messages_count > 0) {
            Data.get($scope.path + 'setMessagesReaded/').then(function (response) {
                $scope.new_messages_count = 0;
            });
        }
    });

    $scope.set_recipient_interval = function () {
        $scope.recipient_interval = setInterval(function () {
            $scope.refresh_current_recipient();
        }, 1500);
    };

    $scope.clear_recipient_interval = function () {
        Data.post($scope.path + 'setCurrentRecipient/' + 'null').then(function (response) {
            clearInterval($scope.recipient_interval);
        });
    };

    $scope.setCurrentRecipient = function (id) {
        Data.get($scope.path + 'getMessages/' + id).then(function (response) {
            $scope.current_recipient_id = id;
            $scope.current_recipient = getWorker(id, $scope.contacts_list)[0];
            $scope.current_messages = response.messages;
            $scope.new_messages_count = response.new_messages_count;
            $scope.glued = true;

            $('#message_box').removeClass('direct-chat-contacts-open');
            if ($('#message_box_body').css('display') == 'none') {
                $('#message_box').toggleBox();
            }
            $('#message_box').show();
            if ($("#aside-right").hasClass('control-sidebar-open')) {
                $("#aside-right").removeClass('control-sidebar-open');
            }
            Data.post($scope.path + 'setCurrentRecipient/' + id).then(function (response) {
                $scope.set_recipient_interval();
            });
        });
    };

    $scope.send_message = function () {
        if ($scope.message_content != '') {
            Data.post($scope.path + 'sendMessage/' + $scope.current_recipient.id, {message: $scope.message_content}).then(function (response) {
                if (response == 'success') {
                    Data.get($scope.path + 'getMessages/' + $scope.current_recipient_id).then(function (response) {
                        $scope.current_messages = response.messages;
                        $scope.glued = true;
                    });
                    $scope.message_content = '';
                }
            });
        }
    };


    function getWorker(id, array) {
        return $.grep(array, function (e) {
            return e.id == id;
        });
    }

});