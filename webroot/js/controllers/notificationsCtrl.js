app.factory("Data", ['$http',
    function ($http) { // This service connects to our REST API

        var obj = {};
        obj.get = function (q) {
            return $http.get(q).then(function (results) {
                return results.data;
            });
        };
        obj.post = function (q, object) {
            return $http.post(q, object).then(function (results) {
                return results.data;
            });
        };
        obj.put = function (q, object) {
            return $http.put(q, object).then(function (results) {
                return results.data;
            });
        };
        obj.delete = function (q) {
            return $http.delete(q).then(function (results) {
                return results.data;
            });
        };

        return obj;
    }]);

app.controller('notificationsCtrl', function ($scope, $rootScope, Data, $compile) {
    Data.get('notifications/getGroups').then(function (response) {
        $scope.groups = [];
        for(i=0;i<response.length;i++){
            obj = {};
            obj.value = response[i].id;
            obj.name = response[i].name;
            $scope.groups.push(obj);
        }
    });

    $scope.add_notification = function () {
        Data.post('notifications/add', {notification: $scope.notification}).then(function (response) {
            if (response.status == 'success') {
                show_success();
                var date = new Date();
                var element = $compile("<tr id='"+ response.id +"'>" +
                    "<td>" + response.id + "</td>" +
                    "<td>" + $scope.notification.text + "</td>" +
                    "<td>" + date.getFullYear() + "-" + correct_date(date.getMonth(), "month") + "-" + correct_date(date.getDate(), "") + "</td>" +
                    "<td>" + $scope.notification.group_id.name + "</td>" +
                    "<td>" + response.sender + "</td>" +
                    "<td><span><i class='fa fa-remove' ng-click='remove_notification(" + response.id + ")'></i></span></td>" +
                    "</tr>")($scope);
                $("#notification_list").prepend(element);
            } else {
                show_error();
            }
        });
    };

    $scope.remove_notification = function(id){
        if(confirm("Usunąć to powiadomienie?")){
            Data.post('notifications/delete', {id: id}).then(function (response) {
                if (response.status == 'success') {
                    show_success();
                    $("#"+id).remove();
                } else {
                    show_error();
                }
            });
        }
    };

    function show_success() {
        toastr.success('Wysłano pomyślnie!', '');
    }

    function show_error() {
        toastr.error('Wystąpił błąd w wysyłaniu!', '');
    }

    function correct_date(number, type) {
        if (type == 'month') {
            if (number < 10) {
                return "0" + (number + 1);
            } else {
                return (number + 1);
            }
        } else {
            if (number < 10) {
                return "0" + number;
            } else {
                return number;
            }
        }
    }
});