app.factory("Data", ['$http',
    function ($http) { // This service connects to our REST API

        var obj = {};
        obj.get = function (q) {
            return $http.get(q).then(function (results) {
                return results.data;
            });
        };
        obj.post = function (q, object) {
            return $http.post(q, object).then(function (results) {
                return results.data;
            });
        };
        obj.put = function (q, object) {
            return $http.put(q, object).then(function (results) {
                return results.data;
            });
        };
        obj.delete = function (q) {
            return $http.delete(q).then(function (results) {
                return results.data;
            });
        };

        return obj;
    }]);

app.controller('productsCtrl', function ($scope, $rootScope, Data) {
    $scope.alert = function(text){
        alert(text);
    };

    $scope.show_product = function (id) {
        Data.post('/born2benew/products/getProduct', {id: id}).then(function(response){
            $scope.current_product = response;
            var images = $scope.current_product.images;
            for(i=0;i<images.length;i++){
                $scope.current_product.images[i].link = 'http://localhost/b2b_cake/webroot/'+images[i].link;
            }
            $scope.bindModels();
            $('#product_modal').modal('toggle');
        });
    };

    $scope.save_input = function (id, name, value) {
        Data.post('products/edit', {object: {id: id, name: name, value: value}}).then(function (response) {
            if (response == 'success') {
                $("#" + name).text(value);
                show_success('Zapisano pomyślnie!');
                $("#" + name + "_" + id).text(value);
            } else {
                show_error('Wystąpił błąd w zapisywaniu!');
            }
        });
    };

    $scope.bindModels = function () {
        var phase = $scope.$root.$$phase;
        if (phase == '$apply' || phase == '$digest') {
        } else {
            $scope.$apply();
        }
    };

    $scope.previewFiles = function() {
        var files   = document.querySelector('input[type=file]').files;
        function readAndPreview(file) {
            if ( /\.(jpe?g|png|gif)$/i.test(file.name) ) {
                var reader = new FileReader();
                reader.addEventListener("load", function () {
                    file = {product_id: $scope.current_product['id'], link: this.result};
                    $scope.current_product.images.push(file);
                    $scope.bindModels();
                }, false);
                reader.readAsDataURL(file);
            }
        }
        if (files) {
            [].forEach.call(files, readAndPreview);
        }
    };

    $scope.toDelete = [];
    $scope.current_product = [];

    $scope.saveImg = function() {
        Data.post('/born2benew/products/saveImages', { files: $scope.current_product.images, delete: $scope.toDelete, id: $scope.current_product.id}).then(function (response) {
            $scope.toDelete.length = 0;
            $scope.current_product.images.length = 0;
            for(i=0;i<response.images.length;i++){
                var image = {id: response.images[i].id, link: 'http://localhost/b2b_cake/webroot/'+response.images[i].link, product_id: response.images[i].product_id};
                $scope.current_product.images.push(image);
            }
            //$scope.getTitleImg($scope.current_product.id);
            if(response != null) {
                show_success("Zapisano pomyślnie");
            }
            else {
                show_error("Błąd zapisu")
            }
        });
    };
    /*
    $scope.getTitleImg = function(id) {
        if($scope.current_product.images != undefined) {
            $scope.titleImage = $scope.current_product.images[0].link;
        }
        else {
            Data.post('/born2benew/products/titleImg', {id: id}).then(function (response) {
                $scope.titleImage = 'http://localhost/born2benew/webroot/'+response.link;
            });
        }
    }*/

    $scope.delete_file = function (index, id, link) {
        $scope.current_product.images.splice(index, 1);
        if(id != undefined) {
            var image = {id: id, link: link};
            $scope.toDelete.push(image);
        }
        else {
            
        }
    };

    $(document).ready(function () {
        //alert('test');
    });
    
    function show_success(message) {
        toastr.success(message, '');
    };

    function show_error(message) {
        toastr.error(message, '');
    };
});