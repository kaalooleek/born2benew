app.factory("Data", ['$http',
    function ($http) { // This service connects to our REST API

        var obj = {};
        obj.get = function (q) {
            return $http.get(q).then(function (results) {
                return results.data;
            });
        };
        obj.post = function (q, object) {
            return $http.post(q, object).then(function (results) {
                return results.data;
            });
        };
        obj.put = function (q, object) {
            return $http.put(q, object).then(function (results) {
                return results.data;
            });
        };
        obj.delete = function (q) {
            return $http.delete(q).then(function (results) {
                return results.data;
            });
        };

        return obj;
    }]);

app.controller('ordersCtrl', function ($scope, $rootScope, Data) {
    $scope.orderModal = function(id) {
        Data.post('/born2benew/orders/orderDetails', {id: id}).then(function(response) {
            $scope.current_order = response[0];
            //alert($scope.current_order.last_name);
            $('#orderModal').modal('toggle');
        });
    };
});