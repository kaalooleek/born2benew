app.factory("Data", ['$http',
    function ($http) { // This service connects to our REST API

        var obj = {};
        obj.get = function (q) {
            return $http.get(q).then(function (results) {
                return results.data;
            });
        };
        obj.post = function (q, object) {
            return $http.post(q, object).then(function (results) {
                return results.data;
            });
        };
        obj.put = function (q, object) {
            return $http.put(q, object).then(function (results) {
                return results.data;
            });
        };
        obj.delete = function (q) {
            return $http.delete(q).then(function (results) {
                return results.data;
            });
        };

        return obj;
    }]);

app.controller('textsCtrl', function ($scope, $rootScope, Data) {

    $scope.alert = function(text){
        alert(text);
    };

    $scope.show_text = function (id) {
        Data.get('texts/getText/' + id).then(function (response) {
            $scope.text = response;
            $('#text_modal').modal('toggle');
        });
    }
});