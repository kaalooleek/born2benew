app.directive('focus', function () {
    return function (scope, element) {
        element[0].focus();
    }
});


app.directive('loading', ['$http', function ($http, $rootScope) {
    return {
        restrict: 'A',
        link: function (scope, elm, attrs, $rootScope) {

            scope.isLoading = function () {
                return $http.pendingRequests.length > 0;
            };
            scope.root = "/born2benew/";
            scope.excludedRequests = [scope.root + 'orders/countOrders/1'];
            scope.showLoading = function () {
                if ($.inArray($http.pendingRequests[0].url, scope.excludedRequests) !== -1) {
                    return false;
                } else {
                    return true;
                }
            };


            scope.$watch(scope.isLoading, function (v) {
                if (v && scope.showLoading()) {
                    elm.show();
                } else {
                    elm.hide();
                }
            });
        }
    };

}]);

app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

app.filter('reverse', function () {
    return function (items) {
        if (items != null) {
            return items.slice().reverse();
        }
    };
});

app.directive('repeatEnd', function () {
    return function (scope, element, attrs) {
        if (scope.$last) {
            $("#messages_container").scrollTo(angular.element(element))
        }
    };
});

app.directive('passwordMatch', [function () {
    return {
        restrict: 'A',
        scope: true,
        require: 'ngModel',
        link: function (scope, elem, attrs, control) {
            var checker = function () {

                //get the value of the first password
                var e1 = scope.$eval(attrs.ngModel);

                //get the value of the other password
                var e2 = scope.$eval(attrs.passwordMatch);
                if (e2 != null)
                    return e1 == e2;
            };
            scope.$watch(checker, function (n) {

                //set the form control to valid if both
                //passwords are the same, else invalid
                control.$setValidity("passwordNoMatch", n);
            });
        }
    };
}]);


app.directive("auth", function () {
    return function (scope, element, attrs) {
        function doStuff() {
            if (attrs.auth == "true") {
                element.css("display", "block");
            } else if (attrs.auth == "false") {
                element.css("display", "none");
            } else if (attrs.auth == "true_false") {
                element.css("display", "none");
            } else if (attrs.auth == "false_false") {
                element.css("display", "block");
            } else if (attrs.auth == "false_true") {
                element.css("display", "none");
            }
        }

        scope.$watch(doStuff, function () {
        });
    }
});
app.directive('access', ['$rootScope', function ($rootScope) {
    // 0 - Pracownik
    // 1 - właściciel
    // 2 - Administrator
    return {
        restrict: 'A',
        scope: {
            'access': '=',
            'zatwierdzony': '=',
            'poziom': '='
        },
        link: function (scope, element, attrs) {

            scope.$watch('access', function (access) {
                scope.$watch('zatwierdzony', function (zatwierdzony) {

                    scope.$watch('poziom', function (poziom) {


                        if (zatwierdzony == "1") {
                            if (poziom == "Pracownik") {
                                if (access != null) {
                                    if (access == "1" || access == "2" || access == "0") {
                                        element.css("display", "block");
                                    } else {
                                        element.css("display", "none");
                                    }
                                }
                            } else if (poziom == "Administrator") {
                                if (access != null) {
                                    if (access == "1" || access == "2") {
                                        element.css("display", "block");
                                    } else {
                                        element.css("display", "none");
                                    }
                                }
                            } else if (poziom == "Właściciel") {
                                if (access != null) {
                                    if (access == "1") {
                                        element.css("display", "block");
                                    } else {
                                        element.css("display", "none");
                                    }
                                }
                            }
                        } else {
                            element.css("display", "none");
                        }
                    });
                });
            });
        }
    };
}]);
app.directive("vsbt", function () {
    return function (scope, element, attrs) {
        function doStuff() {
            if (attrs.vsbt == "1") {
                element.css("display", "block");
            } else if (attrs.vsbt == "0") {
                element.css("display", "none");
            }
        }

        scope.$watch(doStuff, function () {
        });
    }
});
app.directive('compile', ['$compile', function ($compile) {
    return function (scope, element, attrs) {
        scope.$watch(
            function (scope) {
                return scope.$eval(attrs.compile);
            },
            function (value) {
                element.html(value);
                $compile(element.contents())(scope);
            }
        )
    };
}]);
app.directive("vsbtrev", function () {
    return function (scope, element, attrs) {
        function doStuff() {
            if (attrs.vsbtrev == "0") {
                element.css("display", "block");
            } else if (attrs.vsbtrev == "1") {
                element.css("display", "none");
            }
        }

        scope.$watch(doStuff, function () {
        });
    }
});
app.directive("bold", function () {
    return function (scope, element, attrs) {
        function bolden() {
            if (attrs.bold == "1") {
                element.css("font-weight", "bold");
            } else if (attrs.bold == "0") {
                element.css("font-weight", "normal");
            }
        }

        scope.$watch(bolden, function () {
        });
    }
});
app.directive("odebrana", function () {
    return function (scope, element, attrs) {
        function bolden() {
            if (attrs.odebrana == "1") {
                element.text("TAK");
            } else if (attrs.odebrana == "0") {
                element.text("NIE");
            }
        }

        scope.$watch(bolden, function () {
        });
    }
});
app.filter('kwota', [
    function () {// should be altered to suit your needs
        return function (input) {
            var ret = (input) ? input.toString().replace(".", ",") : null;
            if (ret) {
                var decArr = ret.split(",");
                if (decArr.length > 1) {
                    var dec = decArr[1].length;
                    if (dec === 1) {
                        ret += "0";
                    }
                }//this is to show prices like 12,20 and not 12,2
            }
            return ret;
        };
    }]);
app.directive("validnip", function () {
    return function (scope, element, attrs, ctrl) {
        function validnip() {
            var nip = attrs.data;
            s = nip.split("");
            var suma = 0;
            suma = (s[0] * 6) + (s[1] * 5) + (s[2] * 7) + (s[3] * 2) + (s[4] * 3) + (s[5] * 4) + (s[6] * 5) + (s[7] * 6) + (s[8] * 7);
            lk = suma % 11;

            if (lk != s[9]) {

            }
        }

        scope.$watch(validnip, function () {
        });
    }
});