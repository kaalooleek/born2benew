app.factory("Data", ['$http',
    function ($http) { // This service connects to our REST API

        var obj = {};
        obj.get = function (q) {
            return $http.get(q).then(function (results) {
                return results.data;
            });
        };
        obj.post = function (q, object) {
            return $http.post(q, object).then(function (results) {
                return results.data;
            });
        };
        obj.put = function (q, object) {
            return $http.put(q, object).then(function (results) {
                return results.data;
            });
        };
        obj.delete = function (q) {
            return $http.delete(q).then(function (results) {
                return results.data;
            });
        };

        return obj;
    }]);

app.controller('fakturyCtrl', function ($scope, $rootScope, Data) {
    $scope.towary = [];
    $scope.rabat = {kwotowy: '', procentowy: ''};
    $scope.rabat.kwotowy = 'kwotowy';
    $scope.rabat.procentowy = 'procentowy';
    dostepne_towary = [];
    $scope.counter = 0;


    $scope.dodaj_zero = function (liczba) {
        if (liczba < 10) {
            return "0" + liczba;
        } else {
            return liczba;
        }
    }

    var d = new Date();
    mm = $scope.dodaj_zero(d.getMonth() + 1);
    yyyy = d.getFullYear();
    dd = $scope.dodaj_zero(d.getUTCDate());



    $scope.faktura = {data_wystawienia: yyyy + "-" + mm + "-" + dd, miejsce_wystawienia: 'Siedlce', waluta: 'PLN', rodzaj_rabatu: '%', suma_netto: '0.00', suma_vat: '0.00', suma_brutto: '0.00', zaliczka_otrzymana: '0.00'};

    $scope.faktura.nazwa_sprzedawcy = "Killersites Artur Bogucki";
    $scope.faktura.adres_sprzedawcy = "Starowiejska 271";
    $scope.faktura.kod_pocztowy_sprzedawcy = "08-110 Siedlce";
    $scope.faktura.nip_sprzedawcy = "NIP: 821 253 83 69";
    $scope.faktura.email_sprzedawcy = "artur.j.bogucki@gmail.com";
    $scope.faktura.telefon_sprzedawcy = "+48 22 113 70 70";
    $scope.faktura.fax_sprzedawcy = "";

    $scope.add_product = function () {
        $scope.counter++;
        licznik = $scope.counter;
        template = {lp: $scope.counter, nazwa: '', jm: '', ilosc: '', rabat: '', podatek_vat: '', wartosc_vat: '0', wartosc_netto: '0', wartosc_brutto: '0', cena_netto: ''};
        $scope.towary.push(template);

    };

    $(document).ready(function () {
        $(function () {
            $(".datepicker").datepicker();
            $.datepicker.regional['pl'] = {
                closeText: 'Zamknij',
                prevText: 'Poprzedni',
                nextText: 'Następny',
                currentText: 'Dzień',
                monthNames: ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec',
                    'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'],
                monthNamesShort: ['Sty', 'Lu', 'Mar', 'Kw', 'Maj', 'Cze',
                    'Lip', 'Sie', 'Wrz', 'Pa', 'Lis', 'Gru'],
                dayNames: ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota'],
                dayNamesShort: ['Nie', 'Pn', 'Wt', 'Śr', 'Czw', 'Pt', 'So'],
                dayNamesMin: ['N', 'Pn', 'Wt', 'Śr', 'Cz', 'Pt', 'So'],
                weekHeader: 'Tydz',
                dateFormat: 'yy-mm-dd',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''};
            $.datepicker.setDefaults($.datepicker.regional['pl']);

        });
        $("#invoice_search").on('input', function () {
            Data.post('invoices/getinvoices', {search: {query: $(this).val()}}).then(function (results) {
                setTimeout(function () {
                    $("#invoice_search").autocomplete({
                        source: results,
                        create: function (event, ul) {
                            $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
                                ul.click(function (e) {
                                    e.stopPropagation();
                                });
                                return $('<li class="search_result">')
                                        .append('<div style="overflow-x:auto;"><table class="table"><thead><tr><th>Typ</th><th>Numer</th><th>Nazwa</th><th>NIP</th><th>Data sprzed.</th><th>Data wystaw.</th><th>Typ płatn.</th><th>Korekta</th><th>Status</th><th>Akcje</td></tr></thead><tbody><tr><td>' + item.type + '</td><td>' + item.numer + '</td><td>' + item.name + '</td><td>' + item.nip + '</td><td>' + item.sell_date + '</td><td>' + item.invoice_date + '</td><td>' + item.payment_type + '</td><td>' + item.correction + '</td><td>' + item.status + '</td><td><div class="btn-group-vertical">\n\
                                                <button type="button" ng-click="show_invoice('+item.id+')" class="btn btn-sm btn-primary">Zobacz</button>')
                                        .appendTo(ul);
                            };
                        },
                        focus: function (event, ui) {
                            return false;
                        },
                        select: function (event, ui) {
                            return false;
                        },
                        close: function (event, ui) {

                            return false;
                        }
                    });
                }, 1);
            });
        });
        $('#' + ($scope.counter)).autocomplete({
            source: dostepne_towary,
            select: function (event, ui) {
                id = parseFloat($(this).attr('name')) - 1;
                Data.post('danetowaru', {towar: {id_towaru: ui.item.value}}).then(function (results) {
                    $scope.towary[id].nazwa = results.nazwa;
                    $scope.towary[id].pkwiu = results.pkwiu;
                    $scope.towary[id].jm = results.jm;
                    $scope.towary[id].cena_netto = results.cena_netto;
                    $scope.towary[id].podatek_vat = results.stawka_vat;
                });
            }
        });
    });

    $scope.add_product();

    $scope.remove_row = function (id)
    {
        index = id - 1;
        for (x = $scope.counter - 1; x > index; x--)
        {
            $scope.towary[x].lp--;
        }
        $scope.towary.splice((index), 1);
        $scope.counter--;
    };

    $rootScope.$watch(function (rootScope) {
        return rootScope.id_wlasciciela
    },
            function (newValue, oldValue) {
                if (newValue != null)
                {
                    $scope.getfirma();
                    $scope.lista_klientow();
                }
            }
    );

    $scope.$watch('towary', function (newValue, oldValue) {
        $scope.faktura.suma_netto = 0;
        for (x = 0; x < $scope.counter; x++) {
            liczba = parseFloat(newValue[x].wartosc_netto);
            $scope.faktura.suma_netto += parseFloat(liczba)
        }
    }
    , true);
    $scope.$watch('towary', function (newValue, oldValue) {
        $scope.faktura.suma_vat = 0;
        for (x = 0; x < $scope.counter; x++) {
            liczba = parseFloat(newValue[x].wartosc_vat);
            $scope.faktura.suma_vat += parseFloat(liczba)
        }
    }
    , true);
    $scope.$watch('towary', function (newValue, oldValue) {
        $scope.faktura.suma_brutto = 0;
        for (x = 0; x < $scope.counter; x++) {
            liczba = parseFloat(newValue[x].wartosc_brutto);
            $scope.faktura.suma_brutto += parseFloat(liczba)
        }
    }
    , true);
    $scope.wystaw = function ()
    {
        for (x = 0; x < $scope.counter; x++)
        {
            $scope.towary[x].wartosc_vat = $scope.towary[x].wartosc_vat.toFixed(2);
            $scope.towary[x].wartosc_netto = $scope.towary[x].wartosc_netto.toFixed(2);
            $scope.towary[x].wartosc_brutto = $scope.towary[x].wartosc_brutto.toFixed(2);
        }
        $scope.faktura.suma_netto = $scope.faktura.suma_netto.toFixed(2);
        $scope.faktura.suma_brutto = $scope.faktura.suma_brutto.toFixed(2);
        $scope.faktura.suma_vat = $scope.faktura.suma_vat.toFixed(2);
        $scope.faktura.do_zaplaty = $scope.faktura.do_zaplaty.toFixed(2);
        $scope.faktura.id_sprzedawcy = $rootScope.id_wlasciciela;
        $scope.faktura.id_firmy = $rootScope.id_firmy;
        Data.post('add', {towary: $scope.towary, faktura: $scope.faktura}).then(function (filename) {

        });
    };

    $scope.save_input = function (id, name, value)
    {
        Data.post('invoices/view', {object: {id: id, name: name, value: value}}).then(function (response) {
            if (response == 'success') {
                $("#" + name).text(value);
                show_success();
                $('#'+name+"_"+id).text(value);
            } else {
                show_error();
            }
        });
    };

    $scope.redirect = function(action, id){
        alert(action);
        alert(id);
    };

    $scope.show_invoice = function (id) {
        Data.get('invoices/getInvoice/'+id).then(function(response){
            $scope.invoice = response;
            $scope.current_invoice = response;
            $('#invoice_modal').modal('toggle');
        });
    };
    
    $scope.generate_pdf = function (id) {
        Data.post('invoices/pdfInvoice/'+id, {faktura: $scope.current_invoice}).then(function(response) {
            window.open(response);
        });
    };
});