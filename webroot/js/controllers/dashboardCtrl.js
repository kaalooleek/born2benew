app.factory("Data", ['$http',
    function ($http) { // This service connects to our REST API

        var obj = {};
        obj.get = function (q) {
            return $http.get(q).then(function (results) {
                return results.data;
            });
        };
        obj.post = function (q, object) {
            return $http.post(q, object).then(function (results) {
                return results.data;
            });
        };
        obj.put = function (q, object) {
            return $http.put(q, object).then(function (results) {
                return results.data;
            });
        };
        obj.delete = function (q) {
            return $http.delete(q).then(function (results) {
                return results.data;
            });
        };

        return obj;
    }]);

app.controller('dashboardCtrl', function ($scope, $rootScope, Data, $route, browser) {
    $('.panel-primary>.panel-heading').css('background', $('.skin-blue .main-header .navbar').css('background-color'));
    $scope.setSkin = function () {
        Data.post('/born2benew/users/getUserColor').then(function (response) {
            var color = response;
            var name = 'skin-' + color;
            var classList = document.body.className.split(/\s+/);
            for (var i = 0; i < classList.length; i++) {
                if (classList[i].match(/skin-/)) {
                    $('body').removeClass(classList[i]);
                    $('body').addClass(name);
                    $('.panel-heading').css('background', $('.' + name + ' .main-header .navbar').css('background-color'));
                    $('.btn-primary').css('background', $('.' + name + ' .main-header .navbar').css('background-color'));
                    $('.myMessage .direct-chat-text').css('background', $('.' + name + ' .main-header .navbar').css('background-color'));
                    $('.myMessage .direct-chat-text').css('border-color', $('.' + name + ' .main-header .navbar').css('background-color'));
                    $('.direct-chat-text:after').css('border-color', $('.' + name + ' .main-header .navbar').css('background-color'));
                    $('.direct-chat-text:before').css('border-color', $('.' + name + ' .main-header .navbar').css('background-color'));
                    $('.box.box-primary').css('border-top-color', $('.' + name + ' .main-header .navbar').css('background-color'));
                    $('.bg-light-blue').css('background-color', $('.' + name + ' .main-header .navbar').css('background-color') + ' !important');
                    $('.bg-aqua-active, .modal-info .modal-header, .modal-info .modal-footer').css('background-color', $('.' + name + ' .main-header .navbar').css('background-color') + ' !important');
                    $('.collapsed').css('color', $('.' + name + ' .main-header .navbar').css('background-color') + ' !important');
                }
            }
        });

    };

    $scope.countListeners = [];

    $scope.addCountListener = function(section_id){
        $scope.refreshCount(section_id);
        listener = window.setInterval(function(){ $scope.refreshCount(section_id)}, 5000);
        $(".section_link_"+section_id).append('&nbsp; <span class="label label-success" id="section_badge_'+section_id+'"></span>');
        $scope.countListeners.push(listener);
    };

    $scope.refreshCount = function (section_id) {
        Data.get('/born2benew/orders/countOrders/'+section_id).then(function (response) {
            $("#section_badge_"+section_id).html(response);
        });
    };

    $scope.saveSkin = function (name, id) {
        Data.post('/born2benew/users/skinColor', {skin: name, id: id}).then(function (response) {
            $scope.setSkin(name);
        });

    }

    $scope.saveSidebar = function (type, id) {
        Data.post('/born2benew/users/sidebarPosition', {sidebar: type, id: id});
    }

    $scope.setSidebarCollapse = function (id) {
        $('body').removeClass('sidebar-mini');
        $('body').addClass('sidebar-collapse');
        $scope.saveSidebar('sidebar-collapse', id);
    }

    /*
    *    _________________________________
    *   |                                 |      ___
    *   |              Śp.                |     |   |
    *   |          Pasek Chowak           |     |   |
    *   |          2016 - 2016            |_____|   |_____
    *   |         Wielki człowiek,        |_____     _____|
    *   |        patriota, dzielnie       |     |   |
    *   |        walczył o pokój na       |     |   |
    *   |         świecie                 |     |   |
    *   |                                 |     |   |
    *   |         Pokój jego duszy        |     |   |
    *   |                                 |     |   |
    *   |                                 |     |   |
    *   |_________________________________|_____|__ |____[*]__
    *
    * */

    $scope.setSidebarMini = function (id) {
        $('body').removeClass('sidebar-collapse');
        $('body').addClass('sidebar-mini');
        $('body').addClass('sidebar-collapse');
        $scope.saveSidebar('sidebar-mini sidebar-collapse', id);

    }

    $scope.setSidebarNormal = function (id) {
        $('body').removeClass('sidebar-collapse');
        $('body').removeClass('sidebar-mini');
        $scope.saveSidebar('', id);
    }

    $scope.setSizeFixed = function () {
        $('body').removeClass('layout-boxed');
        $('body').addClass('fixed');
    }

    $scope.setSizeBoxed = function () {
        $('body').removeClass('fixed');
        $('body').addClass('layout-boxed');
    }

    /*$scope.showTasks = function () {
        Data.post('users/showTask').then(function (response) {
            $scope.tasks = response;
            if ($scope.tasks.length == 0) {
                $scope.t_number = "";
                $scope.t_message = "Nie masz zadań";
            }
            else if ($scope.tasks.length < 5) {
                $scope.t_number = $scope.tasks.length;
                $scope.t_message = "Masz " + $scope.t_number + " zadania";
            }
            else {
                $scope.t_number = $scope.tasks.length;
                $scope.t_message = "Masz " + $scope.t_number + " zadań";
            }
        });
    }
    $scope.timeDiff = function (date) {

    }
    $scope.taskModal = function (id) {
        Data.post('users/taskDetails', {id: id}).then(function (response) {
            $scope.currentTask = response[0];
            $('#task_modal').modal('toggle');
        });
    }
    $scope.doTask = function (id) {
        Data.post('users/doTask', {task_id: id}).then(function (response) {

        });
    }
    $scope.showNotifications = function () {
        Data.post('users/showNotifications').then(function (response) {
            $scope.notifications = response;
            var length = 0;
            angular.forEach($scope.notifications, function (value, key) {
                if (value.status == 0)
                    length++;
            });
            if (length == 0) {
                $scope.n_number = "";
                $scope.n_message = "Nie masz nowych powiadomień";
            }
            else if (length < 5) {
                $scope.n_number = length;
                $scope.n_message = "Masz " + $scope.n_number + " powiadomienia";
            }
            else {
                $scope.n_number = length;
                $scope.n_message = "Masz " + $scope.n_number + " powiadomień";
            }
            angular.forEach($scope.notifications, function (value, key) {
                if (value.status == 0)
                    value.status = '#E9E9E9';
                else if (value.status == 1)
                    value.status = '';
            });
        });
    };
    $scope.notificationModal = function (id) {
        Data.post('users/notificationDetails', {id: id}).then(function (response) {
            $scope.currentNotification = response[0];
            $('#notification_modal').modal('toggle');
        });
    };
     */
    $(document).ready(function () {
        //$scope.showTasks();
        //$scope.showNotifications();
        $scope.setSkin();
    });
    

});