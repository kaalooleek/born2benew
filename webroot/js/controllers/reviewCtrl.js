app.factory("Data", ['$http',
    function ($http) { // This service connects to our REST API

        var obj = {};
        obj.get = function (q) {
            return $http.get(q).then(function (results) {
                return results.data;
            });
        };
        obj.post = function (q, object) {
            return $http.post(q, object).then(function (results) {
                return results.data;
            });
        };
        obj.put = function (q, object) {
            return $http.put(q, object).then(function (results) {
                return results.data;
            });
        };
        obj.delete = function (q) {
            return $http.delete(q).then(function (results) {
                return results.data;
            });
        };

        return obj;
    }]);
app.controller('reviewCtrl', function ($scope, $rootScope, Data) {
    $scope.review = {
        name: 'test',
        title: 'test',
        photo: 'http://img38.otomoto.pl/images_otomotopl/832722377_1_732x488_rs620caligwintxenonledjedyna-w-polsce-leszno_rev008.jpg',
        rating: 10,
        rating_comfort: 10,
        rating_style: 10,
        rating_sizing: 10,
        sizing: 'good'
    };
    $scope.counter = 0;
    $scope.rating = [
        {"name": "5/5", "value": "5", "select": ' ng-selected="true"',},
        {"name": "4/5", "value": "4", "select": " ",},
        {"name": "3/5", "value": "3", "select": " ",},
        {"name": "2/5", "value": "2", "select": " ",},
        {"name": "1/5", "value": "1", "select": " ",},
    ];
    $scope.status = [
        {"name": "Aktywny", "value": "active", "select": ' ng-selected="true"',},
        {"name": "Nie aktywny", "value": "inactive", "select": " ",},
    ];
    $scope.load_next = function () {
        if ($scope.counter == 0) {
            $scope.counter = document.getElementById("review_id").value;
        }
        $scope.counter++;
        Data.post('' + $scope.counter + "/" + 'next').then(function (response) {
            $scope.counter = response.review.id;
            $scope.review = response;
        });
        $scope.animate();
    };

    $scope.load_previous = function () {
        if ($scope.counter == 0) {
            $scope.counter = document.getElementById("review_id").value;
        }
        if ($scope.counter > 1) {
            $scope.counter--;
            Data.post('' + $scope.counter + "/" + 'previous').then(function (response) {
                $scope.counter = response.review.id;
                $scope.review = response;
            });
        }
        $scope.animate();
    };
    $.fn.extend({
        animateCss: function (animationName) {
            var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            $(this).addClass('animated ' + animationName).one(animationEnd, function () {
                $(this).removeClass('animated ' + animationName);
            });
        }
    });
    $scope.animate = function () {
        $('#anim_container').fadeOut(250, function () {
            $(this).fadeIn(250);
        });
    };
    $scope.save_input = function (id, name, value) {
        Data.post('reviews/edit', {object: {id: id, name: name, value: value}}).then(function (response) {
            if (response == 'success') {
                $("#" + name).text(value);
                show_success('Zapisano pomyślnie!');
                $("#" + name + "_" + id).text(value);
            } else {
                show_error('Wystąpił błąd w zapisywaniu!');
            }
        });
    };
    $scope.delete_review = function (id) {
        if (confirm('Czy na pewno chcesz usunąć ocenę ' + id + '?')) {
            Data.post("reviews/delete/" + id).then(function (response) {
                if (response == 'success') {
                    show_success('Usunięto pomyślnie!');
                } else {
                    show_error('Wystąpił błąd przy usuwaniu!');
                }
            });
        }
    };

    $scope.show_review = function (id) {
        Data.get("reviews/getReview/" + id).then(function (response) {
            $scope.review = response;
            $('#review_modal').modal('toggle');
            $('#review_modal').on('show.bs.modal', function (e) {
                $scope.bindModels();
            });
        });
    };
    
    $scope.getProducts = function() {
        Data.get('getProducts').then(function (response) {
            $scope.products = response;
            $scope.data = {
                ids: []
            };
        });
    }
    
    $scope.create = function () {
        Data.post('/born2benew/reviews/createReview', {ids: $scope.data.ids, review: $scope.data.review, name: $scope.data.name,
            city: $scope.data.city, rating: $scope.data.rating, comfort: $scope.data.rating_comfort,
            style: $scope.data.rating_style, sizing: $scope.data.rating_sizing}).then(function(response) {
            if(response == 'success') {
                show_success('Dodano pomyślnie!');
                //$scope.reset();
            }
            else {
                show_error('Wystąpił błąd przy dodawaniu!');
            }
        });
    }

    $scope.reset = function() {
        $scope.products = angular.copy($scope.master);
    };

    function show_success(message) {
        toastr.success(message, '');
    }

    function show_error(message) {
        toastr.error(message, '');
    }
    
    $(document).ready(function () {
        $scope.getProducts();
    });

    $scope.bindModels = function () {
        var phase = $scope.$root.$$phase;
        if (phase == '$apply' || phase == '$digest') {

        } else {
            $scope.$apply();
        }
    };
});