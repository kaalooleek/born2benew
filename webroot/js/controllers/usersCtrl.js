/* global toastr */

app.factory("Data", ['$http',
    function ($http) { // This service connects to our REST API

        var obj = {};
        obj.get = function (q) {
            return $http.get(q).then(function (results) {
                return results.data;
            });
        };
        obj.post = function (q, object) {
            return $http.post(q, object).then(function (results) {
                return results.data;
            });
        };
        obj.put = function (q, object) {
            return $http.put(q, object).then(function (results) {
                return results.data;
            });
        };
        obj.delete = function (q) {
            return $http.delete(q).then(function (results) {
                return results.data;
            });
        };

        return obj;
    }]);

app.controller('usersCtrl', function ($scope, $rootScope, Data) {
    Data.get('getGroups').then(function (response) {
        $scope.groups = response;
    });
    
    $scope.dodaj = function () {
        Data.post('add', {user: $scope.user, groups_obj: $scope.groups_obj}).then(function (response) {
            if (response.success) {
                show_success();
                $scope.user_new = {};
                $scope.user_new.email = response.email;
                $scope.user_new.password = response.tmp_pass;
                $("#new_user_modal").modal('show', {
                    keyboard: false,
                    backdrop: 'static'
                });
            } else {
                show_error(response);
            }
        });
    };

    $scope.send_mail = function () {
        Data.post('send_mail', {email: $scope.user_new.email, password: $scope.user_new.password}).then(function (response) {
            if(response == 'success') {
                toastr.success('Wysłano pomyślnie!');
            } else {
                toastr.error('Wystąpił błąd w wysyłaniu!');
            }
        });
    };

    $scope.setAccess = function (uid, id, action) {
        Data.post('users/setAccess', {uid: uid, access_id: id, action: action}).then(function (response) {
            if(response == 'success') {
                Data.post('users/get_user', {id: uid}).then(function (response) {
                    $scope.user = response;
                    toastr.success('Zapisano pomyślnie!');
                });
            }
            else {
                toastr.error('Wystąpił błąd!');
            }
        });
    };

    $scope.setGroup = function (uid, id, action) {
        Data.post('users/setGroup', {uid: uid, group_id: id, action: action}).then(function (response) {
            if(response == 'success') {
                Data.post('users/get_user', {id: uid}).then(function (response) {
                    $scope.user = response;
                    toastr.success('Zapisano pomyślnie!');
                });
            } else {
                toastr.error('Wystąpił błąd!');
            }
        });
    };

    $scope.hasAccess = function (obj, list) {
        var i;
        for (i = 0; i < list.length; i++) {
            if (list[i]['controller'] === obj['controller'] && list[i]['action'] === obj['action']) {
                return true;
            }
        }

        return false;
    };

    $scope.hasGroup = function (obj, list) {
        var i;
        for (i = 0; i < list.length; i++) {
            if (list[i]['id'] === obj['id']) {
                return true;
            }
        }

        return false;
    };

    $scope.show_user = function (id) {
        Data.post('users/get_user', {id: id}).then(function (response) {
            $scope.user = response;
            $("#user_modal").modal('toggle');
        });
    };

    function show_success() {
        toastr.success('Dodano pomyślnie!', '');
    }
    function show_error(message) {
        toastr.error(message, '');
    }
});