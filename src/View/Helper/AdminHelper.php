<?php

namespace App\View\Helper;

use Cake\View\Helper;

class AdminHelper extends Helper {

    public $helpers = ['Html'];

    public function getColorName($id) {
        $colors = array(
            1 => __('miętowy'),
            2 => __('zielony'),
            3 => __('różowy'),
            4 => __('fuksja'),
            5 => __('fioletowy'),
            6 => __('koralowy'),
            7 => __('czerwony'),
            8 => __('pomarańcz'),
            9 => __('żółty'),
            10 => __('biały'),
            11 => __('czarny'),
            12 => __('szary'),
            13 => __('beżowy'),
            14 => __('brązowy'),
            15 => __('bordowy'),
            16 => __('niebieski'),
            17 => __('srebrny'),
            18 => __('złoty'),
            19 => __('kobaltowy'),
            20 => __('łososiowy'),
            21 => __('limonka'),
            22 => __('khaki'),
            23 => __('jasny brąz'),
            24 => __('panterka'),
            25 => __('moro'),
            26 => __('granatowy'),
            27 => __('camel')
        );
        return($colors[$id]);
    }

    public function getColorFilename($name) {
        $colors = array(
            __('miętowy') => array('img' => 1),
            __('zielony') => array('img' => 2),
            __('różowy') => array('img' => 3),
            __('fuksja') => array('img' => 4),
            __('fioletowy') => array('img' => 5),
            __('koralowy') => array('img' => 6),
            __('czerwony') => array('img' => 7),
            __('pomarańcz') => array('img' => 8),
            __('żółty') => array('img' => 9),
            __('biały') => array('img' => 10),
            __('czarny') => array('img' => 11),
            __('szary') => array('img' => 12),
            __('beżowy') => array('img' => 13),
            __('brązowy') => array('img' => 14),
            __('bordowy') => array('img' => 15),
            __('niebieski') => array('img' => 16),
            __('srebrny') => array('img' => 17),
            __('złoty') => array('img' => 18),
            __('kobaltowy') => array('img' => 19),
            __('łososiowy') => array('img' => 20),
            __('limonka') => array('img' => 21),
            __('khaki') => array('img' => 22),
            __('jasny brąz') => array('img' => 23),
            __('panterka') => array('img' => 24),
            __('moro') => array('img' => 25),
            __('granatowy') => array('img' => 26),
            __('camel') => array('img' => 27)
        );
        if (empty($colors[$name]['img']))
            return false;
        return $colors[$name]['img'] . '.PNG';
    }

    public function getColors() {
        return array(
            __('miętowy') => array('img' => 1),
            __('zielony') => array('img' => 2),
            __('różowy') => array('img' => 3),
            __('fuksja') => array('img' => 4),
            __('fioletowy') => array('img' => 5),
            __('koralowy') => array('img' => 6),
            __('czerwony') => array('img' => 7),
            __('pomarańcz') => array('img' => 8),
            __('żółty') => array('img' => 9),
            __('biały') => array('img' => 10),
            __('czarny') => array('img' => 11),
            __('szary') => array('img' => 12),
            __('beżowy') => array('img' => 13),
            __('brązowy') => array('img' => 14),
            __('bordowy') => array('img' => 15),
            __('niebieski') => array('img' => 16),
            __('srebrny') => array('img' => 17),
            __('złoty') => array('img' => 18),
            __('kobaltowy') => array('img' => 19),
            __('łososiowy') => array('img' => 20),
            __('limonka') => array('img' => 21),
            __('khaki') => array('img' => 22),
            __('jasny brąz') => array('img' => 23),
            __('panterka') => array('img' => 24),
            __('moro') => array('img' => 25),
            __('granatowy') => array('img' => 26),
            __('camel') => array('img' => 27)
        );
    }

    public function getBreadcrumb($breadcrumb = null) {
        if ($breadcrumb == null)
            return '';
        $return = '';
        $count = count($breadcrumb);
        $i = 0;
        $return = '<ul class="breadcrumbs">';
        foreach ($breadcrumb as $crumb) {
            if (!($i == $count - 1)) {
                $return .= '<li><a href="' . $crumb['url'] . '">' . $crumb['a'] . '</a></li>';
            } else {
                $return .= '<li class="current">' . $crumb['a'] . '</li>';
            }
            $i++;
        }
        $return .= '</ul>';
        return $return;
    }

    public function getOitemStatus($status) {
        if ($status == 'waiting')
            return 'oczekuje';
        elseif ($status == 'permanent_out_of_stock')
            return 'niedostępne';
        elseif ($status == 'collected')
            return 'zebrane';
        elseif ($status == 'notcollected')
            return 'niezebrane';
    }

    public function getOrderStatus($status) {
        if ($status == 'draft')
            return 'złożone';
        elseif ($status == 'sent')
            return 'zakończone';
        elseif ($status == 'canceled')
            return 'anulowane';
    }

    public function getOrderStatusUser($status) {
        if ($status == 'draft')
            return '<span style="font-weight:bold; color:orange;">' . __('ZŁOŻONE') . '</span>';
        elseif ($status == 'sent')
            return '<span style="font-weight:bold; color:green;">' . __('ZAKOŃCZONE') . '</span>';
        elseif ($status == 'canceled')
            return '<span style="font-weight:bold; color:green;">' . __('ANULOWANE<') . '/span>';
    }

    public function getPriority($status) {
        if ($status == '1')
            return 'niski';
        elseif ($status == '2')
            return 'średni';
        elseif ($status == '3')
            return 'wysoki';
        elseif ($status == '4')
            return 'bardzo wysoki';
        elseif ($status == '5')
            return 'najwyższy';
    }

    public function getTaskStatus($status) {
        if ($status == 'draft')
            return '<span style="font-weight:bold; color:red;">Utworzone</span>';
        elseif ($status == 'seen' || $status == 'noticed')
            return '<span style="font-weight:bold; color:red;">Do zrobienia</span>';
        elseif ($status == 'later')
            return '<span style="font-weight:bold; color:orange;">Odłożone na później</span>';
        elseif ($status == 'done')
            return '<span style="font-weight:bold; color:green;">Zrobione</span>';
    }

    public function getStatus($status) {
        if ($status == 'na')
            return 'niedostępny';
        elseif ($status == 'unknown')
            return 'dostępny';
    }

    public function getState($status) {
        if ($status == 'draft')
            return 'szkic';
        elseif ($status == 'inactive')
            return 'nieaktywny';
        elseif ($status == 'active')
            return 'aktywny';
    }

    public function getLocalization($loc) {
        return $loc;
    }

    
    public function getSection($status) {
        if ($status == 'cod_ready_to_send')
            echo 'pobrania';
        elseif ($status == 'cod_ready_to_send_pack')
            echo 'pobrania bez etykiet';
        elseif ($status == 'cod_ready_to_send_pack_all')
            echo 'pobrania z etykietami';
        elseif ($status == 'courier_waiting_for_cash')
            echo 'kurier';
        elseif ($status == 'pickup_waiting_for_cash')
            echo 'paczkomaty';
        elseif ($status == 'courier_ready_to_send')
            echo 'kurier zapłacone';
        elseif ($status == 'pickup_ready_to_send')
            echo 'paczkomaty zapłacone';
        elseif ($status == 'courier_ready_to_send_pack')
            echo 'kurier bez etykiet';
        elseif ($status == 'courier_ready_to_send_pack_all')
            echo 'kurier z etykietami';
        elseif ($status == 'pickup_ready_to_send_pack')
            echo 'paczkomaty bez etykiet';
        elseif ($status == 'pickup_ready_to_send_pack_all')
            echo 'paczkomaty z etykietami';
        elseif ($status == 'done')
            echo 'zakończone';
        elseif ($status == 'exchanges')
            echo 'wymiany';
        elseif ($status == 'exchanges_ready_to_send')
            echo 'wymiany do wysyłki';
        elseif ($status == 'exchanges_ready_to_send_pack')
            echo 'wymiany bez etykiet';
        elseif ($status == 'exchanges_ready_to_send_pack_all')
            echo 'wymiany z etykiatami';
        else
            echo $status;
    }

    public function getSellPrice($price) {
        if (empty($price))
            return 0;
        else {
            if ($price <= 20)
                return $price * 2.2;
            elseif ($price > 20 && $price <= 30)
                return $price * 2.0;
            elseif ($price > 30 && $price <= 40)
                return $price * 1.8;
            elseif ($price > 40)
                return $price * 1.7;
        }
    }

    public function getSizeTotal($status) {
        if ($status <= 0)
            return '<span style="font-weight:bold; color:red;">' . $status . '</span>';
        else
            return '<span style="font-weight:bold; color:green;">' . $status . '</span>';
    }

    function d2w($digits) {
        $jednosci = Array('zero', 'jeden', 'dwa', 'trzy', 'cztery', 'pięć', 'sześć', 'siedem', 'osiem', 'dziewięć');
        $dziesiatki = Array('', 'dziesięć', 'dwadzieścia', 'trzydzieści', 'czterdzieści', 'piećdziesiąt', 'sześćdziesiąt', 'siedemdziesiąt', 'osiemdziesiąt', 'dziewiećdziesiąt');
        $setki = Array('', 'sto', 'dwieście', 'trzysta', 'czterysta', 'piećset', 'sześćset', 'siedemset', 'osiemset', 'dziewiećset');
        $nastki = Array('dziesieć', 'jedenaście', 'dwanaście', 'trzynaście', 'czternaście', 'piętnaście', 'szesnaście', 'siedemnaście', 'osiemnaście', 'dzięwietnaście');
        $tysiace = Array('tysiąc', 'tysiące', 'tysięcy');

        $digits = (string) $digits;
        $digits = strrev($digits);
        $i = strlen($digits);

        $string = '';
        if ($i > 5 && $digits[5] > 0)
            $string .= $setki[$digits[5]] . ' ';
        if ($i > 4 && $digits[4] > 1)
            $string .= $dziesiatki[$digits[4]] . ' ';
        elseif ($i > 3 && $digits[4] == 1)
            $string .= $nastki[$digits[3]] . ' ';
        if ($i > 3 && $digits[3] > 0 && $digits[4] != 1)
            $string .= $jednosci[$digits[3]] . ' ';

        $tmpStr = substr(strrev($digits), 0, -3);
        if (strlen($tmpStr) > 0) {
            $tmpInt = (int) $tmpStr;
            if ($tmpInt == 1)
                $string .= $tysiace[0] . ' ';
            elseif (( $tmpInt % 10 > 1 && $tmpInt % 10 < 5 ) && ( $tmpInt < 10 || $tmpInt > 20 ))
                $string .= $tysiace[1] . ' ';
            else
                $string .= $tysiace[2] . ' ';
        }

        if ($i > 2 && $digits[2] > 0)
            $string .= $setki[$digits[2]] . ' ';
        if ($i > 1 && $digits[1] > 1)
            $string .= $dziesiatki[$digits[1]] . ' ';
        elseif ($i > 0 && $digits[1] == 1)
            $string .= $nastki[$digits[0]] . ' ';
        if ($digits[0] > 0 && $digits[1] != 1)
            $string .= $jednosci[$digits[0]] . ' ';

        return $string;
    }

    public function slownie($kwota) {
        $kwota = str_replace(',', '.', $kwota);
        if (floor($kwota) == $kwota)
            $kwota = $kwota . ',00';
        else
            $kwota = str_replace('.', ',', $kwota);
        $zl = array("złotych", "złoty", "złote");
        $gr = array("groszy", "grosz", "grosze");
        $kwotaArr = explode(',', $kwota);
        $ostZl = substr($kwotaArr[0], -1, 1);
        switch ($ostZl) {
            case "0":
                $zlote = $zl[0];
                break;

            case "1":
                $ost2Zl = substr($kwotaArr[0], -2, 2);


                if ($kwotaArr[0] == "1") {
                    $zlote = $zl[1];
                } elseif ($ost2Zl == "01") {
                    $zlote = $zl[0];
                } else {
                    $zlote = $zl[0];
                }
                break;

            case "2":
                $ost2Zl = substr($kwotaArr[0], -2, 2);
                if ($ost2Zl == "12") {
                    $zlote = $zl[0];
                } else {
                    $zlote = $zl[2];
                }
                break;

            case "3":
                $ost2Zl = substr($kwotaArr[0], -2, 2);
                if ($ost2Zl == "13") {
                    $zlote = $zl[0];
                } else {
                    $zlote = $zl[2];
                }
                break;

            case "4":
                $ost2Zl = substr($kwotaArr[0], -2, 2);
                if ($ost2Zl == "14") {
                    $zlote = $zl[0];
                } else {
                    $zlote = $zl[2];
                }
                break;

            case "5":
                $zlote = $zl[0];
                break;

            case "6":
                $zlote = $zl[0];
                break;

            case "7":
                $zlote = $zl[0];
                break;

            case "8":
                $zlote = $zl[0];
                break;

            case "9":
                $zlote = $zl[0];
                break;
        }
        $ostGr = substr($kwotaArr[1], -1, 1);
        switch ($ostGr) {
            case "0":
                $grosze = $gr[0];
                break;

            case "1":
                $ost2Gr = substr($kwotaArr[1], -2, 2);


                if ($kwotaArr[0] == "1") {
                    $grosze = $gr[1];
                } elseif ($ost2Gr == "01") {
                    $grosze = $gr[1];
                } else {
                    $grosze = $gr[0];
                }
                break;

            case "2":
                $ost2Gr = substr($kwotaArr[1], -2, 2);
                if ($ost2Gr == "12") {
                    $grosze = $gr[0];
                } else {
                    $grosze = $gr[2];
                }
                break;

            case "3":
                $ost2Gr = substr($kwotaArr[1], -2, 2);
                if ($ost2Gr == "13") {
                    $grosze = $gr[0];
                } else {
                    $grosze = $gr[2];
                }
                break;

            case "4":
                $ost2Gr = substr($kwotaArr[1], -2, 2);
                if ($ost2Gr == "14") {
                    $grosze = $gr[0];
                } else {
                    $grosze = $gr[2];
                }
                break;

            case "5":
                $grosze = $gr[0];
                break;

            case "6":
                $grosze = $gr[0];
                break;

            case "7":
                $grosze = $gr[0];
                break;

            case "8":
                $grosze = $gr[0];
                break;

            case "9":
                $grosze = $gr[0];
                break;
        }

        return( $this->d2w($kwotaArr[0]) . ' ' . $zlote . ' i ' . ((floor($kwotaArr[1]) == 0) ? 'zero groszy' : ($this->d2w($kwotaArr[1]) . $grosze)) );
    }
    public function ShowTableWithColorCust()
    {

        $names = array('Niebieski','Czarny','Fioletowy','Zielony','Czerwony','Żółty');
        $skins = array('light-blue','black','purple','green','red','yellow');
        echo '<ul class="list-unstyled clearfix">';
        for ($i = 0; $i < 6; $i++) {
            echo '<li style="float:left; width: 33.33333%; padding: 5px;">';
            echo '<a href="" data-skin="skin-'.$skins[$i].'" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover">';
            echo '<div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-'.$skins[$i].'-active"></span>';
            echo '<span class="bg-'.$skins[$i].'" style="display:block; width: 80%; float: left; height: 7px;"></span></div>';
            echo '<div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32;"></span>';
            echo '<span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>';
            echo '</div></a><p class="text-center no-margin">'.$names[$i].'</p></li>';
        }
        echo '</ul>';
    }
}

?>