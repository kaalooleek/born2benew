<?php
namespace App\Controller;

use App\Model\Entity\WorkersNotification;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

class NotificationsController extends AppController
{
    public function index()
    {
        $this->notifications = TableRegistry::get('notifications')->find('all', ['contain' => ['Groups', 'Workers']]);
        //$this->groups = TableRegistry::get('groups')->find()->;
        //$notifications = $this->paginate($this->Notifications);
        //$this->set(compact('notifications'));
        //$this->set('_serialize', ['notifications']);
        $this->paginate = [
            'contain' => ['Groups','Workers']
        ];
        $this->set('notifications', $this->paginate($this->notifications));
        //debug($this->notifications);
    }

    public function getGroups(){
        if($this->request->is('get')){
            $groups = TableRegistry::get('groups');
            echo json_encode($groups->find()->toArray());
            $this->autoRender = false;
        }
    }
    public function add()
    {
        $notification = $this->Notifications->newEntity();
        if ($this->request->is('post')) {  
            $conn = ConnectionManager::get('default');
            $conn->begin();
            $this->request->data['notification']['worker_id'] = $this->Auth->user('id');
            $this->request->data['notification']['group_id'] = $this->request->data['notification']['group_id']['value'];
            $notification = $this->Notifications->patchEntity($notification, $this->request->data['notification']);
            $workers_notifications = TableRegistry::get('workers_notifications');
            $workers_groups = TableRegistry::get('workers_groups')->find()->where(['group_id' => $this->request->data['notification']['group_id']])->toArray();
            $count = count($workers_groups);
            $check_count = 0;
            if ($this->Notifications->save($notification)) {
                foreach ($workers_groups as $item) {
                    $worker_notification = new WorkersNotification();
                    $worker_notification = $workers_notifications->patchEntity($worker_notification, ['worker_id' => $item['worker_id'], 'notification_id' => $notification->id]);
                   if ($workers_notifications->save($worker_notification)) {
                         $check_count++;
                        }
                }
            }
            $response = new \stdClass();
            if($check_count == $count){
                $conn->commit();
                $response->status = "success";
                $response->id = $notification->id;
                $response->sender = $this->Auth->user('first_name') . " " . $this->Auth->user('last_name');
                echo json_encode($response);
            } else {
                $response->status = "error";
                echo json_encode($response);
            }
            $this->autoRender = false;
        }
    }
    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);
        $notification = $this->Notifications->get($this->request->data['id']);
        $response = new \stdClass();
        if ($this->Notifications->delete($notification)) {
            $response->status = "success";
        } else {
            $response->status = "error";
        }
        echo json_encode($response);
        $this->autoRender = false;
    }
}