<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TransfersInvoices Controller
 *
 * @property \App\Model\Table\TransfersInvoicesTable $TransfersInvoices
 */
class TransfersInvoicesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Transfers', 'Invoices']
        ];
        $transfersInvoices = $this->paginate($this->TransfersInvoices);

        $this->set(compact('transfersInvoices'));
        $this->set('_serialize', ['transfersInvoices']);
    }

    /**
     * View method
     *
     * @param string|null $id Transfers Invoice id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $transfersInvoice = $this->TransfersInvoices->get($id, [
            'contain' => ['Transfers', 'Invoices']
        ]);

        $this->set('transfersInvoice', $transfersInvoice);
        $this->set('_serialize', ['transfersInvoice']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $transfersInvoice = $this->TransfersInvoices->newEntity();
        if ($this->request->is('post')) {
            $transfersInvoice = $this->TransfersInvoices->patchEntity($transfersInvoice, $this->request->data);
            if ($this->TransfersInvoices->save($transfersInvoice)) {
                $this->Flash->success(__('The transfers invoice has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The transfers invoice could not be saved. Please, try again.'));
            }
        }
        $transfers = $this->TransfersInvoices->Transfers->find('list', ['limit' => 200]);
        $invoices = $this->TransfersInvoices->Invoices->find('list', ['limit' => 200]);
        $this->set(compact('transfersInvoice', 'transfers', 'invoices'));
        $this->set('_serialize', ['transfersInvoice']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Transfers Invoice id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $transfersInvoice = $this->TransfersInvoices->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $transfersInvoice = $this->TransfersInvoices->patchEntity($transfersInvoice, $this->request->data);
            if ($this->TransfersInvoices->save($transfersInvoice)) {
                $this->Flash->success(__('The transfers invoice has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The transfers invoice could not be saved. Please, try again.'));
            }
        }
        $transfers = $this->TransfersInvoices->Transfers->find('list', ['limit' => 200]);
        $invoices = $this->TransfersInvoices->Invoices->find('list', ['limit' => 200]);
        $this->set(compact('transfersInvoice', 'transfers', 'invoices'));
        $this->set('_serialize', ['transfersInvoice']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Transfers Invoice id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $transfersInvoice = $this->TransfersInvoices->get($id);
        if ($this->TransfersInvoices->delete($transfersInvoice)) {
            $this->Flash->success(__('The transfers invoice has been deleted.'));
        } else {
            $this->Flash->error(__('The transfers invoice could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
