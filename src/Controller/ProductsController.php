<?php

namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\Product;
use App\Model\Entity\Image;
use Cake\ORM\TableRegistry;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 */
class ProductsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {

        if($this->request->is('post')){
            $this->Products = TableRegistry::get('products')->find()->contain(['Sizes','Categories','Images'])->toArray();
            echo json_encode($this->Products);
            $this->autoRender = false;
        } else {
            $products = $this->paginate(TableRegistry::get('products')->find()->contain(['Sizes','Categories','Images']));
            $this->set(compact('products'));
        }
        //debug($products);
    }

    public function step1(){
        $this->render('step1', 'step1');
    }

    public function step2(){
        $this->render('step2', 'step2');
    }

    public function saveproduct(){
        $data = $this->request->data;
        $files = $data['files'];
        //debug($files);
        $this->loadModel('Products');
        $this->loadModel('Images');


        $product = new Product($this->request->data['data']);
        $new_product = $this->Products->save($product);
        for($i = 0; $i<count($files); $i++){
            $this::base64_to_jpeg($files[$i]['link'], 'img/products/'.$data['data']['code'].'/', $i.'.jpg', "wb");
            $image = new Image();
            $image->link = 'img/products/'.$data['data']['code'].'/'.$i.'.jpg';
            $image->product_id = $new_product['id'];
            if($this->Images->save($image)){
                echo "success";
            }
        }
        $this->autoRender = false;
    }
    /*
    public function titleImg() {
        $data = $this->request->data;
        $id = $data['id'];
        $product = TableRegistry::get('Images')->find()->select('link')->where(['product_id' => $id])->order(['id' => 'ASC'])->first()->toArray();
        echo json_encode($product);
        $this->autoRender = false;
    }*/

    public function saveImages() {
        $data = $this->request->data;
        $id = $data['id'];
        $this->loadModel('Images');
        $imagesTable = TableRegistry::get('Images');
        if(isset($data['delete'])) { // Usuwanie zdjęć
            $delete = $data['delete'];
            for($i = 0; $i < count($delete); $i++) {
                $src = $delete[$i]['link'];
                $ext = explode("webroot/", $src);
                if(isset($ext[1])) {
                    unlink($ext[1]);
                    $record = $this->Images->get($delete[$i]['id']);
                    $result = $this->Images->delete($record);
                }
                else {
                    unlink($src);
                    $record = $this->Images->get($delete[$i]['id']);
                    $result = $this->Images->delete($record);
                }
            }
        }
        if(isset($data['files'])) // Zapisywanie zdjęć
        {
            $files = $data['files'];
            for($i = 0; $i<count($files); $i++){
                $src = $files[$i]['link'];
                $ext = explode("webroot/", $src);
                if(isset($ext[1])) {
                    $date = date("Y_m_d_h_i_s");
                    rename($ext[1], 'img/products/'.$id.'/'.$i.'_'.$date.'.jpg');
                    $image = $imagesTable->get($files[$i]['id']);
                    $image->link = 'img/products/'.$id.'/'.$i.'_'.$date.'.jpg';
                    $imagesTable->save($image);
                }
                else {
                    $date = date("Y_m_d_h_i_s");
                    $this::base64_to_jpeg($files[$i]['link'], 'img/products/'.$id.'/', $i.'_'.$date.'.jpg', "wa+"); //TODO zapisywanie do odpowiedniej ścieżki b2b_cake/webroot/img/products....
                    $image = new Image();
                    $image->link = 'img/products/'.$id.'/'.$i.'_'.$date.'.jpg';
                    $image->product_id = $id;
                    $this->Images->save($image);
                }
            }
        }
        $response = new \stdClass();
        $images = $imagesTable->find()->where(['product_id' => $id]);
        $response->images = $images;
        echo json_encode($response);
        $this->autoRender = false;
    }

    function base64_to_jpeg($base64_string, $folder_name, $output_file, $mode) {
        if(!file_exists('img/products/')){
            mkdir('img/products/');
        }
        if(!file_exists($folder_name)){
            mkdir($folder_name, 0777, true);
        }
        $ifp = fopen($folder_name.$output_file, $mode);
        $data = explode(',', $base64_string);
        fwrite($ifp, base64_decode($data[1]));
        fclose($ifp);

        return $output_file;
    }

    /**
     * View method
     *
     * @param string|null $id Product id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $product = $this->Products->get($id, [
            'contain' => ['Pgroups', 'Categories', 'Locals', 'Imgs', 'Params', 'Reviews', 'Sizes']
        ]);

        $this->set('product', $product);
        $this->set('_serialize', ['product']);
    }

    public function getProduct(){
        $request = $this->request->data;
        $id = $request['id'];
        $product = TableRegistry::get('Products')->find()->where(['id' => $id])->contain(['Sizes', 'Images'])->order(['Products.id' => 'DESC'])->first()->toArray();
        echo json_encode($product);
        $this->autoRender = false;
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {

    }

    public function getCategories(){
        $this->loadModel('Categories');
        $categories = $this->Categories->find()->toArray();
        echo json_encode($categories);
        $this->autoRender = false;
    }

    /**
     * Edit method
     *
     * @param string|null $id Product id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        if ($this->request->is('post')) {
            $reviewsTable = $this->Products;
            $review = $reviewsTable->get($this->request->data['object']['id']); // Return article with id 12

            $review[$this->request->data['object']['name']] = $this->request->data['object']['value'];
            if ($reviewsTable->save($review)) {
                echo 'success';
            } else {
                echo 'error';
            }
            $this->autoRender = false;
        } else {
            $review = $this->Products->get($id);
            if ($review->nip == '') {
                $review->nip = "-";
            }
            if ($review->correction == "true") {
                $review->correction = "Tak";
            } else {
                $review->correction = "Nie";
            }
            if ($review->status == "paid") {
                $review->status = "<span class='label label-success'>Zapłacono</span>";
            } else {
                $review->status = "<span class='label label-warning'>Nie zapłacono</span>";
            }
            if ($review->payment_type == "przelew") {
                $review->payment_type = '<span class="fa fa-fw fa-credit-card"></span> ' . $review->payment_type;
            } else {
                $review->payment_type = '<span class="fa fa-fw fa-money"></span> ' . $review->payment_type;
            }

            $this->set('review', $review);
            /*$this->set('_serialize', ['review']);*/
        }
        /*
        $product = $this->Products->get($id, [
            'contain' => ['Categories', 'Locals']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $product = $this->Products->patchEntity($product, $this->request->data);
            if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The product could not be saved. Please, try again.'));
            }
        }
        $pgroups = $this->Products->Pgroups->find('list', ['limit' => 200]);
        $categories = $this->Products->Categories->find('list', ['limit' => 200]);
        $locals = $this->Products->Locals->find('list', ['limit' => 200]);
        $this->set(compact('product', 'pgroups', 'categories', 'locals'));
        $this->set('_serialize', ['product']);*/
    }

    /**
     * Delete method
     *
     * @param string|null $id Product id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $product = $this->Products->get($id);
        if ($this->Products->delete($product)) {
            $this->Flash->success(__('The product has been deleted.'));
        } else {
            $this->Flash->error(__('The product could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function stock($scope = false, $limit = 100, $simple = false) {
        $this->Product->recursive = 1;
        $this->Product->Behaviors->load('Containable');
        if ($this->request->is('post')) {
            $this->request->params['named']['page'] = 1;
            if (!empty($this->request->data['Product']['search'])) {
                $codes = explode(' ', $this->request->data['Product']['search']);
                $cond = array(
                    'or' => array(
                        'Product.code' => $codes,
                    ));
            } else
                $cond = array(
                    'or' => array(
                        'Product.name LIKE' => '%' . $this->request->data['Product']['search2'] . '%',
                        'Product.keywords LIKE' => '%' . $this->request->data['Product']['search2'] . '%',
                        'Product.title LIKE' => '%' . $this->request->data['Product']['search2'] . '%'
                    ));

            $this->Paginator->settings = array(
                'conditions' => $cond,
                'limit' => 100,
                'contain' => array('Review.id', 'Size', 'Local', 'Img'),
                'order' => array(
                    'Product.id' => 'desc'
                ),
                'maxLimit' => 2000
            );
            $products = $this->Paginator->paginate('Product');
        } else {
            if ($scope == 'all')
                $cond = array('Product.state !=' => 'inactive');
            elseif ($scope == 'waiting')
                $cond = array('Product.waiting_total !=' => 0);
            elseif ($scope == 'shop_1')
                $cond = array('Product.shop_1_total !=' => 0);
            elseif ($scope == 'all_inactive') {
                $cond = array();
            } elseif ($scope == 'unknown') {
                $cond = array('Product.status' => 'unknown', 'Product.shop_1_total <=' => 0);
            } else
                $cond = array('Product.state !=' => 'inactive', 'OR' => array('Product.total !=' => 0, 'Product.virtual_product' => 1));
            $this->Paginator->settings = array(
                'conditions' => $cond,
                'order' => array(
                    'Product.id' => 'desc'
                ),
                'contain' => array('Review.id', 'Size', 'Local', 'Img'),
                'maxLimit' => 2000
            );
            $products = $this->Paginator->paginate('Product');
        }

        $this->layout = 'admin';
        $this->set('products', $products);
        $this->set('menu_item', 'MAGAZYN');
        if ($simple)
            $this->render('stock_simple');
    }

}