<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Imgs Controller
 *
 * @property \App\Model\Table\ImgsTable $Imgs
 */
class ImgsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Products', 'Websites']
        ];
        $imgs = $this->paginate($this->Imgs);

        $this->set(compact('imgs'));
        $this->set('_serialize', ['imgs']);
    }

    /**
     * View method
     *
     * @param string|null $id Img id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $img = $this->Imgs->get($id, [
            'contain' => ['Products', 'Websites']
        ]);

        $this->set('img', $img);
        $this->set('_serialize', ['img']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $img = $this->Imgs->newEntity();
        if ($this->request->is('post')) {
            $img = $this->Imgs->patchEntity($img, $this->request->data);
            if ($this->Imgs->save($img)) {
                $this->Flash->success(__('The img has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The img could not be saved. Please, try again.'));
            }
        }
        $products = $this->Imgs->Products->find('list', ['limit' => 200]);
        $websites = $this->Imgs->Websites->find('list', ['limit' => 200]);
        $this->set(compact('img', 'products', 'websites'));
        $this->set('_serialize', ['img']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Img id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $img = $this->Imgs->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $img = $this->Imgs->patchEntity($img, $this->request->data);
            if ($this->Imgs->save($img)) {
                $this->Flash->success(__('The img has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The img could not be saved. Please, try again.'));
            }
        }
        $products = $this->Imgs->Products->find('list', ['limit' => 200]);
        $websites = $this->Imgs->Websites->find('list', ['limit' => 200]);
        $this->set(compact('img', 'products', 'websites'));
        $this->set('_serialize', ['img']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Img id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $img = $this->Imgs->get($id);
        if ($this->Imgs->delete($img)) {
            $this->Flash->success(__('The img has been deleted.'));
        } else {
            $this->Flash->error(__('The img could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
