<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Oitems Controller
 *
 * @property \App\Model\Table\OitemsTable $Oitems
 */
class OitemsController extends AppController {
    public function test()
    {
        $this->Oitems->Sizes->add(220, 36);
    }
    public function change_oitem() {
        $price = $this->request->data['Oitem']['price'];
        $id = $this->request->data['Oitem']['id'];
        $current_user = $this->Auth->User();
        $size_id = $this->request->data['Oitem']['size_id'];
        $oitem = $this->Oitem->find('first', array('conditions' => array('Oitem.id' => $id)));
        $nitem = array();
        $update_totals = array();
        if (!empty($oitem)) {
            $nitem['Oitem']['id'] = $id;
            $nitem['Oitem']['price'] = $price;
            $nitem['Oitem']['status'] = 'notcollected';
            $nitem['Oitem']['size_id'] = $size_id;
            // DODAWANIE STAREJ PARY
            $osize = $this->Oitem->Size->find('first', array('recursive' => -1, 'conditions' => array('Size.id' => $oitem['Oitem']['size_id'])));
            $pro = $this->Oitem->Size->Product->find('first', array('recursive' => -1, 'conditions' => array('Product.id' => $osize['Size']['product_id'])));
            $osize['Product'] = $pro['Product'];
            if ($osize['Size']['debit'] < 0)
                $osize['Size']['debit'] = $osize['Size']['debit'] + 1;
            else
                $osize['Size']['total'] = $osize['Size']['total'] + 1;
            $this->Oitem->Size->Product->stocklog(array('added', $osize['Product']['code'] . ' ' . $osize['Size']['size'], 1, date('Y-m-d H:i:s'), 'adding'));
            $this->Oitem->Size->save($osize);

            // ODEJMOWANIE NOWEJ PARY
            $nsize = $this->Oitem->Size->find('first', array('recursive' => -1, 'conditions' => array('Size.id' => $size_id)));
            $pro = $this->Oitem->Size->Product->find('first', array('recursive' => -1, 'conditions' => array('Product.id' => $nsize['Size']['product_id'])));
            $nsize['Product'] = $pro['Product'];
            if ($nsize['Size']['total'] != 0) {
                $nsize['Size']['total'] = $nsize['Size']['total'] - 1;
            } else {
                $nsize['Size']['debit'] = $nsize['Size']['debit'] - 1;
            }
            $this->Oitem->Size->Product->stocklog(array('removed', $nsize['Product']['code'] . ' ' . $nsize['Size']['size'], 1, date('Y-m-d H:i:s'), 'removing'));
            $this->Oitem->Size->save($nsize);

            // ZAPISYWANIE OITEM
            $this->Oitem->save($nitem);
            $or = $this->Oitem->Order->find('first', array('recursive' => -1, 'conditions' => array('Order.id' => $oitem['Oitem']['order_id'])));
            // DODAWANIE NOTATKI
            $newor = array();
            $newor['Order']['id'] = $or['Order']['id'];
            $newor['Order']['notes'] = $or['Order']['notes'] . '<span style="color:grey;">[' . date("m/d  G:i") . '] PODMIANA - <span style="color:black;">Z ' . $osize['Product']['code'] . ' ' . $osize['Size']['size'] . ' NA ' . $nsize['Product']['code'] . ' ' . $nsize['Size']['size'] . '</span> [' . $current_user['email'] . ']</span><br/>';
            $this->Oitem->Order->save($newor, false);
            $update_totals[] = $osize['Product']['code'];
            $update_totals[] = $nsize['Product']['code'];
            $this->Oitem->Size->Product->update_totals(true, $update_totals);
            $this->flashSuccess(__('Para została podmieniona.'), array('controller' => 'orders', 'action' => 'view', $or['Order']['id']));
        }
    }

    public function add_oitem() {
        $price = $this->request->data['Oitem']['price'];
        $id = $this->request->data['Oitem']['id'];
        $current_user = $this->Auth->User();
        $size_id = $this->request->data['Oitem']['size_id'];
        $oitem = $this->Oitem->find('first', array('conditions' => array('Oitem.id' => $id)));
        $nitem = array();
        $update_totals = array();
        if (!empty($oitem)) {
            // ODEJMOWANIE NOWEJ PARY
            $nsize = $this->Oitem->Size->find('first', array('recursive' => -1, 'conditions' => array('Size.id' => $size_id)));
            $pro = $this->Oitem->Size->Product->find('first', array('recursive' => -1, 'conditions' => array('Product.id' => $nsize['Size']['product_id'])));
            $nsize['Product'] = $pro['Product'];
            if ($nsize['Size']['total'] != 0) {
                $nsize['Size']['total'] = $nsize['Size']['total'] - 1;
            } else {
                $nsize['Size']['debit'] = $nsize['Size']['debit'] - 1;
            }
            $this->Oitem->Size->Product->stocklog(array('removed', $nsize['Product']['code'] . ' ' . $nsize['Size']['size'], 1, date('Y-m-d H:i:s'), 'removing'));
            $this->Oitem->Size->save($nsize);

            // ZAPISYWANIE OITEM
            $this->Oitem->create();
            $nitem['Oitem']['size_id'] = $nsize['Size']['id'];
            $nitem['Oitem']['order_id'] = $oitem['Oitem']['order_id'];
            $nitem['Oitem']['quantity'] = 1;
            $nitem['Oitem']['returned'] = 0;
            $nitem['Oitem']['status'] = 'notcollected';
            $nitem['Oitem']['price'] = $price;
            $this->Oitem->save($nitem);
            $or = $this->Oitem->Order->find('first', array('recursive' => -1, 'conditions' => array('Order.id' => $oitem['Oitem']['order_id'])));
            // DODAWANIE NOTATKI
            $newor = array();
            $newor['Order']['id'] = $or['Order']['id'];
            $newor['Order']['notes'] = $or['Order']['notes'] . '<span style="color:grey;">[' . date("m/d  G:i") . '] DODANIE - <span style="color:black;">' . $nsize['Product']['code'] . ' ' . $nsize['Size']['size'] . '</span> [' . $current_user['email'] . ']</span><br/>';
            $this->Oitem->Order->save($newor, false);
            $update_totals[] = $nsize['Product']['code'];
            $this->Oitem->Size->Product->update_totals(true, $update_totals);
            $this->flashSuccess(__('Para została dodana.'), array('controller' => 'orders', 'action' => 'view', $or['Order']['id']));
        }
    }

    public function get_oitem_replacebox() {
        $this->layout = 'ajax';
        if ($this->request->is('ajax')) {
            $oitem = $this->request->data['oitem'];
            $price = $this->request->data['price'];
            $code = $this->request->data['code'];
            $size = $this->request->data['size'];
            $pro = $this->Oitem->Size->Product->find('first', array('conditions' => array('Product.code' => $code)));
            if (!empty($pro)) {
                $size = $this->Oitem->Size->find('first', array('conditions' => array('Size.size' => $size, 'Size.product_id' => $pro['Product']['id'])));
                if (!empty($size)) {
                    $size['Product']['Img'] = $pro['Img'];
                    $size['price'] = $price;
                    $this->set('size', $size);
                    $this->set('oitem', $oitem);
                } else {
                    echo 'NIE ZNALAZŁEM ROZMIARU';
                }
            } else {
                echo 'NIE ZNALAZŁEM MODELU';
            }
        }
    }

    public function get_oitem_exchangebox($add = false, $index) {
        $this->layout = 'ajax';
        if ($this->request->is('ajax')) {
            $oitem = $this->request->data['oitem'];
            $price = $this->request->data['price'];
            $code = $this->request->data['code'];
            $size = $this->request->data['size'];
            $oitem = $this->Oitem->find('first', array('conditions' => array('Oitem.id' => $this->request->data['oitem'])));
            $pro = $this->Oitem->Size->Product->find('first', array('conditions' => array('Product.code' => $code)));
            if (!empty($pro)) {
                $size = $this->Oitem->Size->find('first', array('conditions' => array('Size.size' => $size, 'Size.product_id' => $pro['Product']['id'])));
                if (!empty($size)) {
                    $size['Product']['Img'] = $pro['Img'];
                    if ($add == 'add') {
                        $size['price'] = $size['Product']['price'];
                        $size['price_extra'] = 0;
                    } else {
                        if ($size['Size']['product_id'] == $oitem['Size']['product_id']) {
                            $size['price'] = 0;
                            $size['price_extra'] = 0;
                        } else {
                            $size['price_extra'] = $size['Product']['price'] - $oitem['Oitem']['price'];
                            if (!empty($size['price_extra'])) {
                                $size['difference'] = $size['price_extra'];
                                if ($size['price_extra'] < 0)
                                    $size['price_extra'] = 0;
                            }
                            $size['price'] = 0;
                        }
                    }
                    $this->set('index', $index);
                    $this->set('size', $size);
                    $this->set('oitem', $oitem);
                } else {
                    echo 'NIE ZNALAZŁEM ROZMIARU';
                }
            } else {
                echo 'NIE ZNALAZŁEM MODELU';
            }
        }
    }

    public function get_oitem_addbox() {
        $this->layout = 'ajax';
        if ($this->request->is('ajax')) {
            $oitem = $this->request->data['oitem'];
            $price = $this->request->data['price'];
            $code = $this->request->data['code'];
            $size = $this->request->data['size'];
            $pro = $this->Oitem->Size->Product->find('first', array('conditions' => array('Product.code' => $code)));
            if (!empty($pro)) {
                $size = $this->Oitem->Size->find('first', array('conditions' => array('Size.size' => $size, 'Size.product_id' => $pro['Product']['id'])));
                if (!empty($size)) {
                    $size['Product']['Img'] = $pro['Img'];
                    $size['price'] = $price;
                    $this->set('size', $size);
                    $this->set('oitem', $oitem);
                } else {
                    echo 'NIE ZNALAZŁEM ROZMIARU';
                }
            } else {
                echo 'NIE ZNALAZŁEM MODELU';
            }
        }
    }

    public function delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Oitem->id = $id;
        if (!$this->Oitem->exists()) {
            throw new NotFoundException(__('Invalid oitem'));
        }
        $current_user = $this->Auth->User();
        $oitem = $this->Oitem->read();
        if ($this->Oitem->delete()) {
            // DODAWANIE STAREJ PARY
            $osize = $this->Oitem->Size->find('first', array('recursive' => -1, 'conditions' => array('Size.id' => $oitem['Oitem']['size_id'])));
            $pro = $this->Oitem->Size->Product->find('first', array('recursive' => -1, 'conditions' => array('Product.id' => $osize['Size']['product_id'])));
            $osize['Product'] = $pro['Product'];
            if ($osize['Size']['debit'] < 0)
                $osize['Size']['debit'] = $osize['Size']['debit'] + 1;
            else
                $osize['Size']['total'] = $osize['Size']['total'] + 1;
            $this->Oitem->Size->Product->stocklog(array('added', $osize['Product']['code'] . ' ' . $osize['Size']['size'], 1, date('Y-m-d H:i:s'), 'adding'));
            $this->Oitem->Size->save($osize);

            $or = $this->Oitem->Order->find('first', array('recursive' => -1, 'conditions' => array('Order.id' => $oitem['Oitem']['order_id'])));
            // DODAWANIE NOTATKI
            $newor = array();
            $newor['Order']['id'] = $or['Order']['id'];
            $newor['Order']['notes'] = $or['Order']['notes'] . '<span style="color:grey;">[' . date("m/d  G:i") . '] USUNIĘCIE - <span style="color:black;">' . $osize['Product']['code'] . ' ' . $osize['Size']['size'] . '</span> [' . $current_user['email'] . ']</span><br/>';
            $this->Oitem->Order->save($newor, false);
            $update_totals[] = $osize['Product']['code'];
            $this->Oitem->Size->Product->update_totals(true, $update_totals);
            $this->flashSuccess(__('Para została usunięta.'), array('controller' => 'orders', 'action' => 'view', $or['Order']['id']));
        }
    }

}
