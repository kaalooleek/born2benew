<?php

namespace App\Controller;

use App\Controller\AppController;
/**
 * Texts Controller
 *
 * @property \App\Model\Table\TextsTable $Texts
 */
class TextsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
//        $this->Text->recursive = 0;
//        $this->layout = 'admin';
//        $count = $this->Text->find('count', array('conditions' => array('Text.status' => '', 'Text.priority' => 'camp5')));
//        $this->flashInfo('PozostaĹ‚o do wysyĹ‚ki SMS ' . $count);
//        $this->set('texts', $this->paginate());



        $count = $this->Texts->find()->where(['Texts.status' => '', 'Texts.priority' => 'camp5'])->count();

        $this->Flash->set('Pozostało do wysyłki SMS '. $count, ['element' => 'default']);
        

        $texts = $this->paginate($this->Texts);

        $this->set(compact('texts'));
        $this->set('_serialize', ['texts']);
    }

    /**
     * View method
     *
     * @param string|null $id Text id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $text = $this->Texts->get($id, [
            'contain' => []
        ]);

        $this->set('text', $text);
        $this->set('_serialize', ['text']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $text = $this->Texts->newEntity();
        if ($this->request->is('post')) {
            $text = $this->Texts->patchEntity($text, $this->request->data);
            if ($this->Texts->save($text)) {
                $this->Flash->success(__('The text has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The text could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('text'));
        $this->set('_serialize', ['text']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Text id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $text = $this->Texts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $text = $this->Texts->patchEntity($text, $this->request->data);
            if ($this->Texts->save($text)) {
                $this->Flash->success(__('The text has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The text could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('text'));
        $this->set('_serialize', ['text']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Text id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $text = $this->Texts->get($id);
        if ($this->Texts->delete($text)) {
            $this->Flash->success(__('The text has been deleted.'));
        } else {
            $this->Flash->error(__('The text could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function getText($id = null)
    {
        $text = $this->Texts->find()->where(['Texts.id' => $id])->first();

        $dateSent = $text->sent;
        $dateCreated = $text->created;
        $dateModified = $text->modified;

        if ($dateSent != null)
            $text->sent = $dateSent->format('Y-m-d');
        //if($dateSent != null)
        $text->created = $dateCreated->format('Y-m-d');
        //if($dateSent != null)
        $text->modified = $dateModified->format('Y-m-d');

        echo json_encode($text);
        $this->autoRender = false;
    }

}
