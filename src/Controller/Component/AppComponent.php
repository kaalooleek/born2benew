<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

class AppComponent extends Component {

    public function stocklog($fields = false) {
        $fp = fopen('stockbackups/history.csv', 'a');

        fputcsv($fp, $fields);
        fclose($fp);
    }

}

?>