<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Pgroups Controller
 *
 * @property \App\Model\Table\PgroupsTable $Pgroups
 */
class PgroupsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Categories']
        ];
        $pgroups = $this->paginate($this->Pgroups);

        $this->set(compact('pgroups'));
        $this->set('_serialize', ['pgroups']);
    }

    /**
     * View method
     *
     * @param string|null $id Pgroup id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pgroup = $this->Pgroups->get($id, [
            'contain' => ['Categories', 'Products']
        ]);

        $this->set('pgroup', $pgroup);
        $this->set('_serialize', ['pgroup']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pgroup = $this->Pgroups->newEntity();
        if ($this->request->is('post')) {
            $pgroup = $this->Pgroups->patchEntity($pgroup, $this->request->data);
            if ($this->Pgroups->save($pgroup)) {
                $this->Flash->success(__('The pgroup has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pgroup could not be saved. Please, try again.'));
            }
        }
        $categories = $this->Pgroups->Categories->find('list', ['limit' => 200]);
        $this->set(compact('pgroup', 'categories'));
        $this->set('_serialize', ['pgroup']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pgroup id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pgroup = $this->Pgroups->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pgroup = $this->Pgroups->patchEntity($pgroup, $this->request->data);
            if ($this->Pgroups->save($pgroup)) {
                $this->Flash->success(__('The pgroup has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pgroup could not be saved. Please, try again.'));
            }
        }
        $categories = $this->Pgroups->Categories->find('list', ['limit' => 200]);
        $this->set(compact('pgroup', 'categories'));
        $this->set('_serialize', ['pgroup']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pgroup id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pgroup = $this->Pgroups->get($id);
        if ($this->Pgroups->delete($pgroup)) {
            $this->Flash->success(__('The pgroup has been deleted.'));
        } else {
            $this->Flash->error(__('The pgroup could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
