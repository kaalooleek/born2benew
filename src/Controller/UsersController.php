<?php

namespace App\Controller;


use App\Model\Entity\WorkersAccess;
use App\Model\Entity\WorkersGroup;
use Cake\ORM\TableRegistry;

use Cake\Core\Configure;

use FontLib\OpenType\TableDirectoryEntry;
use ReflectionClass;
use ReflectionMethod;
use App\Model\Entity\Worker;
use App\Model\Entity\Message;
use Cake\Cache\Cache;
use Cake\Datasource\ConnectionManager;
use Cake\Mailer\Email;


/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     *
     * @return \Cake\Network\Response|null
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Auth', ['authorize' => 'Controller']);
        $this->Auth->allow(['login', 'logout', 'permanent_logout', 'twostepauth', 'dashboard', 'newpasswd']);
    }

    public function getControllers()
    {
        $files = scandir('../src/Controller/');
        $results = [];
        $ignoreList = [
            '.',
            '..',
            'Component',
            'AppController.php',
        ];
        foreach ($files as $file) {
            if (!in_array($file, $ignoreList)) {
                $controller = explode('.', $file)[0];
                array_push($results, str_replace('Controller', '', $controller));
            }
        }
        return $results;
    }

    public function getActions($controllerName)
    {
        $className = 'App\\Controller\\' . $controllerName . 'Controller';
        $class = new ReflectionClass($className);
        $actions = $class->getMethods(ReflectionMethod::IS_PUBLIC);
        $results = [$controllerName => []];
        $ignoreList = ['beforeFilter', 'afterFilter', 'initialize'];
        foreach ($actions as $action) {
            if ($action->class == $className && !in_array($action->name, $ignoreList)) {
                array_push($results[$controllerName], $action->name);
            }
        }
        return $results;
    }

    public function getResources()
    {
        $controllers = $this->getControllers();
        $resources = [];
        foreach ($controllers as $controller) {
            $actions = $this->getActions($controller);
            array_push($resources, $actions);
        }
        return $resources;
    }

    public function index()
    {
        $workers = $this->paginate(TableRegistry::get("Workers"));
        $this->set(compact('workers'));
        $this->set('_serialize', ['workers']);
    }

    public function login()
    {
        $this->viewBuilder()->layout('login');
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                if ($this->Cookie->read('twostep_key') != Configure::read('AUTH_KEY'))
                    $this->Cookie->write('twostep_passed', null);
                if ($this->Cookie->read('twostep_passed') != null) {
                    if ($user['password_temp']) {
                        $this->request->session()->write('temp_user', $user);
                        return $this->redirect(["controller" => "users", "action" => "newpasswd"]);
                    } else {
                        $this->Auth->setUser($user);
                        return $this->redirect($this->Auth->redirectUrl('/dashboard'));
                    }
                } else {
                    $this->request->session()->write('temp_user', $user);
                    return $this->redirect(["controller" => "users", "action" => "twostepauth"]);
                }
            }
            $this->Flash->error('Nazwa użytkownika lub hasło jest niepoprawne');
        }
    }

    public function dashboard()
    {
        if (!$this->Auth->user()) {
            return $this->redirect(['controller' => 'users', 'action' => 'login']);
        }
        $orders = TableRegistry::get('Orders');
        $orders_from_last_month = $orders->find()->where('created BETWEEN (CURRENT_DATE() - INTERVAL 1 MONTH) AND CURRENT_DATE()');
        $orders_from_last_2month = $orders->find()->where('created BETWEEN (CURRENT_DATE() - INTERVAL 2 MONTH) AND (CURRENT_DATE() - INTERVAL 1 MONTH)');

        if ($orders_from_last_month >= $orders_from_last_2month) $orders_count_ico = 'up';
        else if ($orders_from_last_month < $orders_from_last_2month) $orders_count_ico = 'down';

        // $orders_today = $orders->find()->where('Orders.created > CURDATE()');


        $this->set('orders_count_ico', $orders_count_ico);

        $this->set('orders_from_last_month', $orders_from_last_month);
        $this->set('orders_from_last_2month', $orders_from_last_2month);
        $this->set(compact('orders'));
        $this->set('_serialize', ['orders']);
    }

    public function twostepauth()
    {
        $this->viewBuilder()->layout('twostepauth');
        if ($this->request->session()->read('temp_user') == null) {
            return $this->redirect(["controller" => "users", "action" => "login"]);
        }
        if ($this->request->is('post')) {
            $secretkey = Configure::read("AUTH_KEY");
            if (TokenAuth::verify($secretkey, $this->request->data['token'])) {
                if ($this->request->session()->read('temp_user')['password_temp'] == true) {
                    return $this->redirect(["controller" => "users", "action" => "newpasswd"]);
                } else {
                    $this->Cookie->write('twostep_passed', true);
                    $this->Cookie->write('twostep_key', Configure::read('AUTH_KEY'));
                    $this->Auth->setUser($this->request->session()->read('temp_user'));
                    $this->request->session()->write('temp_user', null);
                    return $this->redirect($this->Auth->redirectUrl('/dashboard'));
                }
            } else {
                $this->Flash->error('Kod jest niepoprawny');
            }
        }
    }

    public function getMyId()
    {
        echo $this->Auth->user('id');
        $this->autoRender = false;
    }


    public function newpasswd()
    {
        $this->viewBuilder()->layout('newpasswd');
        if ($this->request->session()->read('temp_user') == null) {
            return $this->redirect(["controller" => "users", "action" => "login"]);
        }
        if ($this->request->is('post')) {
            $workers = TableRegistry::get("Workers");

            $passwd = $this->request->data['password'];
            $passwd_repeat = $this->request->data['password_repeat'];
            if ($passwd == $passwd_repeat) {

                $temp_worker = $this->request->session()->read('temp_user');
                $temp_worker['password'] = $passwd;
                $temp_worker['password_temp'] = false;
                $worker = new Worker($temp_worker);
                if ($workers->save($worker)) {
                    $this->Auth->setUser($this->request->session()->read('temp_user'));
                    $this->request->session()->write('temp_user', null);
                    return $this->redirect($this->Auth->redirectUrl('/dashboard'));
                }
            } else {
                $this->Flash->error('Hasła nie są takie same');
            }
        }
    }

    function get_client_ip()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if (isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    public function logout()
    {
        $this->Flash->success("Wylogowano pomyślnie!");
        return $this->redirect($this->Auth->logout());
    }

    public function permanentLogout()
    {
        $this->Cookie->write('twostep_passed', null);
        $this->Flash->success("Wylogowano pomyślnie, ten komputer został zdezautoryzowany!");
        return $this->redirect($this->Auth->logout());
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Carts', 'Codes', 'Orders', 'Tasks', 'Wdays']
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    public function getUserColor(){
        echo json_decode(json_encode($this->request->session()->read('Auth.User')), FALSE)->color;
        $this->autoRender = false;
    }

    public function getWorkers()
    {
        if ($this->request->is('get')) {
            $workers = TableRegistry::get('Workers')->find()->where(['id !=' => $this->Auth->user('id')])->select(['id', 'first_name', 'last_name', 'avatar'])->toArray();
            echo json_encode($workers);
        }
        $this->autoRender = false;
    }

    public function getGroups()
    {
        if ($this->request->is('get')) {
            $groups = TableRegistry::get('Groups')->find()->toArray();
            echo json_encode($groups);
        }
        $this->autoRender = false;
    }

    public function getMessages($id = null)
    {
        if ($this->request->is('get')) {
            $messages = TableRegistry::get('Messages')->find()->contain(['Workers'])->where(['sender_id' => $this->Auth->user('id'), 'recipient_id' => $id])->orWhere(['sender_id' => $id, 'recipient_id' => $this->Auth->user('id')])->order(['Messages.created' => 'DESC'])->limit(15);
            $messages_new_count = TableRegistry::get('Messages')->find()->where(['sender_id' => $this->Auth->user('id'), 'recipient_id' => $id, 'status' => 0])->count();
            $messages_obj = new \stdClass();
            $messages_obj->messages = $messages;
            $messages_obj->new_messages_count = $messages_new_count;
            foreach ($messages_obj->messages as $message) {
                $message->worker_id = $message['worker']['id'];
            }
            echo json_encode($messages_obj);
        }
        $this->autoRender = false;
    }

    public function getNewMessagesCount($id = null)
    {
        if ($this->request->is('get')) {
            $messages_new_count = TableRegistry::get('Messages')->find()->where(['sender_id' => $this->Auth->user('id'), 'recipient_id' => $id, 'status' => 0])->count();
            $messages_obj = new \stdClass();
            $messages_obj->new_messages_count = $messages_new_count;
            echo json_encode($messages_obj);
        }
        $this->autoRender = false;
    }

    public function getNewestMessages()
    {
        if ($this->request->is('get')) {
            $conn = ConnectionManager::get('default');
            $stmt = $conn->execute(
                'select messages.*,workers.id, workers.avatar, workers.first_name,workers.last_name from messages inner join ( select max(created) as maxCreated, recipient_id from messages WHERE sender_id = "' . $this->Auth->user('id') . '" group by recipient_id) maxCreated on maxCreated.maxCreated = messages.created join workers on messages.recipient_id = workers.id'
            );
            $messages = $stmt->fetchAll('assoc');
            echo json_encode($messages);
        }
        $this->autoRender = false;
    }

    public function setMessagesReaded()
    {
        if ($this->request->is('get')) {
            $conn = ConnectionManager::get('default');
            $stmt = $conn->execute('UPDATE messages SET status=' . '1' . ' WHERE sender_id="' . $this->Auth->user('id') . '"');
        }
        $this->autoRender = false;
    }

    public function getCurrentRecipient()
    {
        echo $this->Cookie->read('current_recipient');
        $this->autoRender = false;
    }

    public function setCurrentRecipient($id = null)
    {
        $this->Cookie->write('current_recipient', $id);
        $this->autoRender = false;
    }

    public function sendMessage($id = null)
    {
        if ($this->request->is('post')) {
            $message_obj = $this->request->data;
            $message_obj['recipient_id'] = $this->Auth->user('id');
            $message_obj['sender_id'] = $id;
            $message_obj['status'] = 0;
            $message = new Message($message_obj);
            $messages = TableRegistry::get('Messages');
            if ($messages->save($message)) {
                echo "success";
            }
        }
        $this->autoRender = false;
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $this->Workers = TableRegistry::get("Workers");

            if ($this->Workers->find()->where(['email' => $this->request->data['user']['email']])->count() == 0) {
                $this->request->data['user']['password'] = $this->randomPassword();
                $this->request->data['user']['avatar'] = Configure::read('DEFAULT_AVATAR');
                $worker = new Worker($this->request->data['user']);
                $new_worker = $this->Workers->save($worker);
                if ($new_worker) {
                    $count_groups = count($this->request->data['groups_obj']);
                    $count = 0;
                    $user_groups = $this->request->data['groups_obj'];
                    $workers_groups = TableRegistry::get("Workers_groups");
                    foreach ($user_groups as $group){
                        $worker_group = new WorkersGroup();
                        $worker_group->worker_id = $new_worker->id;
                        $worker_group->group_id = $group['id'];
                        if($workers_groups->save($worker_group)){
                            $count++;
                        }
                    }
                    if($count == $count_groups) {
                        echo json_encode(['success' => 'true', 'email' => $this->request->data['user']['email'], 'tmp_pass' => $this->request->data['user']['password']]);
                    }
                }
            } else {
                echo "Konto o podanym adresie E-mail już istnieje";
            }
            $this->autoRender = false;
        }

    }

    public function setAccess()
    {
        if ($this->request->is('post')) {
            $this->loadModel('Workers_accesses');
            if ($this->request->data['action'] == 'add') {
                $new_access = new WorkersAccess();
                $new_access->worker_id = $this->request->data['uid'];
                $new_access->access_id = $this->request->data['access_id'];
                if ($this->WorkersAccesses->save($new_access)) {
                    echo "success";
                } else {
                    echo "error";
                }
            } else if ($this->request->data['action'] == 'delete') {
                $current_access = $this->WorkersAccesses->get($this->request->data['access_id']);
                if ($this->WorkersAccesses->delete($current_access)) {
                    echo "success";
                } else {
                    echo "error";
                }
            }
            $this->autoRender = false;
        }
    }

    public function setGroup()
    {
        if ($this->request->is('post')) {
            if ($this->request->data['action'] == 'add') {
                $new_group = new WorkersGroup();
                $new_group->worker_id = $this->request->data['uid'];
                $new_group->group_id = $this->request->data['group_id'];
                if (TableRegistry::get('WorkerS_groups')->save($new_group)) {
                    echo "success";
                } else {
                    echo "error";
                }
            } else if ($this->request->data['action'] == 'delete') {
                $current_group = TableRegistry::get('WorkerS_groups')->get($this->request->data['group_id']);
                if (TableRegistry::get('WorkerS_groups')->delete($current_group)) {
                    echo "success";
                } else {
                    echo "error";
                }
            }
            $this->autoRender = false;
        }
    }

    public function sendMail()
    {
        $email = new Email();
        $email->attachments([
                'photo.png' => [
                    'file' => WWW_ROOT . '\img\logo_min.png',
                    'mimetype' => 'image/png',
                    'contentId' => 'logo'
                ]
            ])
            ->viewVars([
                'email' => $this->request->data['email'],
                'pass' => $this->request->data['password'],
                'code' => 'https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=otpauth://totp/'.$this->request->data['email'].'?secret=' . Configure::read('AUTH_KEY')
            ])
            ->transport('gmail')
            ->from(['lorak1234@gmail.com' => 'B2B'])
            ->template('add')
            ->to($this->request->data['email'])
            ->emailFormat('html')
            ->subject('Nowe konto w panelu Born2Be');

        if($email->send()){
            echo 'success';
        } else {
            echo 'error';
        }

        $this->autoRender = false;
    }

    function randomPassword()
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    public function getUser()
    {
        if ($this->request->is(['post'])) {
            $worker = TableRegistry::get('Workers')->get($this->request->data['id'], [
                'contain' => ['Accesses', 'Groups'],
                'select' => ['Workers_accesses.id', 'Workers_groups.id']
            ]);
            if (($all_accesses = Cache::read('allaccesses')) === false) {
                $all_accesses = TableRegistry::get('Accesses')->find()->toArray();
                Cache::write('allaccesses', $all_accesses);
            } else {
                $all_accesses = Cache::read('allaccesses');
            }
            if (($all_groups = Cache::read('allgroups')) === false) {
                $all_groups = TableRegistry::get('Groups')->find()->toArray();
                Cache::write('allgroups', $all_groups);
            } else {
                $all_groups = Cache::read('allgroups');
            }
            $worker->all_accesses = $all_accesses;
            $worker->all_groups = $all_groups;

            echo json_encode($worker);
            $this->autoRender = false;
        }
    }

    public function lock_user()
    {
        if ($this->request->is(['post'])) {
            $workers = TableRegistry::get('Workers');
            $worker = $workers->get($this->request->data['id']);
            $worker->locked = 1;
            if ($workers->save($worker)) {
                echo "success";
            } else {
                echo "error";
            }
        }
        $this->autoRender = false;
    }

    public function unlock_user()
    {
        if ($this->request->is(['post'])) {
            $workers = TableRegistry::get('Workers');
            $worker = $workers->get($this->request->data['id']);
            $worker->locked = 0;
            if ($workers->save($worker)) {
                echo "success";
            } else {
                echo "error";
            }
        }
        $this->autoRender = false;
    }

    public function delete_user()
    {
        if ($this->request->is(['post'])) {
            $workers = TableRegistry::get('Workers');
            $worker = $workers->get($this->request->data['id']);
            if ($workers->delete($worker)) {
                echo "success";
            } else {
                echo "error";
            }
        }
        $this->autoRender = false;
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function skinColor()
    {
        $request = $this->request->data;
        $color = $request['skin'];
        $id = $request['id'];
        $Workers = TableRegistry::get('Workers');
        $Worker = $Workers->get($id);
        $Worker->color = $color;
        $Workers->save($Worker);
        $this->Auth->setUser($Worker);
        $this->autoRender = false;
    }

    public function sidebarPosition()
    {
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $sidebar = $request->sidebar;
        $id = $request->id;
        $Workers = TableRegistry::get('Workers');
        $Worker = $Workers->get($id);
        $Worker->sidebar = $sidebar;
        $Workers->save($Worker);
        $this->Auth->setUser($Worker);
    }

    public function showTask()
    {
        $conn = ConnectionManager::get('default');
        $stmt = $conn->execute(
            'SELECT workers_tasks.task_id, workers.avatar, workers.first_name, 
            workers.last_name, tasks.text  FROM workers_tasks join tasks on workers_tasks.task_id = 
            tasks.id join workers on tasks.author = workers.id WHERE workers_tasks.status LIKE 0 
            AND workers_tasks.worker_id LIKE ' . $this->Auth->user('id'));
        $tasks = $stmt->fetchAll('assoc');
        echo json_encode($tasks);
        $this->autoRender = false;
    }

    public function taskDetails()
    {
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $id = $request->id;
        $conn = ConnectionManager::get('default');
        $stmt = $conn->execute(
            'SELECT workers.avatar, workers.first_name, workers.last_name, 
            tasks.text, tasks.date, tasks.id  FROM tasks join workers on tasks.author = 
            workers.id  WHERE tasks.id LIKE ' . $id);
        $taskDet = $stmt->fetchAll('assoc');
        echo json_encode($taskDet);
        $this->autoRender = false;
    }

    public function doTask()
    {
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $task_id = $request->task_id;
        $user_id = $this->Auth->user('id');
        $conn = ConnectionManager::get('default');
        $stmt = $conn->execute(
            'UPDATE workers_tasks SET status = 1 WHERE task_id LIKE ' . $task_id . ' AND worker_id LIKE ' . $user_id);
        $this->autoRender = false;
    }

    public function showNotifications()
    {
        $conn = ConnectionManager::get('default');
        $stmt = $conn->execute(
            'SELECT workers.avatar, workers.first_name, workers.last_name, notifications.text, notifications.date, 
            workers_notifications.notification_id, workers_notifications.status FROM workers_notifications join 
            notifications on workers_notifications.notification_id = notifications.id join workers on 
            notifications.author = workers.id WHERE workers_notifications.worker_id LIKE ' . $this->Auth->user('id') . ' 
            ORDER BY workers_notifications.status ASC LIMIT 10');
        $notifications = $stmt->fetchAll('assoc');
        echo json_encode($notifications);
        $this->autoRender = false;
    }

    public function notificationDetails()
    {
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $notification_id = $request->id;
        $user_id = $this->Auth->user('id');
        $conn = ConnectionManager::get('default');
        $utmt = $conn->execute(
            'UPDATE workers_notifications SET status = 1 WHERE notification_id LIKE ' . $notification_id . ' 
            AND worker_id LIKE ' . $user_id);
        $stmt = $conn->execute(
            'SELECT workers.avatar, workers.first_name, workers.last_name, notifications.text, 
            notifications.date  FROM notifications join workers on notifications.author = workers.id 
            WHERE notifications.id LIKE ' . $notification_id);
        $notificationDet = $stmt->fetchAll('assoc');
        echo json_encode($notificationDet);
        $this->autoRender = false;
    }

    public function refreshControllersList()
    {
        $accesses = $this->Accesses;
        $accesses->deleteAll([]);
        $resources = $this->getResources();
        foreach ($resources as $key => $value) {
            foreach ($value as $key_1 => $value_1) {
                $controller_name = $key_1;
                foreach ($value_1 as $action) {
                    echo $controller_name . " => " . $action . "<br>";

                    $access = $accesses->newEntity();

                    $access->controller = strtolower($controller_name);
                    $access->action = strtolower($action);

                    if ($accesses->save($access)) {
                        // The $article entity contains the id now
                        $id = $access->id;
                    }
                }
            }
        }
    }

}

class Base32Static
{

    private static $map = array(
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', //  7
        'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', // 15
        'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', // 23
        'Y', 'Z', '2', '3', '4', '5', '6', '7', // 31
        '='  // padding char
    );
    private static $flippedMap = array(
        'A' => '0', 'B' => '1', 'C' => '2', 'D' => '3', 'E' => '4', 'F' => '5', 'G' => '6', 'H' => '7',
        'I' => '8', 'J' => '9', 'K' => '10', 'L' => '11', 'M' => '12', 'N' => '13', 'O' => '14', 'P' => '15',
        'Q' => '16', 'R' => '17', 'S' => '18', 'T' => '19', 'U' => '20', 'V' => '21', 'W' => '22', 'X' => '23',
        'Y' => '24', 'Z' => '25', '2' => '26', '3' => '27', '4' => '28', '5' => '29', '6' => '30', '7' => '31'
    );

    /**
     *    Use padding false when encoding for urls
     *
     * @return base32 encoded string
     * @author Bryan Ruiz
     * */
    public static function encode($input, $padding = true)
    {
        if (empty($input))
            return "";
        $input = str_split($input);
        $binaryString = "";
        for ($i = 0; $i < count($input); $i++) {
            $binaryString .= str_pad(base_convert(ord($input[$i]), 10, 2), 8, '0', STR_PAD_LEFT);
        }
        $fiveBitBinaryArray = str_split($binaryString, 5);
        $base32 = "";
        $i = 0;
        while ($i < count($fiveBitBinaryArray)) {
            $base32 .= self::$map[base_convert(str_pad($fiveBitBinaryArray[$i], 5, '0'), 2, 10)];
            $i++;
        }
        if ($padding && ($x = strlen($binaryString) % 40) != 0) {
            if ($x == 8)
                $base32 .= str_repeat(self::$map[32], 6);
            else if ($x == 16)
                $base32 .= str_repeat(self::$map[32], 4);
            else if ($x == 24)
                $base32 .= str_repeat(self::$map[32], 3);
            else if ($x == 32)
                $base32 .= self::$map[32];
        }
        return $base32;
    }

    public static function decode($input)
    {
        if (empty($input))
            return;
        $paddingCharCount = substr_count($input, self::$map[32]);
        $allowedValues = array(6, 4, 3, 1, 0);
        if (!in_array($paddingCharCount, $allowedValues))
            return false;
        for ($i = 0; $i < 4; $i++) {
            if ($paddingCharCount == $allowedValues[$i] &&
                substr($input, -($allowedValues[$i])) != str_repeat(self::$map[32], $allowedValues[$i])
            )
                return false;
        }
        $input = str_replace('=', '', $input);
        $input = str_split($input);
        $binaryString = "";
        for ($i = 0; $i < count($input); $i = $i + 8) {
            $x = "";
            if (!in_array($input[$i], self::$map))
                return false;
            for ($j = 0; $j < 8; $j++) {
                $x .= str_pad(base_convert(@self::$flippedMap[@$input[$i + $j]], 10, 2), 5, '0', STR_PAD_LEFT);
            }
            $eightBits = str_split($x, 8);
            for ($z = 0; $z < count($eightBits); $z++) {
                $binaryString .= (($y = chr(base_convert($eightBits[$z], 2, 10))) || ord($y) == 48) ? $y : "";
            }
        }
        return $binaryString;
    }

}

class TokenAuth
{

    /**
     * verify
     *
     * @param string $secretkey Secret clue (base 32).
     * @return bool True if success, false if failure
     */
    public static function verify($secretkey, $code, $rangein30s = 1)
    {
        $key = base32static::decode($secretkey);

        $unixtimestamp = time() / 30;
        for ($i = -($rangein30s); $i <= $rangein30s; $i++) {
            $checktime = (int)($unixtimestamp + $i);
            $thiskey = self::oath_hotp($key, $checktime);

            if ((int)$code == self::oath_truncate($thiskey, 6)) {
                return true;
            }
        }
        return false;
    }

    public static function getTokenCode($secretkey, $rangein30s = 3)
    {
        $result = "";
        $key = base32static::decode($secretkey);

        $unixtimestamp = time() / 30;
        for ($i = -($rangein30s); $i <= $rangein30s; $i++) {
            $checktime = (int)($unixtimestamp + $i);
            $thiskey = self::oath_hotp($key, $checktime);

            $result = $result . " # " . self::oath_truncate($thiskey, 6);
        }
        return $result;
    }

    public static function getTokenCodeDebug($secretkey, $rangein30s = 3)
    {
        $result = "";
        print "<br/>SecretKey: $secretkey <br/>";
        $key = base32static::decode($secretkey);
        print "Key(base 32 decode): $key <br/>";

        $unixtimestamp = time() / 30;

        print "UnixTimeStamp (time()/30): $unixtimestamp <br/>";

        for ($i = -($rangein30s); $i <= $rangein30s; $i++) {
            $checktime = (int)($unixtimestamp + $i);

            print "Calculating oath_hotp from (int)(unixtimestamp +- 30sec offset): $checktime basing on secret key<br/>";
            $thiskey = self::oath_hotp($key, $checktime, true);
            print "======================================================<br/>";
            print "CheckTime: $checktime oath_hotp:" . $thiskey . "<br/>";

            $result = $result . " # " . self::oath_truncate($thiskey, 6, true);
        }
        return $result;
    }

    public static function getBarCodeUrl($username, $domain, $secretkey)
    {
        $url = "https://www.google.com/chart";
        $url = $url . "?chs=200x200&chld=M|0&cht=qr&chl=otpauth://totp/";
        $url = $url . $username . "@" . $domain . "%3Fsecret%3D" . $secretkey;
        return $url;
    }

    public static function generateRandomClue($length = 16)
    {
        $b32 = "234567QWERTYUIOPASDFGHJKLZXCVBNM";
        $s = "";

        for ($i = 0; $i < $length; $i++)
            $s .= $b32[rand(0, 31)];

        return $s;
    }

    private static function hotp_tobytestream($key)
    {
        $result = array();
        $last = strlen($key);
        for ($i = 0; $i < $last; $i = $i + 2) {
            $x = $key[$i] + $key[$i + 1];
            $x = strtoupper($x);
            $x = hexdec($x);
            $result = $result . chr($x);
        }
        return $result;
    }

    private static function oath_hotp($key, $counter, $debug = false)
    {
        $result = "";
        $orgcounter = $counter;

        $cur_counter = array(0, 0, 0, 0, 0, 0, 0, 0);
        if ($debug) {
            print "Packing counter $counter (" . dechex($counter) . ")into binary string - pay attention to hex representation of key and binary representation<br/>";
        }
        for ($i = 7; $i >= 0; $i--) {       // C for unsigned char, * for  repeating to the end of the input data
            $cur_counter[$i] = pack('C*', $counter);
            if ($debug) {
                print $cur_counter[$i] . "(" . dechex(ord($cur_counter[$i])) . ")" . " from $counter <br/>";
            }
            $counter = $counter >> 8;
        }
        if ($debug) {
            foreach ($cur_counter as $char) {
                print ord($char) . " ";
            }
            print "<br/>";
        }
        $binary = implode($cur_counter);


        // Pad to 8 characters
        str_pad($binary, 8, chr(0), STR_PAD_LEFT);
        if ($debug) {
            print "Prior to HMAC calculation pad with zero on the left until 8 characters.<br/>";
            print "Calculate sha1 HMAC(Hash-based Message Authentication Code http://en.wikipedia.org/wiki/HMAC).<br/>";
            print "hash_hmac ('sha1', $binary, $key)<br/>";
        }

        $result = hash_hmac('sha1', $binary, $key);
        if ($debug) {
            print "Result: $result <br/>";
        }


        return $result;
    }

    private static function oath_truncate($hash, $length = 6, $debug = false)
    {
        $result = "";
        // Convert to dec
        if ($debug) {
            print "converting hex hash into characters<br/>";
        }
        $hashcharacters = str_split($hash, 2);
        if ($debug) {
            print_r($hashcharacters);
            print "<br/>and convert to decimals:<br/>";
        }


        for ($j = 0; $j < count($hashcharacters); $j++) {
            $hmac_result[] = hexdec($hashcharacters[$j]);
        }
        if ($debug) {
            print_r($hmac_result);
        }


        //http://php.net/manual/ru/function.hash-hmac.php
        // adopted from brent at thebrent dot net 21-May-2009 08:17 comment

        $offset = $hmac_result[19] & 0xf;
        if ($debug) {
            print "Calculating offset as 19th element of hmac:" . $hmac_result[19] . "<br/>";
            print "offset:" . $offset;
        }

        $result = (
                (($hmac_result[$offset + 0] & 0x7f) << 24) |
                (($hmac_result[$offset + 1] & 0xff) << 16) |
                (($hmac_result[$offset + 2] & 0xff) << 8) |
                ($hmac_result[$offset + 3] & 0xff)
            ) % pow(10, $length);

        return $result;
    }
}