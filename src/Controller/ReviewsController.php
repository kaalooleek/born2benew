<?php

namespace App\Controller;

use Cake\ORM\TableRegistry;
use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;

/**
 * Reviews Controller
 *
 * @property \App\Model\Table\ReviewsTable $Reviews
 */
class ReviewsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
//      $this->paginate = [
//            'contain' => ['product_id']
//        ];
//        
        $reviews = $this->paginate($this->Reviews);
        $this->set(compact('reviews'));
        $this->set('_serialize', ['reviews']);
    }

    /**
     * View method
     *
     * @param string|null $id Review id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null, $action = null) {
        /* $review = $this->Reviews->get($id, [
          'contain' => ['Products', 'Websites']
          ]); */
        //$review = $this->Reviews->get($id);

        if ($this->request->is('post')) {
            $response = [];
            $producttable = TableRegistry::get("products");
            if ($action == 'next') {
                $review = $this->Reviews->find()->where(['Reviews.id' => $id])->first();
                $product = $producttable->find()->where(['Products.id' => $review['product_id']])->first();
                while ($review == null) {
                    $id++;
                    $review = $this->Reviews->find()->where(['Reviews.id' => $id])->first();
                    $product = $producttable->find()->where(['Products.id' => $review['product_id']])->first();
                }
                $response['review'] = $review;
                $response['product'] = $product;
            } elseif ($action == 'previous') {
                $review = $this->Reviews->find()->where(['Reviews.id' => $id])->first();
                $product = $producttable->find()->where(['Products.id' => $review['product_id']])->first();
                while ($review == null && $id != 1) {
                    if ($id > 1) {
                        $id--;
                    }
                    $review = $this->Reviews->find()->where(['Reviews.id' => $id])->first();
                    $product = $producttable->find()->where(['Products.id' => $review['product_id']])->first();
                }
                $response['review'] = $review;
                $response['product'] = $product;
            }


            echo json_encode($response);
            $this->autoRender = false;
        }
        $review = $this->Reviews->find()->where(['Reviews.id' => $id])->first();
        $this->set('review', $review);
        $this->set('_serialize', ['review']);
        $producttable = TableRegistry::get("products");
        $products = $producttable->find()->where(['Products.id' => $review['product_id']])->first();
        $this->set('products', $products);
        $img_path = 'http://born2be.pl/img/products/' . $products['code'] . '/1.jpg';
        $pht = '<img style="min-width:150px; width:150px;" src=' . $img_path . '></img>';
        $this->set('photo', $pht);
    }

    /**Edit method ...**/
    public function edit($id = null) {
        if ($this->request->is('post')) {
            $reviewsTable = $this->Reviews;
            $review = $reviewsTable->get($this->request->data['object']['id']); // Return article with id 12

            $review[$this->request->data['object']['name']] = $this->request->data['object']['value'];
            if ($reviewsTable->save($review)) {
                echo 'success';
            } else {
                echo 'error';
            }
            $this->autoRender = false;
        } else {
            $review = $this->Reviews->get($id);
            if ($review->nip == '') {
                $review->nip = "-";
            }
            if ($review->correction == "true") {
                $review->correction = "Tak";
            } else {
                $review->correction = "Nie";
            }
            if ($review->status == "paid") {
                $review->status = "<span class='label label-success'>Zapłacono</span>";
            } else {
                $review->status = "<span class='label label-warning'>Nie zapłacono</span>";
            }
            if ($review->payment_type == "przelew") {
                $review->payment_type = '<span class="fa fa-fw fa-credit-card"></span> ' . $review->payment_type;
            } else {
                $review->payment_type = '<span class="fa fa-fw fa-money"></span> ' . $review->payment_type;
            }

            $this->set('review', $review);
            /*$this->set('_serialize', ['review']);*/
        }
    }
    /*
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */

    public function add($id = null) {

        $product = $this->Reviews->Products->find()->where(['Products.id' => $id])->first();
        /*
        if($product == null){

            $this->Flash->error(__('Wpisz id produktu po slashu aby dodać opinie'));
            
        }
        */
        
        $this->set('product', $product);

        $review = $this->Reviews->newEntity();
        if ($this->request->is('post')) {

            $this->request->data['product_id'] =  $id;
            $review = $this->Reviews->patchEntity($review, $this->request->data);
            if ($this->Reviews->save($review)) {
                $this->Flash->success(__('Opinia została zapisana :)'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Wystąpił błąd podczas zapisywania :('));
            }
        }
        $products = $this->Reviews->Products->find('list', ['limit' => 200]);
        //$websites = $this->Reviews->Websites->find('list', ['limit' => 200]);
        $this->set(compact('review', 'products'));
        $this->set('_serialize', ['review']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Review id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $review = $this->Reviews->get($id);
        if ($this->Reviews->delete($review)) {
            echo "success";
        } else {
            echo "error";
        }
        $this->autoRender = false;
    }

    public function getReview($id = null){
        $review = $this->Reviews->find()->where(['id' => $id])->first();
        echo json_encode($review);
        $this->autoRender = false;
    }

    public function getProducts()
    {
        if ($this->request->is('get')) {
            $products = TableRegistry::get('Products')->find()->select(['id', 'category_id', 'name', 'sell_price', 'code', 'keywords'])->limit(50)->toArray();
            echo json_encode($products);
        }
        $this->autoRender = false;
    }

    public function createReview() {
        if ($this->request->is('post')) {
            $length = count($this->request->data['ids']);
            $success = false;
            for($x = 0; $x < $length; $x++ )
            {
                $new_review = $this->Reviews->newEntity();
                $new_review->review = $this->request->data['review'];
                $new_review->name = $this->request->data['name'];
                $new_review->city = $this->request->data['city'];
                $new_review->rating = $this->request->data['rating'];
                $new_review->comfort = $this->request->data['comfort'];
                $new_review->style = $this->request->data['style'];
                $new_review->sizing = $this->request->data['sizing'];
                $new_review->thumbs_up = 0;
                $new_review->thumbs_down = 0;
                $new_review->status = 'available';
                $new_review->product_id = $this->request->data['ids'][$x];

                if($this->Reviews->save($new_review)){
                    $success = true;
                }
                else {
                    $success = false;
                }
            }
            if($success) {
                echo "success";
            }
            $this->autoRender = false;
        }
    }
}
