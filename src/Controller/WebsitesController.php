<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Websites Controller
 *
 * @property \App\Model\Table\WebsitesTable $Websites
 */
class WebsitesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $websites = $this->paginate($this->Websites);

        $this->set(compact('websites'));
        $this->set('_serialize', ['websites']);
    }

    /**
     * View method
     *
     * @param string|null $id Website id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $website = $this->Websites->get($id, [
            'contain' => ['Coupons', 'Imgs', 'Orders', 'Reviews', 'Subscribers']
        ]);

        $this->set('website', $website);
        $this->set('_serialize', ['website']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $website = $this->Websites->newEntity();
        if ($this->request->is('post')) {
            $website = $this->Websites->patchEntity($website, $this->request->data);
            if ($this->Websites->save($website)) {
                $this->Flash->success(__('The website has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The website could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('website'));
        $this->set('_serialize', ['website']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Website id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $website = $this->Websites->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $website = $this->Websites->patchEntity($website, $this->request->data);
            if ($this->Websites->save($website)) {
                $this->Flash->success(__('The website has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The website could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('website'));
        $this->set('_serialize', ['website']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Website id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $website = $this->Websites->get($id);
        if ($this->Websites->delete($website)) {
            $this->Flash->success(__('The website has been deleted.'));
        } else {
            $this->Flash->error(__('The website could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
