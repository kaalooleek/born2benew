<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use App\Model\Entity\Invoice;
use App\Model\Entity\Item;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use DateTime;
use CakePdf\Pdf\CakePdf;

/**
 * Invoices Controller
 *
 * @property \App\Model\Table\InvoicesTable $Invoices
 */
class InvoicesController extends AppController {

    public function index() {
        $invoices = $this->paginate($this->Invoices);
        $parsedInvoices = array();
        foreach ($invoices as $invoice) {
            $invoice->label = $invoice->name;
            $invoice->value = $invoice->id;
            $invoice->sell_date = date("Y-m-d", strtotime($invoice->sell_date));
            $invoice->invoice_date = date("Y-m-d", strtotime($invoice->invoice_date));
            if ($invoice->nip == '') {
                $invoice->nip = "-";
            }
            if ($invoice->correction == "true") {
                $invoice->correction = "Tak";
            } else {
                $invoice->correction = "Nie";
            }
            if ($invoice->status == "paid") {
                $invoice->status = "<span class='label label-success'>Zapłacono</span>";
            } else {
                $invoice->status = "<span class='label label-warning'>Nie zapłacono</span>";
            }
            array_push($parsedInvoices, $invoice);
        }
        $invoices = $parsedInvoices;
        $this->set(compact('invoices'));
        $this->set('_serialize', ['invoices']);
    }

    /**
     * View method
     *
     * @param string|null $id Invoice id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        if ($this->request->is('post')) {
            $invoicesTable = $this->Invoices;
            $invoice = $invoicesTable->get($this->request->data['object']['id']); // Return article with id 12

            $invoice[$this->request->data['object']['name']] = $this->request->data['object']['value'];
            if ($invoicesTable->save($invoice)) {
                echo 'success';
            } else {
                echo 'error';
            }
            $this->autoRender = false;
        } else {
            $invoice = $this->Invoices->get($id, [
                'contain' => ['Transfers', 'Invoices', 'Items']
            ]);

            $invoice->sell_date = date("Y-m-d", strtotime($invoice->sell_date));
            $invoice->invoice_date = date("Y-m-d", strtotime($invoice->invoice_date));
            if ($invoice->nip == '') {
                $invoice->nip = "-";
            }
            if ($invoice->correction == "true") {
                $invoice->correction = "Tak";
            } else {
                $invoice->correction = "Nie";
            }
            if ($invoice->status == "paid") {
                $invoice->status = "<span class='label label-success'>Zapłacono</span>";
            } else {
                $invoice->status = "<span class='label label-warning'>Nie zapłacono</span>";
            }
            if ($invoice->payment_type == "przelew") {
                $invoice->payment_type = '<span class="fa fa-fw fa-credit-card"></span> ' . $invoice->payment_type;
            } else {
                $invoice->payment_type = '<span class="fa fa-fw fa-money"></span> ' . $invoice->payment_type;
            }

            $this->set('invoice', $invoice);
            $this->set('_serialize', ['invoice']);
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {


        if ($this->request->is('post')) {
            $invoice = $this->Invoices->newEntity();
            $items_table = TableRegistry::get("Items");
            $item = $items_table->newEntity();
            debug($this->request->data);
            $faktura = new Invoice($invoice->parseInvoice($this->request->data['faktura']));

            if ($this->Invoices->save($faktura)) {
                debug($faktura->id);
            } else {
                echo "Error";
            }
            $towary = $this->request->data['towary'];
            foreach ($towary as $towar) {
                $parsed_item = new Item($item->parseItem($towar, $faktura->id, $this->request->data['faktura']['waluta']));
                $items_table->save($parsed_item);
            }
            $this->autoRender = false;
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Invoice id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {

    }

    /**
     * Delete method
     *
     * @param string|null $id Invoice id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $invoice = $this->Invoices->get($id);
        if ($this->Invoices->delete($invoice)) {
            $this->Flash->success(__('The invoice has been deleted.'));
        } else {
            $this->Flash->error(__('The invoice could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function getinvoices() {
        if ($this->request->is('post')) {
            $query = $this->request->data['search']['query'];
            $parsedInvoices = array();
            $invoices = $this->Invoices->find()->where(['address LIKE' => '%' . $query . '%'])->orWhere(['name LIKE' => '%' . $query . '%'])->orWhere(['name LIKE' => '%' . $query . '%'])->orderDesc('Invoices.id')->limit(5);
            foreach ($invoices as $invoice) {
                $invoice->label = $invoice->name;
                $invoice->value = $invoice->id;
                $invoice->sell_date = date("Y-m-d", strtotime($invoice->sell_date));
                $invoice->invoice_date = date("Y-m-d", strtotime($invoice->invoice_date));
                if ($invoice->nip == '') {
                    $invoice->nip = "-";
                }
                if ($invoice->correction == "true") {
                    $invoice->correction = "Tak";
                } else {
                    $invoice->correction = "Nie";
                }
                if ($invoice->status == "paid") {
                    $invoice->status = "Zapłacono";
                } else {
                    $invoice->status = "";
                }
                array_push($parsedInvoices, $invoice);
            }
            echo json_encode($parsedInvoices);
            $this->autoRender = false;
        }
    }

    private function get_md5($invoice) {
        return md5($invoice['Invoice']['id'] . $invoice['Invoice']['order_id'] . 'ddVkzos35tg');
    }

    public function viewInvoice($id) {
        $this->viewBuilder('print_default');

        $invoice = $this->Invoices->find()->where(['Invoices.id' => $id])->toArray();

        $total = 0;
        debug($invoice);
        foreach ($invoice['Item'] as $item) {
            $total = $total + $item['Item']['price'] * $item['Item']['quantity'];
        }

        $invoice['Invoice']['total'] = $total;
        $invoice['Invoice']['slownie'] = $this->slownie($total);
        $invoices[] = $invoice;
        $this->set(compact('invoices'));
    }

	public function getInvoice($id = null){
		$invoice = $this->Invoices->find()->where(['Invoices.id' => $id])->contain(['Items'])->first();
		$date = new DateTime($invoice->invoice_date);
		$invoice->invoice_date = $date->format('Y-m-d');
		echo json_encode($invoice);
		$this->autoRender = false;
	}

    public function pdfInvoice($id = null) {
		if($id != null) {
			$this->pdfConfig = array(
				'orientation' => 'portrait',
				'pageSize' => 'A4',
				'download' => true
			);
			$CakePdf = new \CakePdf\Pdf\CakePdf();
			$CakePdf->template('Invoices/view');
			$invoice = $this->Invoices->get($id, [
				'contain' => ['Items']
			]);
			$this->set('invoice', $invoice);
			$CakePdf->viewVars(array('invoice' => $invoice));
			$pdf = $CakePdf->write(APP . 'files' . DS . 'invoices' . DS . $invoice->id . '.pdf');
			$path = 'src/files/invoices/' . $invoice->id . '.pdf';
			echo $path;
			$this->autoRender = false;
		}
		else {
			echo "Failed to generate PDF";
		}
    }

    function d2w( $digits )
	{
		$jednosci = Array( 'zero', 'jeden', 'dwa', 'trzy', 'cztery', 'pięć', 'sześć', 'siedem', 'osiem', 'dziewięć' );
		$dziesiatki = Array( '', 'dziesięć', 'dwadzieścia', 'trzydzieści', 'czterdzieści', 'piećdziesiąt', 'sześćdziesiąt', 'siedemdziesiąt', 'osiemdziesiąt', 'dziewiećdziesiąt' );
		$setki = Array( '', 'sto', 'dwieście', 'trzysta', 'czterysta', 'piećset', 'sześćset', 'siedemset', 'osiemset', 'dziewiećset' );
		$nastki = Array( 'dziesieć', 'jedenaście', 'dwanaście', 'trzynaście', 'czternaście', 'piętnaście', 'szesnaście', 'siedemnaście', 'osiemnaście', 'dzięwietnaście' );
		$tysiace = Array( 'tysiąc', 'tysiące', 'tysięcy' );

		$digits = (string) $digits;
		$digits = strrev( $digits );
		$i = strlen( $digits );

		$string = '';
		debug($i);
		if( $i > 5 && $digits[5] > 0 )
		$string .= $setki[ $digits[5] ] . ' ';
		if( $i > 4 && $digits[4] > 1 )
		$string .= $dziesiatki[ $digits[4] ] . ' ';
		elseif( $i > 3 && $digits[4] == 1 )
		$string .= $nastki[$digits[3]] . ' ';
		if( $i > 3 && $digits[3] > 0 && $digits[4] != 1 )
		$string .= $jednosci[ $digits[3] ] . ' ';

		$tmpStr = substr( strrev( $digits ), 0, -3 );
		if( strlen( $tmpStr ) > 0 )
		{
			$tmpInt = (int) $tmpStr;
			if( $tmpInt == 1 )
			$string .= $tysiace[0] . ' ';
			elseif( ( $tmpInt % 10 > 1 && $tmpInt % 10 < 5 ) && ( $tmpInt < 10 || $tmpInt > 20 ) )
			$string .= $tysiace[1] . ' ';
			else
			$string .= $tysiace[2] . ' ';
		}

		if( $i > 2 && $digits[2] > 0 )
		$string .= $setki[$digits[2]] . ' ';
		if( $i > 1 && $digits[1] > 1 )
		$string .= $dziesiatki[$digits[1]] . ' ';
		elseif( $i > 0 && $digits[1] == 1 )
		$string .= $nastki[$digits[0]] . ' ';
		if( $digits[0] > 0 && $digits[1] != 1 )
		$string .= $jednosci[$digits[0]] . ' ';

		return $string;

	}

	function slownie($kwota)
	{
		$kwota = str_replace(',', '.', $kwota);
		if(floor($kwota) == $kwota)
			$kwota = $kwota.',00';
		else
			$kwota = str_replace('.', ',', $kwota);
		$zl = array("złotych", "złoty", "złote");
		$gr = array("groszy", "grosz", "grosze");
		$kwotaArr = explode( ',', $kwota );
		$ostZl = substr($kwotaArr[0], -1, 1);
		switch($ostZl){
			case "0":
				$zlote = $zl[0];
				break;

			case "1":
				$ost2Zl = substr($kwotaArr[0], -2, 2);


				if($kwotaArr[0] == "1"){
					$zlote = $zl[1];
				}
				elseif($ost2Zl == "01"){
					$zlote = $zl[0];
				}
				else{
					$zlote = $zl[0];
				}
				break;

			case "2":
				$ost2Zl = substr($kwotaArr[0], -2, 2);
				if($ost2Zl == "12"){
					$zlote = $zl[0];
				}
				else{
					$zlote = $zl[2];
				}
				break;

			case "3":
				$ost2Zl = substr($kwotaArr[0], -2, 2);
				if($ost2Zl == "13"){
					$zlote = $zl[0];
				}
				else{
					$zlote = $zl[2];
				}
				break;

			case "4":
				$ost2Zl = substr($kwotaArr[0], -2, 2);
				if($ost2Zl == "14"){
					$zlote = $zl[0];
				}
				else{
					$zlote = $zl[2];
				}
				break;

			case "5":
				$zlote = $zl[0];
				break;

			case "6":
				$zlote = $zl[0];
				break;

			case "7":
				$zlote = $zl[0];
				break;

			case "8":
				$zlote = $zl[0];
				break;

			case "9":
				$zlote = $zl[0];
				break;
		}




		############### PONIZEJ ||VVV|| GROSZE





		$ostGr = substr($kwotaArr[1], -1, 1);
		switch($ostGr){
			case "0":
				$grosze = $gr[0];
				break;

			case "1":
				$ost2Gr = substr($kwotaArr[1], -2, 2);


				if($kwotaArr[0] == "1"){
					$grosze = $gr[1];
				}
				elseif($ost2Gr == "01"){
					$grosze = $gr[1];
				}
				else{
					$grosze = $gr[0];
				}
				break;

			case "2":
				$ost2Gr = substr($kwotaArr[1], -2, 2);
				if($ost2Gr == "12"){
					$grosze = $gr[0];
				}
				else{
					$grosze = $gr[2];
				}
				break;

			case "3":
				$ost2Gr = substr($kwotaArr[1], -2, 2);
				if($ost2Gr == "13"){
					$grosze = $gr[0];
				}
				else{
					$grosze = $gr[2];
				}
				break;

			case "4":
				$ost2Gr = substr($kwotaArr[1], -2, 2);
				if($ost2Gr == "14"){
					$grosze = $gr[0];
				}
				else{
					$grosze = $gr[2];
				}
				break;

			case "5":
				$grosze = $gr[0];
				break;

			case "6":
				$grosze = $gr[0];
				break;

			case "7":
				$grosze = $gr[0];
				break;

			case "8":
				$grosze = $gr[0];
				break;

			case "9":
				$grosze = $gr[0];
				break;
		}

		return( $this->d2w($kwotaArr[0] ) . ' '.$zlote.' i '.((floor($kwotaArr[1]) == 0) ? 'zero groszy' : ($this->d2w( $kwotaArr[1] ).$grosze)) );
	}

}
