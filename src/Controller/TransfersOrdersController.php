<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TransfersOrders Controller
 *
 * @property \App\Model\Table\TransfersOrdersTable $TransfersOrders
 */
class TransfersOrdersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Orders', 'Transfers']
        ];
        $transfersOrders = $this->paginate($this->TransfersOrders);

        $this->set(compact('transfersOrders'));
        $this->set('_serialize', ['transfersOrders']);
    }

    /**
     * View method
     *
     * @param string|null $id Transfers Order id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $transfersOrder = $this->TransfersOrders->get($id, [
            'contain' => ['Orders', 'Transfers']
        ]);

        $this->set('transfersOrder', $transfersOrder);
        $this->set('_serialize', ['transfersOrder']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $transfersOrder = $this->TransfersOrders->newEntity();
        if ($this->request->is('post')) {
            $transfersOrder = $this->TransfersOrders->patchEntity($transfersOrder, $this->request->data);
            if ($this->TransfersOrders->save($transfersOrder)) {
                $this->Flash->success(__('The transfers order has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The transfers order could not be saved. Please, try again.'));
            }
        }
        $orders = $this->TransfersOrders->Orders->find('list', ['limit' => 200]);
        $transfers = $this->TransfersOrders->Transfers->find('list', ['limit' => 200]);
        $this->set(compact('transfersOrder', 'orders', 'transfers'));
        $this->set('_serialize', ['transfersOrder']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Transfers Order id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $transfersOrder = $this->TransfersOrders->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $transfersOrder = $this->TransfersOrders->patchEntity($transfersOrder, $this->request->data);
            if ($this->TransfersOrders->save($transfersOrder)) {
                $this->Flash->success(__('The transfers order has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The transfers order could not be saved. Please, try again.'));
            }
        }
        $orders = $this->TransfersOrders->Orders->find('list', ['limit' => 200]);
        $transfers = $this->TransfersOrders->Transfers->find('list', ['limit' => 200]);
        $this->set(compact('transfersOrder', 'orders', 'transfers'));
        $this->set('_serialize', ['transfersOrder']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Transfers Order id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $transfersOrder = $this->TransfersOrders->get($id);
        if ($this->TransfersOrders->delete($transfersOrder)) {
            $this->Flash->success(__('The transfers order has been deleted.'));
        } else {
            $this->Flash->error(__('The transfers order could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
