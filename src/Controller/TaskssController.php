<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Taskss Controller
 *
 * @property \App\Model\Table\TaskssTable $Taskss
 */
class TaskssController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->tasks = TableRegistry::get('taskss')->find('all')->contain(['Groups', 'Workers']);
        $this->paginate = [
            'contain' => ['Groups','Workers']
        ];
        $this->set('tasks', $this->paginate($this->tasks));
    }

    /**
     * View method
     *
     * @param string|null $id Tasks id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tasks = $this->Taskss->get($id, [
            'contain' => []
        ]);

        $this->set('tasks', $tasks);
        $this->set('_serialize', ['tasks']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tasks = $this->Taskss->newEntity();
        if ($this->request->is('post')) {
            $tasks = $this->Taskss->patchEntity($tasks, $this->request->data);
            if ($this->Taskss->save($tasks)) {
                $this->Flash->success(__('The tasks has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The tasks could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('tasks'));
        $this->set('_serialize', ['tasks']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tasks id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tasks = $this->Taskss->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tasks = $this->Taskss->patchEntity($tasks, $this->request->data);
            if ($this->Taskss->save($tasks)) {
                $this->Flash->success(__('The tasks has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The tasks could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('tasks'));
        $this->set('_serialize', ['tasks']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tasks id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tasks = $this->Taskss->get($id);
        if ($this->Taskss->delete($tasks)) {
            $this->Flash->success(__('The tasks has been deleted.'));
        } else {
            $this->Flash->error(__('The tasks could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
