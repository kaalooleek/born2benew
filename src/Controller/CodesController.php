<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Codes Controller
 *
 * @property \App\Model\Table\CodesTable $Codes
 */
class CodesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Orders', 'Coupons']
        ];
        $codes = $this->paginate($this->Codes);

        $this->set(compact('codes'));
        $this->set('_serialize', ['codes']);
    }

    /**
     * View method
     *
     * @param string|null $id Code id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $code = $this->Codes->get($id, [
            'contain' => ['Users', 'Orders', 'Coupons']
        ]);

        $this->set('code', $code);
        $this->set('_serialize', ['code']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $code = $this->Codes->newEntity();
        if ($this->request->is('post')) {
            $code = $this->Codes->patchEntity($code, $this->request->data);
            if ($this->Codes->save($code)) {
                $this->Flash->success(__('The code has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The code could not be saved. Please, try again.'));
            }
        }
        $users = $this->Codes->Users->find('list', ['limit' => 200]);
        $orders = $this->Codes->Orders->find('list', ['limit' => 200]);
        $coupons = $this->Codes->Coupons->find('list', ['limit' => 200]);
        $this->set(compact('code', 'users', 'orders', 'coupons'));
        $this->set('_serialize', ['code']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Code id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $code = $this->Codes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $code = $this->Codes->patchEntity($code, $this->request->data);
            if ($this->Codes->save($code)) {
                $this->Flash->success(__('The code has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The code could not be saved. Please, try again.'));
            }
        }
        $users = $this->Codes->Users->find('list', ['limit' => 200]);
        $orders = $this->Codes->Orders->find('list', ['limit' => 200]);
        $coupons = $this->Codes->Coupons->find('list', ['limit' => 200]);
        $this->set(compact('code', 'users', 'orders', 'coupons'));
        $this->set('_serialize', ['code']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Code id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $code = $this->Codes->get($id);
        if ($this->Codes->delete($code)) {
            $this->Flash->success(__('The code has been deleted.'));
        } else {
            $this->Flash->error(__('The code could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
