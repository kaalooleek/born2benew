<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CartsSizes Controller
 *
 * @property \App\Model\Table\CartsSizesTable $CartsSizes
 */
class CartsSizesController extends AppController
{

    /**
     * Index method asd asd 
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Carts', 'Sizes']
        ];
        $cartsSizes = $this->paginate($this->CartsSizes);

        $this->set(compact('cartsSizes'));
        $this->set('_serialize', ['cartsSizes']);
    }

    /**
     * View method
     *
     * @param string|null $id Carts Size id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $cartsSize = $this->CartsSizes->get($id, [
            'contain' => ['Carts', 'Sizes']
        ]);

        $this->set('cartsSize', $cartsSize);
        $this->set('_serialize', ['cartsSize']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $cartsSize = $this->CartsSizes->newEntity();
        if ($this->request->is('post')) {
            $cartsSize = $this->CartsSizes->patchEntity($cartsSize, $this->request->data);
            if ($this->CartsSizes->save($cartsSize)) {
                $this->Flash->success(__('The carts size has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The carts size could not be saved. Please, try again.'));
            }
        }
        $carts = $this->CartsSizes->Carts->find('list', ['limit' => 200]);
        $sizes = $this->CartsSizes->Sizes->find('list', ['limit' => 200]);
        $this->set(compact('cartsSize', 'carts', 'sizes'));
        $this->set('_serialize', ['cartsSize']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Carts Size id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cartsSize = $this->CartsSizes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cartsSize = $this->CartsSizes->patchEntity($cartsSize, $this->request->data);
            if ($this->CartsSizes->save($cartsSize)) {
                $this->Flash->success(__('The carts size has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The carts size could not be saved. Please, try again.'));
            }
        }
        $carts = $this->CartsSizes->Carts->find('list', ['limit' => 200]);
        $sizes = $this->CartsSizes->Sizes->find('list', ['limit' => 200]);
        $this->set(compact('cartsSize', 'carts', 'sizes'));
        $this->set('_serialize', ['cartsSize']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Carts Size id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $cartsSize = $this->CartsSizes->get($id);
        if ($this->CartsSizes->delete($cartsSize)) {
            $this->Flash->success(__('The carts size has been deleted.'));
        } else {
            $this->Flash->error(__('The carts size could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
