<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 */
class OrdersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('App');
    }
    public function index($section_id = null)
    {
        $this->loadModel('SectionsConditions');
        $this->loadModel('Conditions');
        $conditions = $this->SectionsConditions->find()->contain(['Conditions'])->where(['SectionsConditions.section_id' => $section_id])->toArray();
        $conditions_parsed = [];
        foreach($conditions as $condition){
            $conditions_parsed[$condition->condition['column_name']] = $condition->condition['value'];
        }
        $orders = $this->paginate($this->Orders->find()->contain(['Carriers'])->where($conditions_parsed));
        $this->set(compact('orders'));
        $this->set('_serialize', ['orders']);
    }
    public function orderDetails() {
        $request = $this->request->data;
        $id = $request['id'];
        $order = TableRegistry::get('Orders')->find()->where(['id' => $id]);
        echo json_encode($order);
        $this->autoRender = false;
    }

    public function countOrders($section_id = null){
        $this->loadModel('SectionsConditions');
        $this->loadModel('Conditions');
        $conditions = $this->SectionsConditions->find()->contain(['Conditions'])->where(['SectionsConditions.section_id' => $section_id])->toArray();
        $conditions_parsed = [];
        foreach($conditions as $condition){
            $conditions_parsed[$condition->condition['column_name']] = $condition->condition['value'];
        }
        $count = $this->Orders->find()->contain(['Carriers'])->where($conditions_parsed)->count();
        echo json_encode($count);
        $this->autoRender = false;
    }
}