<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Cake\Network\Session;
use Cake\Core\Configure;
use Cake\I18n\Time;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        Email::configTransport('gmail', [
            'host' => 'ssl://smtp.gmail.com',
            'port' => 465,
            'username' => 'lorak1234@gmail.com',
            'password' => 'gprgflobtxxzytgr',
            'className' => 'Smtp',
            'log' => true,
            'context' => [
                'ssl' => [
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                ]
            ]
        ]);

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Cookie');

        if ($this->Cookie->read('twostep_key') != Configure::read('AUTH_KEY') && $this->Cookie->read('twostep_passed')) {
            $this->Cookie->write('twostep_passed', null);
            $this->redirect(["controller" => "users", "action" => "logout"]);
        }

        $this->loadComponent('Auth', [
            'authorize' => 'Controller',
            'authenticate' => [
                'form' => [
                    'userModel' => 'Workers',
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ]
                ]
            ],
            'unauthorizedRedirect' => [
                'controller' => 'users',
                'action' => 'dashboard',
                'prefix' => false
            ],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login'
            ]
        ]);
        $this->loadModel('Users');
        $this->loadModel('Sections');
        $this->loadModel('Orders');
        $this->loadModel('Accesses');
        $this->loadModel('WorkersAccesses');
        $sections = $this->Sections->find()->all()->toArray();
        $user = json_decode(json_encode($this->request->session()->read('Auth.User')), FALSE);
        Time::$defaultLocale = 'en-En';
        Time::setToStringFormat('YYYY-MM-dd');
        $data = Time::now();
        if ($this->Cookie->read('welcome_date') != $data && $this->request->session()->read('Auth.User')) {
            $this->Cookie->write('welcome_show', true);
            $this->Cookie->write('welcome_date', $data);
            $user->show_welcome = $this->Cookie->read('welcome_show');
        } else {
            $this->Cookie->write('welcome_show', false);
            @$user->show_welcome = $this->Cookie->read('welcome_show');
        }
        if ($this->request->session()->read('Auth.User')) {
            $user_accesses = $this->WorkersAccesses->find()->where(['worker_id' => $user->id])->contain(['Accesses'])->toArray();
            $accesses = [];
            foreach ($user_accesses as $value) {
                $access = [];
                $access['controller'] = $value['access']['controller'];
                $access['action'] = $value['access']['action'];
                array_push($accesses, $access);
            }
            $this->user_accesses = $accesses;
            $menu = ['views' => [
                ['label' => 'Faktury',
                    'items' => [
                        ['label' => 'Lista faktur', 'visibility' => true, 'link' => ['controller' => 'Invoices', 'action' => 'index']],
                    ]
                ],
                ['label' => 'Magazyn',
                    'items' => [
                        ['label' => 'Lista produktów', 'visibility' => true, 'link' => ['controller' => 'Products', 'action' => 'index']],
                        ['label' => 'Dodaj produkty', 'visibility' => true, 'link' => ['controller' => 'Products', 'action' => 'add']],
                    ]
                ], ['label' => 'Wiadomości SMS',
                    'items' => [
                        ['label' => 'Lista wiadomości', 'visibility' => true, 'link' => ['controller' => 'Texts', 'action' => 'index']],
                    ]
                ],
                ['label' => 'Oceny',
                    'items' => [
                        ['label' => 'Lista Ocen', 'visibility' => true, 'link' => ['controller' => 'Reviews', 'action' => 'index']],
                        ['label' => 'Nowa Ocena', 'visibility' => true, 'link' => ['controller' => 'Reviews', 'action' => 'add']]
                    ]
                ],
                ['label' => 'Pracownicy',
                    'items' => [
                        ['label' => 'Lista pracowników', 'visibility' => true, 'link' => ['controller' => 'Users', 'action' => 'index']],
                        ['label' => 'Dodaj konto pracownika', 'visibility' => true, 'link' => ['controller' => 'Users', 'action' => 'add']],
                    ]
                ],
            ]
            ];
            $items = [];
            foreach($sections as $section){
                $new_section = ['label' => $section['name'], 'visibility' => true, 'link' => ['controller' => 'Orders', 'action' => 'index', $section['id']], 'section_id' => $section['id']];
                array_push($items, $new_section);
            }
            $sections_items = ['label' => 'Zamówienia', 'items' => $items];
            array_push($menu['views'], $sections_items);

            foreach ($menu['views'] as $key => $value) {
                foreach ($value['items'] as $key_1 => $value_1) {
                    $this->checked_ctrl = strtolower($value_1['link']['controller']);
                    $this->checked_action = strtolower($value_1['link']['action']);
                    if ($this->hasAccess()) {
                        $menu['views'][$key]['items'][$key_1]['visibility'] = true;
                    } else {
                        $menu['views'][$key]['items'][$key_1]['visibility'] = true;
                    }
                }
            }
            $this->set('menu', $menu);
            $this->set('auth_user', $user);
        }
    }

    public function hasAccess()
    {
        foreach ($this->user_accesses as $access) {
            if ($access['controller'] == $this->checked_ctrl && $access['action'] == $this->checked_action) {
                return true;
            }
        }
        return false;
    }

    public function isAuthorized($user = null)
    {
        /* if ($this->WorkersAccesses->find('all')->contain(['Accesses'])->where(['WorkersAccesses.worker_id' => '2', 'Accesses.controller' => $this->request->controller, 'Accesses.action' => $this->request->action])->count() > 0) {
          return true;
          } */
        return true;
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

}
