<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Wdays Controller
 *
 * @property \App\Model\Table\WdaysTable $Wdays
 */
class WdaysController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $wdays = $this->paginate($this->Wdays);

        $this->set(compact('wdays'));
        $this->set('_serialize', ['wdays']);
    }

    /**
     * View method
     *
     * @param string|null $id Wday id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $wday = $this->Wdays->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('wday', $wday);
        $this->set('_serialize', ['wday']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $wday = $this->Wdays->newEntity();
        if ($this->request->is('post')) {
            $wday = $this->Wdays->patchEntity($wday, $this->request->data);
            if ($this->Wdays->save($wday)) {
                $this->Flash->success(__('The wday has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The wday could not be saved. Please, try again.'));
            }
        }
        $users = $this->Wdays->Users->find('list', ['limit' => 200]);
        $this->set(compact('wday', 'users'));
        $this->set('_serialize', ['wday']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Wday id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $wday = $this->Wdays->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $wday = $this->Wdays->patchEntity($wday, $this->request->data);
            if ($this->Wdays->save($wday)) {
                $this->Flash->success(__('The wday has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The wday could not be saved. Please, try again.'));
            }
        }
        $users = $this->Wdays->Users->find('list', ['limit' => 200]);
        $this->set(compact('wday', 'users'));
        $this->set('_serialize', ['wday']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Wday id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $wday = $this->Wdays->get($id);
        if ($this->Wdays->delete($wday)) {
            $this->Flash->success(__('The wday has been deleted.'));
        } else {
            $this->Flash->error(__('The wday could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
