<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ProductsLocals Controller
 *
 * @property \App\Model\Table\ProductsLocalsTable $ProductsLocals
 */
class ProductsLocalsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Products', 'Locals']
        ];
        $productsLocals = $this->paginate($this->ProductsLocals);

        $this->set(compact('productsLocals'));
        $this->set('_serialize', ['productsLocals']);
    }

    /**
     * View method
     *
     * @param string|null $id Products Local id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $productsLocal = $this->ProductsLocals->get($id, [
            'contain' => ['Products', 'Locals']
        ]);

        $this->set('productsLocal', $productsLocal);
        $this->set('_serialize', ['productsLocal']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $productsLocal = $this->ProductsLocals->newEntity();
        if ($this->request->is('post')) {
            $productsLocal = $this->ProductsLocals->patchEntity($productsLocal, $this->request->data);
            if ($this->ProductsLocals->save($productsLocal)) {
                $this->Flash->success(__('The products local has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The products local could not be saved. Please, try again.'));
            }
        }
        $products = $this->ProductsLocals->Products->find('list', ['limit' => 200]);
        $locals = $this->ProductsLocals->Locals->find('list', ['limit' => 200]);
        $this->set(compact('productsLocal', 'products', 'locals'));
        $this->set('_serialize', ['productsLocal']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Products Local id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $productsLocal = $this->ProductsLocals->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $productsLocal = $this->ProductsLocals->patchEntity($productsLocal, $this->request->data);
            if ($this->ProductsLocals->save($productsLocal)) {
                $this->Flash->success(__('The products local has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The products local could not be saved. Please, try again.'));
            }
        }
        $products = $this->ProductsLocals->Products->find('list', ['limit' => 200]);
        $locals = $this->ProductsLocals->Locals->find('list', ['limit' => 200]);
        $this->set(compact('productsLocal', 'products', 'locals'));
        $this->set('_serialize', ['productsLocal']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Products Local id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $productsLocal = $this->ProductsLocals->get($id);
        if ($this->ProductsLocals->delete($productsLocal)) {
            $this->Flash->success(__('The products local has been deleted.'));
        } else {
            $this->Flash->error(__('The products local could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
