<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Params Controller
 *
 * @property \App\Model\Table\ParamsTable $Params
 */
class ParamsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Products']
        ];
        $params = $this->paginate($this->Params);

        $this->set(compact('params'));
        $this->set('_serialize', ['params']);
    }

    /**
     * View method
     *
     * @param string|null $id Param id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $param = $this->Params->get($id, [
            'contain' => ['Products']
        ]);

        $this->set('param', $param);
        $this->set('_serialize', ['param']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $param = $this->Params->newEntity();
        if ($this->request->is('post')) {
            $param = $this->Params->patchEntity($param, $this->request->data);
            if ($this->Params->save($param)) {
                $this->Flash->success(__('The param has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The param could not be saved. Please, try again.'));
            }
        }
        $products = $this->Params->Products->find('list', ['limit' => 200]);
        $this->set(compact('param', 'products'));
        $this->set('_serialize', ['param']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Param id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $param = $this->Params->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $param = $this->Params->patchEntity($param, $this->request->data);
            if ($this->Params->save($param)) {
                $this->Flash->success(__('The param has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The param could not be saved. Please, try again.'));
            }
        }
        $products = $this->Params->Products->find('list', ['limit' => 200]);
        $this->set(compact('param', 'products'));
        $this->set('_serialize', ['param']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Param id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $param = $this->Params->get($id);
        if ($this->Params->delete($param)) {
            $this->Flash->success(__('The param has been deleted.'));
        } else {
            $this->Flash->error(__('The param could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
