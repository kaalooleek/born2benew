<?php
namespace App\Model\Table;

use App\Model\Entity\Review;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Reviews Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Products
 */
class ReviewsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('reviews');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('review_title', 'create')
            ->notEmpty('review_title');

        $validator
            ->requirePresence('review', 'create')
            ->notEmpty('review');

        $validator
            ->integer('rating')
            ->requirePresence('rating', 'create')
            ->notEmpty('rating');

        $validator
            ->integer('comfort')
            ->requirePresence('comfort', 'create')
            ->notEmpty('comfort');

        $validator
            ->integer('style')
            ->requirePresence('style', 'create')
            ->notEmpty('style');

        $validator
            ->integer('sizing')
            ->requirePresence('sizing', 'create')
            ->notEmpty('sizing');

        $validator
            ->boolean('is_recommended')
            ->requirePresence('is_recommended', 'create')
            ->notEmpty('is_recommended');

        $validator
            ->integer('thumbs_up')
            ->requirePresence('thumbs_up', 'create')
            ->notEmpty('thumbs_up');

        $validator
            ->integer('thumbs_down')
            ->requirePresence('thumbs_down', 'create')
            ->notEmpty('thumbs_down');

        $validator
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('city', 'create')
            ->notEmpty('city');

        $validator
            ->boolean('review_anonym')
            ->requirePresence('review_anonym', 'create')
            ->notEmpty('review_anonym');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['product_id'], 'Products'));
        return $rules;
    }
}
