<?php
namespace App\Model\Table;

use App\Model\Entity\Product;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Products Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Categories
 * @property \Cake\ORM\Association\HasMany $Images
 * @property \Cake\ORM\Association\HasMany $OrdersItems
 * @property \Cake\ORM\Association\HasMany $Reviews
 * @property \Cake\ORM\Association\HasMany $Sizes
 */
class ProductsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('products');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Images', [
            'foreignKey' => 'product_id'
        ]);
        $this->hasMany('OrdersItems', [
            'foreignKey' => 'product_id'
        ]);
        $this->hasMany('Reviews', [
            'foreignKey' => 'product_id'
        ]);
        $this->hasMany('Sizes', [
            'foreignKey' => 'product_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->numeric('sell_price')
            ->requirePresence('sell_price', 'create')
            ->notEmpty('sell_price');

        $validator
            ->requirePresence('code', 'create')
            ->notEmpty('code');

        $validator
            ->requirePresence('keywords', 'create')
            ->notEmpty('keywords');

        $validator
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        $validator
            ->requirePresence('color', 'create')
            ->notEmpty('color');

        $validator
            ->integer('score')
            ->allowEmpty('score');

        $validator
            ->integer('popularity')
            ->allowEmpty('popularity');

        $validator
            ->integer('views')
            ->allowEmpty('views');

        $validator
            ->requirePresence('material', 'create')
            ->notEmpty('material');

        $validator
            ->requirePresence('heel_type', 'create')
            ->notEmpty('heel_type');

        $validator
            ->numeric('heel_height')
            ->requirePresence('heel_height', 'create')
            ->notEmpty('heel_height');

        $validator
            ->requirePresence('pattern', 'create')
            ->notEmpty('pattern');

        $validator
            ->requirePresence('shoe_fastener', 'create')
            ->notEmpty('shoe_fastener');

        $validator
            ->requirePresence('shoe_toecap', 'create')
            ->notEmpty('shoe_toecap');

        $validator
            ->requirePresence('sole', 'create')
            ->notEmpty('sole');

        $validator
            ->requirePresence('extras', 'create')
            ->notEmpty('extras');

        $validator
            ->boolean('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['category_id'], 'Categories'));
        return $rules;
    }
}
