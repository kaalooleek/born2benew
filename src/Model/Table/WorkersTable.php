<?php
namespace App\Model\Table;

use App\Model\Entity\Worker;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Workers Model
 *
 * @property \Cake\ORM\Association\HasMany $Workersdays
 * @property \Cake\ORM\Association\BelongsToMany $Accesses
 * @property \Cake\ORM\Association\BelongsToMany $Groups
 * @property \Cake\ORM\Association\BelongsToMany $Notifications
 * @property \Cake\ORM\Association\BelongsToMany $Tasks
 */
class WorkersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('workers');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Workersdays', [
            'foreignKey' => 'worker_id'
        ]);
        $this->belongsToMany('Accesses', [
            'foreignKey' => 'worker_id',
            'targetForeignKey' => 'access_id',
            'joinTable' => 'workers_accesses'
        ]);
        $this->belongsToMany('Groups', [
            'foreignKey' => 'worker_id',
            'targetForeignKey' => 'group_id',
            'joinTable' => 'workers_groups'
        ]);
        $this->belongsToMany('Notifications', [
            'foreignKey' => 'worker_id',
            'targetForeignKey' => 'notification_id',
            'joinTable' => 'workers_notifications'
        ]);
        $this->belongsToMany('Tasks', [
            'foreignKey' => 'worker_id',
            'targetForeignKey' => 'task_id',
            'joinTable' => 'workers_tasks'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('ean');

        $validator
            ->requirePresence('first_name', 'create')
            ->notEmpty('first_name');

        $validator
            ->requirePresence('last_name', 'create')
            ->notEmpty('last_name');

        $validator
            ->requirePresence('slug', 'create')
            ->notEmpty('slug');

        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->boolean('password_temp')
            ->requirePresence('password_temp', 'create')
            ->notEmpty('password_temp');

        $validator
            ->requirePresence('avatar', 'create')
            ->notEmpty('avatar');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->boolean('locked')
            ->requirePresence('locked', 'create')
            ->notEmpty('locked');

        $validator
            ->requirePresence('color', 'create')
            ->notEmpty('color');

        $validator
            ->allowEmpty('sidebar');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        return $rules;
    }
}
