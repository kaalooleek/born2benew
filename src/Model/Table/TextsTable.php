<?php
namespace App\Model\Table;

use App\Model\Entity\Text;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Texts Model
 *
 */
class TextsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('texts');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('message', 'create')
            ->notEmpty('message');

        $validator
            ->integer('priority')
            ->requirePresence('priority', 'create')
            ->notEmpty('priority');

        $validator
            ->requirePresence('number', 'create')
            ->notEmpty('number');

        $validator
            ->boolean('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->boolean('in_progress')
            ->requirePresence('in_progress', 'create')
            ->notEmpty('in_progress');

        $validator
            ->boolean('delivered')
            ->requirePresence('delivered', 'create')
            ->notEmpty('delivered');

        $validator
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->dateTime('taken')
            ->requirePresence('taken', 'create')
            ->notEmpty('taken');

        $validator
            ->dateTime('sent')
            ->requirePresence('sent', 'create')
            ->notEmpty('sent');

        $validator
            ->dateTime('delivered_dt')
            ->requirePresence('delivered_dt', 'create')
            ->notEmpty('delivered_dt');

        $validator
            ->requirePresence('error_status', 'create')
            ->notEmpty('error_status');

        return $validator;
    }
}
