<?php
namespace App\Model\Table;

use App\Model\Entity\Website;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Websites Model
 *
 * @property \Cake\ORM\Association\HasMany $Coupons
 * @property \Cake\ORM\Association\HasMany $Imgs
 * @property \Cake\ORM\Association\HasMany $Orders
 * @property \Cake\ORM\Association\HasMany $Reviews
 * @property \Cake\ORM\Association\HasMany $Subscribers
 * @property \Cake\ORM\Association\HasMany $Users
 */
class WebsitesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('websites');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->hasMany('Coupons', [
            'foreignKey' => 'website_id'
        ]);
        $this->hasMany('Imgs', [
            'foreignKey' => 'website_id'
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'website_id'
        ]);
        $this->hasMany('Reviews', [
            'foreignKey' => 'website_id'
        ]);
        $this->hasMany('Subscribers', [
            'foreignKey' => 'website_id'
        ]);
        $this->hasMany('Users', [
            'foreignKey' => 'website_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('url', 'create')
            ->notEmpty('url');

        $validator
            ->requirePresence('country', 'create')
            ->notEmpty('country');

        $validator
            ->requirePresence('currency', 'create')
            ->notEmpty('currency');

        $validator
            ->requirePresence('language', 'create')
            ->notEmpty('language');

        return $validator;
    }
}
