<?php
namespace App\Model\Table;

use App\Model\Entity\Variant;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Variants Model
 *
 * @property \Cake\ORM\Association\HasMany $ProductsIndexes
 */
class VariantsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('variants');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('ProductsIndexes', [
            'foreignKey' => 'variant_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('color', 'create')
            ->notEmpty('color');

        $validator
            ->requirePresence('material', 'create')
            ->notEmpty('material');

        $validator
            ->requirePresence('platform_height', 'create')
            ->notEmpty('platform_height');

        $validator
            ->requirePresence('heel_height', 'create')
            ->notEmpty('heel_height');

        $validator
            ->requirePresence('pattern', 'create')
            ->notEmpty('pattern');

        $validator
            ->requirePresence('shoe_fastener', 'create')
            ->notEmpty('shoe_fastener');

        $validator
            ->requirePresence('detail', 'create')
            ->notEmpty('detail');

        $validator
            ->integer('shoe_toecap')
            ->requirePresence('shoe_toecap', 'create')
            ->notEmpty('shoe_toecap');

        $validator
            ->requirePresence('shoe_height', 'create')
            ->notEmpty('shoe_height');

        return $validator;
    }
}
