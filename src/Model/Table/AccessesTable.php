<?php
namespace App\Model\Table;

use App\Model\Entity\Access;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Accesses Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Workers
 */
class AccessesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('accesses');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsToMany('Workers', [
            'foreignKey' => 'access_id',
            'targetForeignKey' => 'worker_id',
            'joinTable' => 'workers_accesses'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('controller', 'create')
            ->notEmpty('controller');

        $validator
            ->requirePresence('action', 'create')
            ->notEmpty('action');

        return $validator;
    }
}
