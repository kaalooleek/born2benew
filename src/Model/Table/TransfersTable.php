<?php
namespace App\Model\Table;

use App\Model\Entity\Transfer;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Transfers Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Invoices
 * @property \Cake\ORM\Association\BelongsTo $Orders
 * @property \Cake\ORM\Association\BelongsToMany $Invoices
 * @property \Cake\ORM\Association\BelongsToMany $Orders
 */
class TransfersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('transfers');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Invoices', [
            'foreignKey' => 'invoice_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Invoices', [
            'foreignKey' => 'transfer_id',
            'targetForeignKey' => 'invoice_id',
            'joinTable' => 'transfers_invoices'
        ]);
        $this->belongsToMany('Orders', [
            'foreignKey' => 'transfer_id',
            'targetForeignKey' => 'order_id',
            'joinTable' => 'transfers_orders'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->date('date')
            ->requirePresence('date', 'create')
            ->notEmpty('date');

        $validator
            ->date('accounting_date')
            ->requirePresence('accounting_date', 'create')
            ->notEmpty('accounting_date');

        $validator
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->requirePresence('sender', 'create')
            ->notEmpty('sender');

        $validator
            ->requirePresence('receiver', 'create')
            ->notEmpty('receiver');

        $validator
            ->requirePresence('account_number', 'create')
            ->notEmpty('account_number');

        $validator
            ->numeric('amount')
            ->requirePresence('amount', 'create')
            ->notEmpty('amount');

        $validator
            ->requirePresence('balance', 'create')
            ->notEmpty('balance');

        $validator
            ->requirePresence('raw_amount', 'create')
            ->notEmpty('raw_amount');

        $validator
            ->requirePresence('raw_balance', 'create')
            ->notEmpty('raw_balance');

        $validator
            ->numeric('transfer_balance')
            ->requirePresence('transfer_balance', 'create')
            ->notEmpty('transfer_balance');

        $validator
            ->boolean('recognized')
            ->requirePresence('recognized', 'create')
            ->notEmpty('recognized');

        $validator
            ->boolean('incoming')
            ->requirePresence('incoming', 'create')
            ->notEmpty('incoming');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['invoice_id'], 'Invoices'));
        $rules->add($rules->existsIn(['order_id'], 'Orders'));
        return $rules;
    }
}
