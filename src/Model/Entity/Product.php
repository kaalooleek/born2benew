<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Product Entity.
 *
 * @property int $id
 * @property int $category_id
 * @property \App\Model\Entity\Category $category
 * @property string $name
 * @property float $sell_price
 * @property string $code
 * @property string $keywords
 * @property string $description
 * @property string $color
 * @property int $score
 * @property int $popularity
 * @property int $views
 * @property \Cake\I18n\Time $created
 * @property string $material
 * @property string $heel_type
 * @property float $heel_height
 * @property string $pattern
 * @property string $shoe_fastener
 * @property string $shoe_toecap
 * @property string $sole
 * @property string $extras
 * @property bool $status
 * @property \App\Model\Entity\Image[] $images
 * @property \App\Model\Entity\OrdersItem[] $orders_items
 * @property \App\Model\Entity\Review[] $reviews
 * @property \App\Model\Entity\Size[] $sizes
 */
class Product extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
