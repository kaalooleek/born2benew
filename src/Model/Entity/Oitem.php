<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Oitem Entity.
 *
 * @property int $id
 * @property int $size_id
 * @property \App\Model\Entity\Size $size
 * @property int $order_id
 * @property \App\Model\Entity\Order $order
 * @property int $quantity
 * @property float $price
 * @property string $currency
 * @property string $status
 * @property bool $returned
 */
class Oitem extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
