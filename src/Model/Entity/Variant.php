<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Variant Entity.
 *
 * @property int $id
 * @property string $color
 * @property string $material
 * @property string $platform_height
 * @property string $heel_height
 * @property string $pattern
 * @property string $shoe_fastener
 * @property string $detail
 * @property int $shoe_toecap
 * @property string $shoe_height
 * @property \App\Model\Entity\Product $product
 */
class Variant extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
