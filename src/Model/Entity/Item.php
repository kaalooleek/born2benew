<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Item Entity.
 *
 * @property int $id
 * @property string $name
 * @property float $price
 * @property string $currency
 * @property int $quantity
 * @property string $discount
 * @property float $netto_price
 * @property float $vat_rate
 * @property float $vat_value
 * @property float $netto_value
 * @property float $brutto_value
 * @property int $invoice_id
 * @property \App\Model\Entity\Invoice $invoice
 * @property string $jm
 * @property int $size_id
 * @property \App\Model\Entity\Size $size
 * @property int $item_id
 * @property \App\Model\Entity\Item[] $items
 */
class Item extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
