<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Website Entity.
 *
 * @property int $id
 * @property string $name
 * @property string $url
 * @property string $country
 * @property string $currency
 * @property string $language
 * @property \App\Model\Entity\Coupon[] $coupons
 * @property \App\Model\Entity\Img[] $imgs
 * @property \App\Model\Entity\Order[] $orders
 * @property \App\Model\Entity\Review[] $reviews
 * @property \App\Model\Entity\Subscriber[] $subscribers
 * @property \App\Model\Entity\User[] $users
 */
class Website extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
