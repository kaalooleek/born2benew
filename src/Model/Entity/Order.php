<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Order Entity.
 *
 * @property int $id
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property string $email
 * @property string $street
 * @property string $street_no
 * @property string $street_no_2
 * @property string $zip
 * @property string $city
 * @property string $country
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $currency
 * @property string $status
 * @property float $to_paid
 * @property float $paid_amount
 * @property int $carrier_id
 * @property \App\Model\Entity\Carrier $carrier
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property string $paczkomat
 * @property string $notes
 * @property string $track_and_trace
 * @property bool $checked
 * @property bool $hold
 * @property \App\Model\Entity\Code[] $codes
 * @property \App\Model\Entity\Invoice[] $invoices
 * @property \App\Model\Entity\OrdersItem[] $orders_items
 */
class Order extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
