<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Text Entity.
 *
 * @property int $id
 * @property string $message
 * @property int $priority
 * @property string $number
 * @property bool $status
 * @property bool $in_progress
 * @property bool $delivered
 * @property string $type
 * @property \Cake\I18n\Time $taken
 * @property \Cake\I18n\Time $sent
 * @property \Cake\I18n\Time $delivered_dt
 * @property \Cake\I18n\Time $created
 * @property string $error_status
 */
class Text extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
