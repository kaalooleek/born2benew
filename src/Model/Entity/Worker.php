<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Worker Entity.
 *
 * @property int $id
 * @property string $ean
 * @property string $first_name
 * @property string $last_name
 * @property string $slug
 * @property string $password
 * @property bool $password_temp
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property string $avatar
 * @property string $email
 * @property bool $locked
 * @property string $color
 * @property string $sidebar
 * @property \App\Model\Entity\Access[] $accesses
 * @property \App\Model\Entity\Group[] $groups
 * @property \App\Model\Entity\Notification[] $notifications
 * @property \App\Model\Entity\Task[] $tasks
 */
class Worker extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    /**
     * Fields that are excluded from JSON an array versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
}
