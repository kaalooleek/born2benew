<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PhysicalProduct Entity.
 *
 * @property int $id
 * @property int $products_id
 * @property \App\Model\Entity\Product $product
 * @property int $variants_id
 * @property \App\Model\Entity\Variant $variant
 * @property string $barcode
 * @property int $warehouse_id
 * @property \App\Model\Entity\Warehouse $warehouse
 * @property string $shelf
 * @property int $type_id
 * @property \App\Model\Entity\Type $type
 */
class PhysicalProduct extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
