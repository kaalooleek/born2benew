<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Invoice Entity.
 *
 * @property int $id
 * @property string $type
 * @property string $number
 * @property string $address
 * @property string $zip
 * @property string $city
 * @property string $name
 * @property string $nip
 * @property string $pesel
 * @property string $email
 * @property string $phone
 * @property string $fax
 * @property \Cake\I18n\Time $sell_date
 * @property \Cake\I18n\Time $invoice_date
 * @property string $discount_type
 * @property string $payment_type
 * @property \Cake\I18n\Time $payment_term
 * @property bool $correction
 * @property string $account
 * @property string $bank_name
 * @property string $bic_swift
 * @property string $comments
 * @property string $reason
 * @property string $status
 * @property float $paid
 * @property int $order_id
 * @property \App\Model\Entity\Order $order
 */
class Invoice extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
