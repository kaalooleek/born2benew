<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Img Entity.
 *
 * @property int $id
 * @property string $url
 * @property int $product_id
 * @property \App\Model\Entity\Product $product
 * @property int $website_id
 * @property \App\Model\Entity\Website $website
 */
class Img extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
