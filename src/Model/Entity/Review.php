<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Review Entity.
 *
 * @property int $id
 * @property int $product_id
 * @property \App\Model\Entity\Product $product
 * @property string $review_title
 * @property string $review
 * @property int $rating
 * @property int $comfort
 * @property int $style
 * @property int $sizing
 * @property bool $is_recommended
 * @property int $thumbs_up
 * @property int $thumbs_down
 * @property string $status
 * @property string $name
 * @property string $city
 * @property bool $review_anonym
 * @property \Cake\I18n\Time $created
 */
class Review extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
