<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * User Entity.
 *
 * @property int $id
 * @property string $password
 * @property string $email
 * @property bool $locked
 * @property string $street
 * @property string $street_no
 * @property string $street_no_2
 * @property string $zip_code
 * @property string $city
 * @property string $country
 * @property string $first_name
 * @property string $last_name
 * @property int $phone
 * @property \Cake\I18n\Time $birthday
 * @property string $comp_name
 * @property int $comp_nip
 * @property string $comp_street
 * @property string $comp_street_no
 * @property string $comp_street_no_2
 * @property string $comp_zip
 * @property string $comp_country
 * @property string $comp_city
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property string $token
 * @property \App\Model\Entity\Code[] $codes
 * @property \App\Model\Entity\Order[] $orders
 * @property \App\Model\Entity\Session[] $sessions
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    /**
     * Fields that are excluded from JSON an array versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
        'token'
    ];
}
