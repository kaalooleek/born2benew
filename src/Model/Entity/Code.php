<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Code Entity.
 *
 * @property int $id
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property int $order_id
 * @property \App\Model\Entity\Order $order
 * @property int $coupon_id
 * @property \App\Model\Entity\Coupon $coupon
 * @property bool $is_used
 * @property string $code
 */
class Code extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
