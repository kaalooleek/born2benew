<div class="panel panel-primary">
    <div class="panel-heading"><?= __('Texts') ?></div>
    <div class="panel-body">
        <div class="table-responsive">
            <table cellpadding="0" cellspacing="0" class="table table-hover">
                <h3><?= h($text->id) ?></h3>
                <tr>
                    <th><?= __('Msg') ?></th>
                    <td><?= h($text->msg) ?></td>
                </tr>
                <tr>
                    <th><?= __('Priority') ?></th>
                    <td><?= h($text->priority) ?></td>
                </tr>
                <tr>
                    <th><?= __('Number') ?></th>
                    <td><?= h($text->number) ?></td>
                </tr>
                <tr>
                    <th><?= __('Sent To') ?></th>
                    <td><?= h($text->sent_to) ?></td>
                </tr>
                <tr>
                    <th><?= __('Status') ?></th>
                    <td><?= h($text->status) ?></td>
                </tr>
                <tr>
                    <th><?= __('Sent') ?></th>
                    <td><?= h($text->sent) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($text->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($text->modified) ?></td>
                </tr>
                <tr>
                    <th><?= __('Incoming') ?></th>
                    <td><?= $text->incoming ? __('Yes') : __('No'); ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>