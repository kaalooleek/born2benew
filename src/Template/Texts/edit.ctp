<div class="panel panel-primary">
    <div class="panel-heading"><?= __('Edytuj text') ?></div>
    <div class="panel-body">
        <div class="table-responsive">
    <?= $this->Form->create($text) ?>
    <fieldset>
        <?php
            echo $this->Form->input('msg', ['class' => 'form-control margin-bottom']);
            echo $this->Form->input('priority', ['class' => 'form-control margin-bottom']);
            echo $this->Form->input('number', ['class' => 'form-control margin-bottom']);
            echo $this->Form->input('sent_to', ['class' => 'form-control margin-bottom']);
            echo $this->Form->input('status', ['class' => 'form-control margin-bottom']);
            echo $this->Form->input('incoming');
            echo $this->Form->input('sent');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success spacer']) ?>
    <?= $this->Form->end() ?>
</div>
