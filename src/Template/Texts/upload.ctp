<div class="orders form">
    <?php echo $this->Form->create('Text', array('type' => 'file', 'url' => array('controller' => 'texts', 'action' => 'upload', 'file'))); ?>
    <fieldset>

        <?php
        echo $this->Form->input('file', array('type' => 'file'));
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Wczytaj plik')); ?>
</div>
<div>
    <?php if (!empty($incorrect)): ?>
        <table >
            <tr>
                <td colspan="3" style=" font-weight: bold;color:red;">NIEPOPRAWNE</td>
            </tr>
            <?php foreach ($incorrect as $rec): ?>
                <tr>
                    <td style="width:30%; color:red;"><?php echo $rec['phone']; ?></td>
                    <td style="width:30%; color:red;"><?php echo $rec['msg']; ?></td>
                    <td style="width:30%; color:red;"><?php echo $rec['priority']; ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
</div>
<div>
    <?php if (!empty($correct)): ?>
        <table >
            <tr>
                <td colspan="3" style=" font-weight: bold;color:green;">POPRAWNE</td>
            </tr>
            <?php foreach ($correct as $rec): ?>
                <tr>
                    <td style="width:30%; color:green;"><?php echo $rec['phone']; ?></td>
                    <td style="width:30%; color:green;"><?php echo $rec['msg']; ?></td>
                    <td style="width:30%; color:green;"><?php echo $rec['priority']; ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
        <?php echo $this->Form->create('Text', array('type' => 'file', 'url' => array('controller' => 'texts', 'action' => 'upload', 'file', $priority))); ?>
        <?php
        $x = 0;
        ?>
        <?php echo $this->Form->input('Other.info', array('value' => 'add_msgs', 'type' => 'hidden')); ?>
        <?php
        echo $this->Form->input('Other.filename', array('type' => 'hidden'));
        echo $this->Form->input('file', array('type' => 'file'));
        ?>
        <?php
        echo $this->Form->end('Dodaj smsy do wysĹ‚ania');
        $x++;
        ?>

    <?php endif; ?>
</div>
