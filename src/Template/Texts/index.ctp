<div class="panel panel-primary" ng-controller="textsCtrl">
    <div class="panel-heading"><?= __('Wiadomości') ?></div>
    <div class="panel-body">
        <div class="table-responsive">
            <table cellpadding="0" cellspacing="0" class="table table-hover">
                <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id', 'ID') ?></th>
                    <th><?= $this->Paginator->sort('msg', 'Treść') ?></th>
                    <th><?= $this->Paginator->sort('priority', 'Priotytet') ?></th>
                    <th><?= $this->Paginator->sort('number', 'Numer') ?></th>
                    <th><?= $this->Paginator->sort('sent_to', 'Adresat') ?></th>
                    <th><?= $this->Paginator->sort('status', 'Status') ?></th>
                    <th><?= $this->Paginator->sort('incoming', 'Incoming') ?></th>
                    <th><?= $this->Paginator->sort('sent', 'Wysłano') ?></th>
                    <th><?= $this->Paginator->sort('created', 'Utworzono') ?></th>
                    <th><?= $this->Paginator->sort('modified', 'Zmodyfikowano') ?></th>
                    <th class="actions"><?= __('Akcje') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($texts as $text): ?>
                <tr>
                    <td><?= $this->Number->format($text->id) ?></td>
                    <td><p class="long-text"><?= h($text->msg) ?></p></td>
                    <td><?= h($text->priority) ?></td>
                    <td><?= h($text->number) ?></td>
                    <td><?= h($text->sent_to) ?></td>
                    <td><?= h($text->status) ?></td>
                    <td><?= h($text->incoming) ?></td>
                    <td><?= h($text->sent) ?></td>
                    <td><?= h($text->created) ?></td>
                    <td><?= h($text->modified) ?></td>
                    <td class="actions">
                        <div style="width: 120px;">
                            <div class="btn-group">
                                <button type="button"
                                        class="btn btn-primary"
                                        ng-click="show_text(<?= $text->id ?>)">Szczegóły
                                </button>
                                <button type="button" class="btn btn-primary dropdown-toggle"
                                        data-toggle="dropdown">
                                    <span class="caret"></span>
                                </button>
                            </div>
                        </div>
                        <div id="text_modal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 class="modal-title">Szczegóły wiadomości</h3>
                                    </div>
                                    <div class="modal-body table-responsive">
                                        <table cellpadding="0" cellspacing="0" class="table table-hover">
                                            <tr>
                                                <th><?= __('Treść') ?></th>
                                                <th style="font-weight: normal !important; white-space: normal;">
                                                    {{ text.msg }}
                                                </th>
                                            </tr>
                                            <tr>
                                                <th><?= __('Priorytet') ?></th>
                                                <td>{{ text.priority }}</td>
                                            </tr>
                                            <tr>
                                                <th><?= __('Numer') ?></th>
                                                <td>{{ text.number }}</td>
                                            </tr>
                                            <tr>
                                                <th><?= __('Adresat') ?></th>
                                                <td>{{ text.set_to }}</td>
                                            </tr>
                                            <tr>
                                                <th><?= __('Status') ?></th>
                                                <td>{{ text.status }}</td>
                                            </tr>
                                            <tr>
                                                <th><?= __('Incoming') ?></th>
                                                <td><?= $text->incoming ? __('Yes') : __('No'); ?></td>
                                            </tr>
                                            <tr>
                                                <th><?= __('Wysłano') ?></th>
                                                <td>{{ text.sent }}</td>
                                            </tr>
                                            <tr>
                                                <th><?= __('Utworzono') ?></th>
                                                <td>{{ text.created }}</td>
                                            </tr>
                                            <tr>
                                                <th><?= __('Modyfikacja') ?></th>
                                                <td>{{ text.modified }}</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default">Edytuj</button> <!--  TODO -->
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php endforeach; ?>
                </tbody>
    </table>
</div>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>
</div>
</div>