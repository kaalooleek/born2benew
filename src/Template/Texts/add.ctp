<div class="panel panel-primary">
    <div class="panel-heading"><?= __('Nowa faktura') ?></div>
    <div class="panel-body">
        <div class="texts form large-9 medium-8 columns content">
            <?= $this->Form->create($text) ?>
            <fieldset>
                <legend><?= __('Add Text') ?></legend>
                <?php
                echo $this->Form->input('msg', ['class' => 'form-control spacer']);
                echo $this->Form->input('priority', ['class' => 'form-control spacer']);
                echo $this->Form->input('number', ['class' => 'form-control spacer']);
                echo $this->Form->input('sent_to', ['class' => 'form-control spacer']);
                echo $this->Form->input('status', ['class' => 'form-control spacer']);
                echo $this->Form->input('incoming', ['class' => 'form-control spacer']);
                echo $this->Form->input('sent', ['class' => 'form-control spacer']);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Dodaj'), ['class' => 'btn btn-success spacer']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>