<style>
    .modal-table {
        height: 200px !important;
    }
</style>

<div class="row" ng-controller="usersCtrl">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Lista pracowników</div>
            <div class="panel-body">
                <div class="table-responsive main_row">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th><?= $this->Paginator->sort('id', 'ID') ?></th>
                            <th><?= $this->Paginator->sort('first_name', 'Imię') ?></th>
                            <th><?= $this->Paginator->sort('last_name', 'Nazwisko') ?></th>
                            <th><?= $this->Paginator->sort('email', 'E-mail') ?></th>
                            <th><?= $this->Paginator->sort('created', 'Utworzony') ?></th>
                            <th><?= $this->Paginator->sort('mofified', 'Zmodyfikowany') ?></th>
                            <th class="actions"><?= __('Akcje') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($workers as $worker): ?>
                        <tr>
                            <td><?= $this->Number->format($worker->id) ?></td>
                            <td><?= h($worker->first_name) ?></td>
                            <td><?= h($worker->last_name) ?></td>
                            <td><?= h($worker->email) ?></td>
                            <td><?= h($worker->created) ?></td>
                            <td><?= h($worker->modified) ?></td>
                            <td>
                                <div style="width: 100px;">
                                    <div class="btn-group">
                                        <button class="btn btn-primary" ng-click="show_user(<?= $worker->id ?>)">
                                            Zobacz
                                        </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div class="paginator">
                    <ul class="pagination pull-right">
                        <?= $this->Paginator->prev('< ' . __('')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('') . ' >') ?>
                    </ul>
                    <p><?= $this->Paginator->counter() ?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-info" id="user_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <img class="profile-user-img img-responsive img-circle" src="{{user.avatar}}"
                         alt="User profile picture">

                    <h3 class="profile-username text-center">{{user.first_name}} {{user.last_name}}</h3>

                    <p class="text-center profile-username">{{user.role}}</p>
                    <hr>
                </div>
                <div class="modal-body" style="background: #FFF !important;">
                    <div class="box-body">
                        <div class="box-group" id="accordion_access">
                            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                            <div class="panel box box-primary">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion_access"
                                           href=".collapsing_access" aria-expanded="false" class="collapsed">
                                            <span><i class="fa fa-lock"></i> &nbsp;Uprawnienia</span>
                                        </a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse collapsing_custom collapsing_access">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h4>Dostępne</h4>
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-bordered scrollable_table">
                                                        <tbody class="scrollable_body">
                                                        <tr ng-hide="{{hasAccess(x, user.accesses)}}"
                                                            class="scrollable_tr" ng-repeat="x in user.all_accesses">
                                                            <td data-toggle="tooltip" title="Kliknij, aby dodać dostęp"
                                                                data-to id="{{x.id}}"
                                                                ng-click="setAccess(user.id, x.id, 'add')">
                                                                {{x.controller}}/{{x.action}}
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <h4>Przydzielone</h4>
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-bordered scrollable_table">
                                                        <tbody class="scrollable_body">
                                                        <tr class="scrollable_tr" ng-repeat="x in user.accesses">
                                                            <td data-toggle="tooltip" title="Kliknij, aby usunąć dostęp"
                                                                id="{{x._joinData.id}}"
                                                                ng-click="setAccess(user.id, x._joinData.id, 'delete')">
                                                                {{x.controller}}/{{x.action}}
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box-group" id="accordion_groups">
                            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                            <div class="panel box box-primary">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion_groups"
                                           href=".collapsing_groups" aria-expanded="false" class="collapsed">
                                            <span><i class="fa fa-group"></i> &nbsp;Grupy</span>
                                        </a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse collapsing_custom collapsing_groups">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h4>Dostępne</h4>
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-bordered scrollable_table">
                                                        <tbody class="scrollable_body">
                                                        <tr ng-hide="{{hasGroup(x, user.groups)}}" class="scrollable_tr"
                                                            ng-repeat="x in user.all_groups">
                                                            <td data-toggle="tooltip"
                                                                title="Kliknij, aby dodać do grupy" data-to
                                                                id="{{x.id}}" ng-click="setGroup(user.id, x.id, 'add')">
                                                                {{x.name}}/{{x.desc}}
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <h4>Przydzielone</h4>
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-bordered scrollable_table">
                                                        <tbody class="scrollable_body">
                                                        <tr class="scrollable_tr" ng-repeat="x in user.groups">
                                                            <td data-toggle="tooltip"
                                                                title="Kliknij, aby usunąć z grupy"
                                                                id="{{x._joinData.id}}"
                                                                ng-click="setGroup(user.id, x._joinData.id, 'delete')">
                                                                {{x.name}}/{{x.desc}}
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box-group" id="accordion_workertime">
                            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                            <div class="panel box box-primary">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion_workertime"
                                           href=".collapsing_workertime" aria-expanded="false" class="collapsed">
                                            <span><i class="fa fa-clock-o"></i> &nbsp;Czas pracy</span>
                                        </a>
                                    </h4>
                                </div>
                                <div class="panel-collapse collapse collapsing_custom collapsing_workertime">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5>Podsumowanie</h5>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <p class="lead pull-left"><</p>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <center><p class="lead">Sierpień 2016</p></center>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <p class="lead pull-right">></p>
                                                    </div>
                                                </div>
                                                <table class="table">
                                                    <thead>
                                                        <th>Suma godzin</th>
                                                        <th>W tym nadgodzin</th>
                                                    </thead>
                                                    <tbody>
                                                        <td>276</td>
                                                        <td>22</td>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</div>