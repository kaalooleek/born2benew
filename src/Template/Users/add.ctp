<style>
    input:focus:invalid,
    textarea:focus:invalid {
        border: solid 2px #F5192F;
    }

    input:focus:valid,
    textarea:focus:valid {
        border: solid 2px #18E109;
        background-color: #fff;
    }
</style>

<div class="row" ng-controller="usersCtrl">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Dodawanie nowego pracownika</div>
            <div class="panel-body">
                <form name="new_user">
                    <div class="form-group">
                        <label for="first_name">Imię</label>
                        <input type="text" class="form-control" id="first_name" ng-model="user.first_name" required>
                    </div>
                    <div class="form-group">
                        <label for="last_name">Nazwisko</label>
                        <input type="text" class="form-control" id="last_name" ng-model="user.last_name" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" ng-model="user.email"
                               ng-pattern="/^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/"
                               required>
                    </div>
                    <div class="form-group">
                        <label for="groups">Grupy</label>
                        <div isteven-multi-select="" id="groups" input-model="groups" output-model="groups_obj" button-label="icon name" item-label="icon name maker" tick-property="ticked"></div>
                    </div>
                    <button class="btn btn-primary" ng-click="dodaj()" ng-disabled="new_user.$invalid || groups_obj.length == 0">Dodaj</button>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-primary" id="new_user_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Informacja o dodanym użytkowniku</h4>
                </div>
                <div class="modal-body">
                    <p>Email: {{user_new.email}}</p>
                    <p>Tymczasowo wygenerowane hasło to: {{user_new.password}}</p>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-outline" ng-click="send_mail()">Wyślij dane na maila</button>
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Zamknij</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>