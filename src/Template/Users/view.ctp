<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Websites'), ['controller' => 'Websites', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Website'), ['controller' => 'Websites', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Carts'), ['controller' => 'Carts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cart'), ['controller' => 'Carts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Codes'), ['controller' => 'Codes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Code'), ['controller' => 'Codes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tasks'), ['controller' => 'Tasks', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Task'), ['controller' => 'Tasks', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Wdays'), ['controller' => 'Wdays', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Wday'), ['controller' => 'Wdays', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Website') ?></th>
            <td><?= $user->has('website') ? $this->Html->link($user->website->name, ['controller' => 'Websites', 'action' => 'view', $user->website->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Password') ?></th>
            <td><?= h($user->password) ?></td>
        </tr>
        <tr>
            <th><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th><?= __('Access Level') ?></th>
            <td><?= h($user->access_level) ?></td>
        </tr>
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($user->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Street') ?></th>
            <td><?= h($user->street) ?></td>
        </tr>
        <tr>
            <th><?= __('Street No') ?></th>
            <td><?= h($user->street_no) ?></td>
        </tr>
        <tr>
            <th><?= __('Street No2') ?></th>
            <td><?= h($user->street_no2) ?></td>
        </tr>
        <tr>
            <th><?= __('Post') ?></th>
            <td><?= h($user->post) ?></td>
        </tr>
        <tr>
            <th><?= __('City') ?></th>
            <td><?= h($user->city) ?></td>
        </tr>
        <tr>
            <th><?= __('Country') ?></th>
            <td><?= h($user->country) ?></td>
        </tr>
        <tr>
            <th><?= __('First Name') ?></th>
            <td><?= h($user->first_name) ?></td>
        </tr>
        <tr>
            <th><?= __('Second Name') ?></th>
            <td><?= h($user->second_name) ?></td>
        </tr>
        <tr>
            <th><?= __('Phone') ?></th>
            <td><?= h($user->phone) ?></td>
        </tr>
        <tr>
            <th><?= __('Comp Name') ?></th>
            <td><?= h($user->comp_name) ?></td>
        </tr>
        <tr>
            <th><?= __('Comp Nip') ?></th>
            <td><?= h($user->comp_nip) ?></td>
        </tr>
        <tr>
            <th><?= __('Comp Street') ?></th>
            <td><?= h($user->comp_street) ?></td>
        </tr>
        <tr>
            <th><?= __('Comp Post') ?></th>
            <td><?= h($user->comp_post) ?></td>
        </tr>
        <tr>
            <th><?= __('Comp City') ?></th>
            <td><?= h($user->comp_city) ?></td>
        </tr>
        <tr>
            <th><?= __('Comp Country') ?></th>
            <td><?= h($user->comp_country) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($user->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($user->modified) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Active') ?></th>
            <td><?= $user->is_active ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Carts') ?></h4>
        <?php if (!empty($user->carts)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->carts as $carts): ?>
            <tr>
                <td><?= h($carts->id) ?></td>
                <td><?= h($carts->user_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Carts', 'action' => 'view', $carts->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Carts', 'action' => 'edit', $carts->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Carts', 'action' => 'delete', $carts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $carts->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Codes') ?></h4>
        <?php if (!empty($user->codes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Order Id') ?></th>
                <th><?= __('Coupon Id') ?></th>
                <th><?= __('Is Used') ?></th>
                <th><?= __('Code') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->codes as $codes): ?>
            <tr>
                <td><?= h($codes->id) ?></td>
                <td><?= h($codes->user_id) ?></td>
                <td><?= h($codes->order_id) ?></td>
                <td><?= h($codes->coupon_id) ?></td>
                <td><?= h($codes->is_used) ?></td>
                <td><?= h($codes->code) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Codes', 'action' => 'view', $codes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Codes', 'action' => 'edit', $codes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Codes', 'action' => 'delete', $codes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $codes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Orders') ?></h4>
        <?php if (!empty($user->orders)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Order Id') ?></th>
                <th><?= __('Website Id') ?></th>
                <th><?= __('Shelf') ?></th>
                <th><?= __('Oldshelf') ?></th>
                <th><?= __('Email') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Street') ?></th>
                <th><?= __('Street No') ?></th>
                <th><?= __('Street No2') ?></th>
                <th><?= __('Post') ?></th>
                <th><?= __('City') ?></th>
                <th><?= __('Country') ?></th>
                <th><?= __('First Name') ?></th>
                <th><?= __('Second Name') ?></th>
                <th><?= __('Phone') ?></th>
                <th><?= __('Comp Name') ?></th>
                <th><?= __('Comp Nip') ?></th>
                <th><?= __('Comp Street') ?></th>
                <th><?= __('Comp Post') ?></th>
                <th><?= __('Comp City') ?></th>
                <th><?= __('Comp Country') ?></th>
                <th><?= __('Status') ?></th>
                <th><?= __('Currency') ?></th>
                <th><?= __('Is Notified') ?></th>
                <th><?= __('Section') ?></th>
                <th><?= __('Payment Type') ?></th>
                <th><?= __('Delivery Type') ?></th>
                <th><?= __('Delivery Cost') ?></th>
                <th><?= __('Carrier') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th><?= __('Invoice Printed') ?></th>
                <th><?= __('Remarks') ?></th>
                <th><?= __('Courier Remarks') ?></th>
                <th><?= __('Paczkomat') ?></th>
                <th><?= __('Notes') ?></th>
                <th><?= __('Trackandtrace') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->orders as $orders): ?>
            <tr>
                <td><?= h($orders->id) ?></td>
                <td><?= h($orders->user_id) ?></td>
                <td><?= h($orders->order_id) ?></td>
                <td><?= h($orders->website_id) ?></td>
                <td><?= h($orders->shelf) ?></td>
                <td><?= h($orders->oldshelf) ?></td>
                <td><?= h($orders->email) ?></td>
                <td><?= h($orders->name) ?></td>
                <td><?= h($orders->street) ?></td>
                <td><?= h($orders->street_no) ?></td>
                <td><?= h($orders->street_no2) ?></td>
                <td><?= h($orders->post) ?></td>
                <td><?= h($orders->city) ?></td>
                <td><?= h($orders->country) ?></td>
                <td><?= h($orders->first_name) ?></td>
                <td><?= h($orders->second_name) ?></td>
                <td><?= h($orders->phone) ?></td>
                <td><?= h($orders->comp_name) ?></td>
                <td><?= h($orders->comp_nip) ?></td>
                <td><?= h($orders->comp_street) ?></td>
                <td><?= h($orders->comp_post) ?></td>
                <td><?= h($orders->comp_city) ?></td>
                <td><?= h($orders->comp_country) ?></td>
                <td><?= h($orders->status) ?></td>
                <td><?= h($orders->currency) ?></td>
                <td><?= h($orders->is_notified) ?></td>
                <td><?= h($orders->section) ?></td>
                <td><?= h($orders->payment_type) ?></td>
                <td><?= h($orders->delivery_type) ?></td>
                <td><?= h($orders->delivery_cost) ?></td>
                <td><?= h($orders->carrier) ?></td>
                <td><?= h($orders->created) ?></td>
                <td><?= h($orders->modified) ?></td>
                <td><?= h($orders->invoice_printed) ?></td>
                <td><?= h($orders->remarks) ?></td>
                <td><?= h($orders->courier_remarks) ?></td>
                <td><?= h($orders->paczkomat) ?></td>
                <td><?= h($orders->notes) ?></td>
                <td><?= h($orders->trackandtrace) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Orders', 'action' => 'view', $orders->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Orders', 'action' => 'edit', $orders->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Orders', 'action' => 'delete', $orders->id], ['confirm' => __('Are you sure you want to delete # {0}?', $orders->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tasks') ?></h4>
        <?php if (!empty($user->tasks)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Type') ?></th>
                <th><?= __('Source') ?></th>
                <th><?= __('Status') ?></th>
                <th><?= __('Priority') ?></th>
                <th><?= __('Title') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->tasks as $tasks): ?>
            <tr>
                <td><?= h($tasks->id) ?></td>
                <td><?= h($tasks->user_id) ?></td>
                <td><?= h($tasks->description) ?></td>
                <td><?= h($tasks->type) ?></td>
                <td><?= h($tasks->source) ?></td>
                <td><?= h($tasks->status) ?></td>
                <td><?= h($tasks->priority) ?></td>
                <td><?= h($tasks->title) ?></td>
                <td><?= h($tasks->created) ?></td>
                <td><?= h($tasks->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Tasks', 'action' => 'view', $tasks->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Tasks', 'action' => 'edit', $tasks->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Tasks', 'action' => 'delete', $tasks->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tasks->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Wdays') ?></h4>
        <?php if (!empty($user->wdays)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Date') ?></th>
                <th><?= __('Checkin') ?></th>
                <th><?= __('Checkout') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->wdays as $wdays): ?>
            <tr>
                <td><?= h($wdays->id) ?></td>
                <td><?= h($wdays->user_id) ?></td>
                <td><?= h($wdays->date) ?></td>
                <td><?= h($wdays->checkin) ?></td>
                <td><?= h($wdays->checkout) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Wdays', 'action' => 'view', $wdays->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Wdays', 'action' => 'edit', $wdays->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Wdays', 'action' => 'delete', $wdays->id], ['confirm' => __('Are you sure you want to delete # {0}?', $wdays->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
