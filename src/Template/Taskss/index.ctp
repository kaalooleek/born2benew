<style>
    input:focus:invalid,
    textarea:focus:invalid {
        border: solid 2px #F5192F;
    }

    input:focus:valid,
    textarea:focus:valid {
        border: solid 2px #18E109;
        background-color: #fff;
    }
</style>
<div class="panel panel-primary" ng-controller="notificationsCtrl">
    <div class="panel-heading ">Powiadomienia</div>
    <div class="panel-body">
        <div class="row">
            <form name="task_form">
                <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="Treść zadania" name="text" ng-model="task.text" required/>
                </div>
                <div class="col-md-2">
                    <select class="form-control" id="groups_select" name="groups_select" ng-model="tasks.group_id" ng-options="group as group.name for group in groups" required>
                        <option value="" ng-selected>Wybierz odbiorców</option>
                    </select>
                </div>
                <div class="col-md-1">
                    <button class="pull-right btn btn-primary" ng-disabled="task_form.$invalid"
                            ng-click="add_task()">Dodaj
                    </button>
                </div>
            </form>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="table-responsive box-body no-padding">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="1%"><?= $this->Paginator->sort('id') ?></th>
                                <th width="62%"><?= $this->Paginator->sort('text') ?></th>
                                <th><?= $this->Paginator->sort('date') ?></th>
                                <th><?= $this->Paginator->sort('group_id') ?></th>
                                <th>Nadawca</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="tasks_list">
                            <?php foreach ($tasks as $task): ?>
                            <tr id="<?= $task->id ?>">
                                <td><?= h($task->id) ?></td>
                                <td><?= h($task->text) ?></td>
                                <td><?= h($task->date) ?></td>
                                <td><?= h($task->group['name']) ?></td>
                                <td><?= h($task->workers[0]['first_name'])." ".h($task->workers[0]['last_name'])?></td>
                                <td><span><i class="fa fa-remove" ng-click="remove_task(<?= $task->id ?>)"></i></span></td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="paginator">
                            <ul class="pagination pull-right">
                                <?= $this->Paginator->prev('< ' . __('')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('') . ' >') ?>
                            </ul>
                        </div>
                    </div>
                    <?= $this->Paginator->counter() ?>
                </div>
            </div>
        </div>
    </div>