<div class="callout callout-error" role="alert" onclick="this.classList.add('hidden')">
    <?= h($message) ?>
</div>