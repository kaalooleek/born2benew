<html>
<head></head>
<body>
<div>
    <div style="background-color: black; text-align: center;">
        <img src="cid:logo" />
    </div>
    <div style="border: 1px solid black;padding: 10px 10px 10px 10px;">
        <hr>
        <center><p>Dane do logowania w panelu administracyjnym</p></center>
        <hr>
        <p>Twój email: <b><?= $email ?></b> </p>
        <p>Twoje tymczasowe hasło: <b><?= $pass ?></b></p>
        <p><b>(Po zalogowaniu zostaniesz poproszony/poproszona o ustawienie nowego hasła)</b></p>
        <hr>
        <p>Kod do wykorzystania w aplikacji Google Authenticator</p>
        <p>Kod należy zeskanować aplikacją Google Authenticator, aby otrzymać token do logowania w panelu na nowych urządzeniach</p>
        <hr>
        <p><img src="<?= $code ?>"></p>
        <hr/>
        Z poważaniem,<br>
        Zespół Born2Be.
    </div>
</div>
</body>
</html>