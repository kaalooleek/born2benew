<div ng-controller="productsCtrl">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <span class="lead">Produkty</span>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="search" placeholder="Wyszukaj..."/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <hr/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id', 'ID') ?></th>
                                        <th>Zdjęcie</th>
                                        <th><?= $this->Paginator->sort('code', 'Kod') ?></th>
                                        <th><?= $this->Paginator->sort('name', 'Nazwa') ?></th>
                                        <th><?= $this->Paginator->sort('total', 'Sztuk') ?></th>
                                        <th><?= $this->Paginator->sort('category_id', 'Kategoria') ?></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($products as $product){ ?>
                                    <tr>
                                        <td><?= $product->id ?></td>
                                        <?php if(count($product['images'])){ ?>
                                        <td><img class="img-responsive"
                                                 src="http://localhost/b2b_cake/webroot/<?= $product['images'][0]['link'] ?>"
                                                 height="30%" width="30%"></td>
                                        <?php } else {?>
                                        <td></td>
                                        <?php } ?>
                                        <td><?= $product->code ?></td>
                                        <td><?= $product->name ?></td>
                                        <td>
                                            <?php if(count($product['sizes'])) {?>
                                            <?php foreach($product['sizes'] as $size) { ?>
                                            R<?= $size['size'] ?> - <?= $size['total'] ?> szt.<br>
                                            <?php }} else { ?>
                                            Brak rozmiarów
                                            <?php } ?>
                                        </td>
                                        <td><?= $product->category['name'] ?></td>
                                        <td>
                                            <button class="btn btn-primary" data-toggle="modal" data-target="#viewmodal"
                                                    ng-click="show_product(<?= $product->id ?>)"><i
                                                    class="fa fa-eye"></i> Zobacz
                                            </button>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev('< ' . __('')) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next(__('') . ' >') ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="viewmodal" class="modal modal-primary fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header"
                     style="background-color: white !important; color: #333 !important; border-bottom: 1px solid #e5e5e5 !important;">
                    <h4 class="modal-title">Szczegóły produktu</h4>
                </div>
                <div class="modal-body" style="background-color: white !important; color: #333 !important;">
                    <div class="row">
                        <div class="col-md-12" style="border-bottom: 1px solid #e5e5e5 !important;">
                            <div class="col-md-6">
                                <table class="table">
                                    <tr>
                                        <td><b>ID:</b> {{ current_product.id }}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>ID Kategorii:</b>
                                            <div style="display:inline-block;min-width:50px;min-height:17px;" id="category_id" data-toggle="tooltip" title="Dwukrotne kliknięcie - edytuje pole">
                                                {{ current_product.category_id }}
                                            </div>
                                            <div class="form-group input-group" id="category_id_group" style="display: none;">
                                                <span class="input-group-addon" ng-click="save_input(current_product.id, 'category_id', category_id_input);"><i class="glyphicon glyphicon-save"></i></span>
                                                <input type="text" id="category_id_input" ng-model="category_id_input" class="form-control">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Kod:</b>
                                            <div style="display:inline-block;min-width:50px;min-height:17px;" id="code" data-toggle="tooltip" title="Dwukrotne kliknięcie - edytuje pole">
                                                {{ current_product.code }}
                                            </div>
                                            <div class="form-group input-group" id="code_group" style="display: none;">
                                                <span class="input-group-addon" ng-click="save_input(current_product.id, 'code', code_input);"><i class="glyphicon glyphicon-save"></i></span>
                                                <input type="text" id="code_input" ng-model="code_input" class="form-control">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Kolor:</b>
                                            <div style="display:inline-block;min-width:50px;min-height:17px;" id="color" data-toggle="tooltip" title="Dwukrotne kliknięcie - edytuje pole">
                                                {{ current_product.color }}
                                            </div>
                                            <div class="form-group input-group" id="color_group" style="display: none;">
                                                <span class="input-group-addon" ng-click="save_input(current_product.id, 'color', color_input);"><i class="glyphicon glyphicon-save"></i></span>
                                                <input type="text" id="color_input" ng-model="color_input" class="form-control">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Utworzono:</b>
                                            <div style="display:inline-block;min-width:50px;min-height:17px;" id="created" data-toggle="tooltip" title="Dwukrotne kliknięcie - edytuje pole">
                                                {{ current_product.created | date: 'yyyy-MM-dd HH:mm:ss' }}
                                            </div>
                                            <div class="form-group input-group" id="created_group" style="display: none;">
                                                <span class="input-group-addon" ng-click="save_input(current_product.id, 'created', created_input);"><i class="glyphicon glyphicon-save"></i></span>
                                                <input type="text" id="created_input" ng-model="created_input" class="form-control">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="long-text">
                                            <b>Opis:</b>
                                            <div style="display:inline-block;min-width:50px;min-height:17px;" id="description" data-toggle="tooltip" title="Dwukrotne kliknięcie - edytuje pole">
                                                {{ current_product.description }}
                                            </div>
                                            <div class="form-group input-group" id="description_group" style="display: none;">
                                                <span class="input-group-addon" ng-click="save_input(current_product.id, 'description', description_input);"><i class="glyphicon glyphicon-save"></i></span>
                                                <input type="text" id="description_input" ng-model="description_input" class="form-control">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Słowa kluczowe:</b>
                                            <div style="display:inline-block;min-width:50px;min-height:17px;" id="keywords" data-toggle="tooltip" title="Dwukrotne kliknięcie - edytuje pole">
                                                {{ current_product.keywords }}
                                            </div>
                                            <div class="form-group input-group" id="keywords_group" style="display: none;">
                                                <span class="input-group-addon" ng-click="save_input(current_product.id, 'keywords', keywords_input);"><i class="glyphicon glyphicon-save"></i></span>
                                                <input type="text" id="keywords_input" ng-model="keywords_input" class="form-control">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Nazwa:</b>
                                            <div style="display:inline-block;min-width:50px;min-height:17px;" id="name" data-toggle="tooltip" title="Dwukrotne kliknięcie - edytuje pole">
                                                {{ current_product.name }}
                                            </div>
                                            <div class="form-group input-group" id="name_group" style="display: none;">
                                                <span class="input-group-addon" ng-click="save_input(current_product.id, 'name', name_input);"><i class="glyphicon glyphicon-save"></i></span>
                                                <input type="text" id="name_input" ng-model="name_input" class="form-control">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Popularność:</b>
                                            <div style="display:inline-block;min-width:50px;min-height:17px;" id="popularity" data-toggle="tooltip" title="Dwukrotne kliknięcie - edytuje pole">
                                                {{ current_product.popularity }}
                                            </div>
                                            <div class="form-group input-group" id="popularity_group" style="display: none;">
                                                <span class="input-group-addon" ng-click="save_input(current_product.id, 'popularity', popularity_input);"><i class="glyphicon glyphicon-save"></i></span>
                                                <input type="text" id="popularity_input" ng-model="popularity_input" class="form-control">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Wynik(score):</b>
                                            <div style="display:inline-block;min-width:50px;min-height:17px;" id="score" data-toggle="tooltip" title="Dwukrotne kliknięcie - edytuje pole">
                                                {{ current_product.score }}
                                            </div>
                                            <div class="form-group input-group" id="score_group" style="display: none;">
                                                <span class="input-group-addon" ng-click="save_input(current_product.id, 'score', score_input);"><i class="glyphicon glyphicon-save"></i></span>
                                                <input type="text" id="score_input" ng-model="score_input" class="form-control">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Cena sprzedaży:</b>
                                            <div style="display:inline-block;min-width:50px;min-height:17px;" id="sell_price" data-toggle="tooltip" title="Dwukrotne kliknięcie - edytuje pole">
                                                {{ current_product.sell_price }} PLN
                                            </div>
                                            <div class="form-group input-group" id="sell_price_group" style="display: none;">
                                                <span class="input-group-addon" ng-click="save_input(current_product.id, 'sell_price', sell_price_input);"><i class="glyphicon glyphicon-save"></i></span>
                                                <input type="text" id="sell_price_input" ng-model="sell_price_input" class="form-control">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><b>Odsłony:</b>
                                            <div style="display:inline-block;min-width:50px;min-height:17px;" id="views" data-toggle="tooltip" title="Dwukrotne kliknięcie - edytuje pole">
                                                {{ current_product.views }}
                                            </div>
                                            <div class="form-group input-group" id="views_group" style="display: none;">
                                                <span class="input-group-addon" ng-click="save_input(current_product.id, 'views', views_input);"><i class="glyphicon glyphicon-save"></i></span>
                                                <input type="text" id="views_input" ng-model="views_input" class="form-control">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12" style="border: 1px solid #e5e5e5 !important; min-height: 400px;">
                                        <div class="col-sm-4" ng-repeat="file in current_product.images">
                                            <a href="javascript:void(0)" class="thumbnail" ng-click="delete_file($index, file.id, file.link)">
                                                <img title="{{file.name}}" src="{{file.link}}" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="padding-top: 1%;">
                                    <div class="col-md-12" style="border: 1px solid #e5e5e5 !important;"><input type="file" ng-model="data.image" onchange="angular.element(this).scope().previewFiles()" multiple name="fileToUpload" id="fileToUpload" style="padding-top: 1%; padding-bottom: 1%;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12 table-responsive" style="max-height: 200px;">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>ID Produktu</th>
                                        <th>Rozmiar</th>
                                        <th>Wkładka</th>
                                        <th>Razem</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="x in current_product.sizes">
                                        <td>{{ x.id }}</td>
                                        <td>{{ x.product_id }}</td>
                                        <td>{{ x.size }}</td>
                                        <td>{{ x.length }}</td>
                                        <td>{{ x.total }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="background-color: white !important; color: #333 !important; border-top: 1px solid #e5e5e5 !important;">
                    <button type="button" class="btn btn-primary" ng-click="saveImg()">Zapisz obrazy</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Zamknij</button>
                </div>
            </div>
        </div>
    </div>
    <script src="/born2benew/js/jquery-2.2.4.min.js"></script>
    <script>
        $(document).ready(function () {
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }

            var inputs = ['category_id', 'code', 'color', 'created', 'description', 'keywords', 'name', 'popularity', 'score', 'sell_price', 'views'];
            for (i = 0; i < inputs.length; i++) {
                (function (i) {
                    var name = inputs[i];
                    $("#" + name).dblclick(function () {
                        $("#" + name).fadeToggle('fast', "linear");
                        $("#" + name + "_group").fadeToggle('fast', "linear");
                        var value = $('#' + name).text();
                        $("#" + name + "_input").val(value);
                        $("body").on('click', function (e) {
                            if (!$('#' + name + '_input').is(e.target)) {
                                var new_value = $("#" + name + "_input").val();
                                $('#' + name + '_group').fadeToggle('fast', "linear");
                                $("#" + name).fadeToggle('fast', "linear");
                                $('#' + name + '_' + review.id + '_table').fadeToggle('fast', "linear");
                                $("body").off("click");
                            }
                        });
                    });
                })(i);
            }
            function show_success() {
                toastr.success('Zapisano pomyślnie!');
            }

            function show_error() {
                toastr.error('Wystąpił błąd w zapisywaniu!');
            }
        });
    </script>
</div>
