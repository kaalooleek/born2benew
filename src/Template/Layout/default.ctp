<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html ng-app="myApp">
<head>
    <base href="b2b">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>BORN2BE.pl | panel administracyjny</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <?= $this->Html->css('bootstrap.min.css'); ?>
    <!-- Font Awesome -->
    <?= $this->Html->css('font-awesome.min'); ?>
    <?= $this->Html->css('ionicons.min'); ?>
    <?= $this->Html->css('AdminLTE.min.css'); ?>
    <?= $this->Html->css('main'); ?>
    <?= $this->Html->css('skins/skin-blue.css'); ?>
    <?= $this->Html->css('skins/skin-black.css'); ?>
    <?= $this->Html->css('skins/skin-purple.css'); ?>
    <?= $this->Html->css('skins/skin-yellow.css'); ?>
    <?= $this->Html->css('skins/skin-red.css'); ?>
    <?= $this->Html->css('skins/skin-green.css'); ?>
    <?= $this->Html->css('skins/skin-pink.css'); ?>
    <?= $this->Html->css('bootstrap-datepicker.min'); ?>
    <?= $this->Html->css('jquery.autocomplete'); ?>
    <?= $this->Html->css('jquery-ui.min'); ?>
    <?= $this->Html->css('toastr.min'); ?>
    <?= $this->Html->css('bootstrap-switch.min'); ?>
    <?= $this->Html->css('jquery.custom-scrollbar'); ?>
    <?= $this->Html->css('bootstrap-select.min'); ?>
    <?= $this->Html->css('angular-multi-select.css'); ?>

    <link rel="stylesheet" href="http://s.mlcdn.co/animate.css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="custom_background hold-transition 'skin-'<?= $auth_user->color ?> <?= $auth_user->sidebar ?> fixed"
      ng-cloak=""
      ng-controller="dashboardCtrl">

<div class="m-app-loading" data-loading>

    <!--
        HACKY CODE WARNING: I'm putting Style block inside directive so that it
        will be removed from the DOM when we remove the directive container.
    -->
    <style type="text/css">
        div.m-app-loading {
            position: absolute;
        }

        div.m-app-loading div.animated-container {
            background-color: #2a2826;
            bottom: 0px;
            left: 0px;
            opacity: 1.0;
            position: fixed;
            right: 0px;
            top: 0px;
            z-index: 999999;
        }

        /* Used to initialize the ng-leave animation state. */
        div.m-app-loading div.animated-container.ng-leave {
            opacity: 1.0;
            transition: all linear 0ms;
            -webkit-transition: all linear 0ms;
        }

        /* Used to set the end properties of the ng-leave animation state. */
        div.m-app-loading div.animated-container.ng-leave-active {
            opacity: 0;
        }

        div.m-app-loading div.messaging {
            color: #FFFFFF;
            font-family: monospace;
            left: 0px;
            margin-top: -37px;
            position: absolute;
            right: 0px;
            text-align: center;
            top: 20%;
        }
    </style>


    <!-- BEGIN: Actual animated container. -->
    <div class="animated-container">

        <div class="messaging">
            <div class="row"><?php echo $this->Html->image('loading.gif', ['alt' => 'Loading', 'style' => 'width:250px;']); ?></div>
            <div class="row">
                <h1>
                    Ładowanie danych
                </h1>
            </div>
            <div class="row">
                <p>
                    Proszę czekać...
                </p>
            </div>
        </div>

    </div>
    <!-- END: Actual animated container. -->

</div>
<!-- END: App-Loading Screen. -->

<div class="row">
    <div class="Absolute-Center is-Responsive" id="splashscreen" style="display: none;">
        <div id="logo-container"><img src="<?php echo $auth_user->avatar; ?>" class="user-image"/></div>
        <div class="col-md-12 col-xs-12 col-lg-12 col-sm-12 text-center">
            <div><h1 class="lead welcome_message" id="message_header" style="display:inline-block"></h1>
                <h1 class="welcome_message" style="display:inline-block" id="blinking_line">|</h1></div>
        </div>
    </div>
</div>

<div class="wrapper" id="main_wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <?php echo $this->Html->link('<span class="logo-mini"><b>B</b>2B</span>' . $this->Html->image('logo_min.png',
        ['height' => 'auto', 'width' => '100px']), ['controller' => 'users', 'action' => 'dashboard'], ['class' =>
        'logo', 'escape' => false]); ?>

        <!-- Header Navbar -->
        <nav class="navbar" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="<?php echo $auth_user->avatar; ?>" class="user-image" alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs"><?php echo $auth_user->
                                first_name . " " . $auth_user->last_name; ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="<?php echo $auth_user->avatar; ?>" class="img-circle" alt="User Image">

                                <p>
                                    <?php echo $auth_user->first_name . " " . $auth_user->last_name; ?>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?= $this->Html->link(__('Wyloguj'), ['controller' => 'Users', 'action' =>
                                        'logout'], ['class' => 'btn btn-block btn-primary btn-sm']); ?>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <?= $this->Html->link(__('Wyloguj (dezautoryzuje ten komputer)'), ['controller'
                                        => 'Users', 'action' => 'permanent_logout'], ['class' => 'btn btn-block
                                        btn-primary btn-sm']); ?>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-list"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar" id="main_sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?php echo $auth_user->avatar; ?>" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?php echo $auth_user->first_name . " " . $auth_user->last_name; ?></p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>

            <!-- search form (Optional) -->
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Szukaj...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i
                                        class="fa fa-search"></i>
                                </button>
                            </span>
                </div>
            </form>
            <!-- /.search form -->
            <!-- lewacki pasek -->
            <ul class="sidebar-menu">
                <li class="header">Menu</li>
                <?php foreach ($menu['views'] as $main): ?>
                <li class="treeview">
                    <a href="#"><i class="fa fa-link"></i> <span><?= $main['label'] ?></span> <i
                            class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <?php foreach ($main['items'] as $item): ?>
                        <?php
                                if ($item['visibility']) {
                                    if(isset($item['section_id'])){
                                        echo '<li>' . $this->Html->link($item['label'], $item['link'], ['class' => 'section_link_'.$item['section_id'],'ng-init' => 'addCountListener('.$item['section_id'].')']) . '</li>';
                                    } else {
                                        echo '<li>' . $this->Html->link($item['label'], $item['link']) . '</li>';
                                    }
                                }
                        ?>
                        <?php endforeach; ?>
                    </ul>
                </li>
                <?php endforeach; ?>
            </ul>


            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <?= $this->Flash->render() ?>
            <?php echo $this->fetch('content'); ?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark" id="aside-right">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <!-- /.tab-pane -->

            <div class="tab-pane active" id="control-sidebar-home-tab">
                <h3 class="control-sidebar-heading">Kolorystyka panelu</h3>
                <ul class="list-unstyled clearfix">
                    <?php
                $names = array('Niebieski', 'Fioletowy','Zielony','Czerwony','Żółty', 'Różowy');
                $skins = array('blue', 'purple','green','red','yellow','pink');
                for ($i = 0; $i < 6; $i++) {
                ?>
                    <li style="float:left; width: 33.33333%; padding: 5px;">
                        <a href="" ng-click="saveSkin('<?= $skins[$i] ?>', '<?= $auth_user->id ?>')"
                           style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)"
                           class="clearfix full-opacity-hover">
                            <div><span style="display:block; width: 20%; float: left; height: 7px;"
                                       class="bg-<?= $skins[$i] ?>-active"></span>
                                <span class="bg-<?= $skins[$i] ?>"
                                      style="display:block; width: 80%; float: left; height: 7px;"></span></div>
                            <div><span
                                    style="display:block; width: 20%; float: left; height: 20px; background: #222d32;"></span>
                                <span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;"></span>
                            </div>
                        </a>
                        <p class="text-center no-margin"><?= $names[$i] ?></p></li>
                    <?php
                }
                ?>
                </ul>

                <h3 class="control-sidebar-heading">Szerokość witryny</h3>
                <ul class="control-sidebar-menu">
                    <li><a href="" class="control-sidebar-subheading" ng-click="setSizeFixed()">Pełna</a></li>
                    <li><a href="" class="control-sidebar-subheading" ng-click="setSizecBoxed()"> Pudełko </a></li>
                </ul>

                <h3 class="control-sidebar-heading">Ustawienia paska</h3>
                <ul class="control-sidebar-menu">
                    <li><a href="" class="control-sidebar-subheading"
                           ng-click="setSidebarCollapse('<?= $auth_user->id ?>')">Pasek chowak</a></li>
                    <li><a href="" class="control-sidebar-subheading" ng-click="setSidebarMini('<?= $auth_user->id ?>')">Mały
                        pasek</a></li>
                    <li><a href="" class="control-sidebar-subheading"
                           ng-click="setSidebarNormal('<?= $auth_user->id ?>')">Pasek normalny</a></li>


                </ul>
                <!-- /.control-sidebar-menu -->

            </div>
            <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<?= $this->Html->script('jquery-2.2.4.min'); ?>
<?= $this->Html->script('bootstrap.min.js'); ?>
<script>
    var AdminLTEOptions = {
        //Enable sidebar expand on hover effect for sidebar mini
        //This option is forced to true if both the fixed layout and sidebar mini
        //are used together
        sidebarExpandOnHover: true,
        //BoxRefresh Plugin
        enableBoxRefresh: true,
        //Bootstrap.js tooltip
        enableBSToppltip: true,
        colors: {
            lightBlue: "#3c8dbc",
            red: "#D50000",
            green: "#00a65a",
            aqua: "#00c0ef",
            yellow: "#f39c12",
            blue: "#0073b7",
            navy: "#001F3F",
            teal: "#39CCCC",
            olive: "#3D9970",
            lime: "#01FF70",
            orange: "#FF851B",
            fuchsia: "#F012BE",
            purple: "#8E24AA",
            maroon: "#D81B60",
            black: "#222222",
            gray: "#d2d6de"
        }
    };
</script>
<?= $this->Html->script('app.min.js'); ?>

<?= $this->Html->script('angular.min.js'); ?>
<?= $this->Html->script('angular-sanitize.js'); ?>
<?= $this->Html->script('angular-route.min.js'); ?>
<?= $this->Html->script('angular-animate.min.js'); ?>
<?= $this->Html->script('controllers/app.js'); ?>
<?= $this->Html->script('controllers/directives.js'); ?>
<?= $this->Html->script('controllers/fakturyCtrl.js'); ?>
<?= $this->Html->script('controllers/reviewCtrl.js'); ?>
<?= $this->Html->script('controllers/ordersCtrl.js'); ?>
<?= $this->Html->script('controllers/usersCtrl.js'); ?>
<?= $this->Html->script('controllers/productsCtrl.js'); ?>
<?= $this->Html->script('controllers/messagesCtrl.js'); ?>
<?= $this->Html->script('controllers/textsCtrl.js'); ?>
<?= $this->Html->script('controllers/dashboardCtrl.js'); ?>
<?= $this->Html->script('controllers/stepsCtrl.js'); ?>
<?= $this->Html->script('controllers/notificationsCtrl.js'); ?>
<?= $this->Html->script('bootstrap-datepicker.min'); ?>
<?= $this->Html->script('jquery-ui'); ?>
<?= $this->Html->script('slimScroll/jquery.slimscroll.min'); ?>
<?= $this->Html->script('toastr.min'); ?>
<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/angular-scroll/1.0.0/angular-scroll.min.js'); ?>
<?= $this->Html->script('jquery.custom-scrollbar'); ?>
<?= $this->Html->script('bootstrap-select.min'); ?>
<?= $this->Html->script('angular-multi-step-form'); ?>
<?= $this->Html->script('https://vitalets.github.io/checklist-model/checklist-model.js'); ?>
<?= $this->Html->script('angular-multi-select.js'); ?>
<?= $this->Html->script('checklist-model.js'); ?>

<script>

    $(document).ready(function () {
        if ('<?= $auth_user->show_welcome ?>' == '1') {
            $("#splashscreen").slideDown(500, function () {

            });
            $('#main_wrapper').css('display', 'none');
            var message = '<?php echo "Witaj " . $auth_user->slug; ?>';

            var i = 0;                     //  set your counter to 1

            function myLoop() {           //  create a loop function
                setTimeout(function () {
                    $("#message_header").append(message.charAt(i)); //  call a 3s setTimeout when the loop is called
                    i++;                     //  increment the counter
                    if (i < message.length) {            //  if the counter < 10, call the loop function
                        myLoop();             //  ..  again which will trigger another
                    } else {
                        setTimeout(function () {
                            $("#splashscreen").hide("slide", {direction: "down"}, 500, function () {
                                $(this).remove();
                                $("#main_wrapper").show(1000);
                            });
                        }, 500)
                    }
                }, 150)
            }

            myLoop();
            function blink(selector) {
                $(selector).fadeOut('medium', function () {
                    $(this).fadeIn('medium', function () {
                        blink(this);
                    });
                });
            }

            blink('#blinking_line');
        } else {
            $('#splashscreen').css('display', 'none');
        }
    });


    $(document).ready(function () {
        $(".container").customScrollbar();
    });
</script>
<div id="task_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Szczegóły zadania</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="well">
                            <div class="row text-center">
                                <img src="{{ currentTask.avatar }}" class="img-circle center-block"
                                     style="max-height: 200px;">
                                <hr style="margin-bottom: 10px;">
                                <b>
                                    {{ currentTask.first_name }}
                                    {{ currentTask.last_name }}
                                </b>
                                <hr style="margin-top: 10px;">
                            </div>
                            <div class="row text-center">
                                <button type="button" class="btn btn-primary"
                                        ng-click="doTask(currentTask.id); showTasks();" data-dismiss="modal">Zrobione !
                                </button>
                                <button type="button" class="btn" data-dismiss="modal">Zamknij</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <p><b>Data: </b>{{ currentTask.date | date:'d-M-yyyy' }}</p>
                        <p><b>Treść: </b>{{ currentTask.text }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="notification_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Szczegóły powiadomienia</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="well">
                            <div class="row text-center">
                                <img src="{{ currentNotification.avatar }}" class="img-circle center-block"
                                     style="max-height: 200px;">
                                <hr style="margin-bottom: 10px;">
                                <b>
                                    {{ currentNotification.first_name }}
                                    {{ currentNotification.last_name }}
                                </b>
                                <hr style="margin-top: 10px;">
                            </div>
                            <div class="row text-center">
                                <button type="button" class="btn" data-dismiss="modal">Zamknij</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <p><b>Data: </b>{{ currentNotification.date | date:'d-M-yyyy' }}</p>
                        <p><b>Treść: </b>{{ currentNotification.text }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
