<div class="panel panel-primary" ng-controller="dashboardCtrl">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-4">
                <button class="btn btn-default pull-left" ng-click="$previousStep();bindModels();" disabled>Wstecz
                </button>
            </div>
            <div class="col-md-4"><h3>Dodawanie produktu [1/2]</h3></div>
            <div class="col-md-4">
                <button class="btn btn-default pull-right" ng-click="$nextStep();bindModels();">Dalej</button>
            </div>
        </div>
    </div>
    <div class="panel-body" ng-scope="stepsCtrl">
        <div class="col-md-6">
            <table class="table">
                <tr>
                    <td colspan="2">
                        <select class="form-control" ng-model="data.category_id">
                            <option ng-repeat="category in categories" value="{{category.id}}">{{category.name}}
                            </option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Nazwa:</td>
                    <td><input type="text" class="form-control" ng-model="data.name"></td>
                </tr>
                <tr>
                    <td>Cena:</td>
                    <td>
                        <div class="input-group"><input type="text" class="form-control" ng-model="data.sell_price">
                            <span class="input-group-addon" id="basic-addon2">zł</span></div>
                    </td>
                </tr>
                <tr>
                    <td>Kod:</td>
                    <td><input type="text" class="form-control" ng-model="data.code"></td>
                </tr>
                <tr>
                    <td>Opis:</td>
                    <td><textarea class="form-control" ng-model="data.description"
                                  style="resize: vertical">{{data.desc}}</textarea></td>
                </tr>
                <tr>
                    <td>Tagi (oddziel ;):</td>
                    <td><input type="text" class="form-control" ng-model="data.keywords"></td>
                </tr>
            </table>
        </div> <!-- Produkt i jego dane -->
        <div class="col-md-6" style="border-left: 1px solid #e5e5e5 !important;">
            <table class="table">
                <tr>
                    <td>Kolor:</td>
                    <td><input type="text" class="form-control" ng-model="data.color"></td>
                </tr>
                <tr>
                    <td>Materiał:</td>
                    <td><input type="text" class="form-control" ng-model="data.material"></td>
                </tr>
                <tr>
                    <td>Typ obcasa:</td>
                    <td><input type="text" class="form-control" ng-model="data.heel_type"></td>
                </tr>
                <tr>
                    <td>Wysokość obcasa:</td>
                    <td>
                        <div class="input-group"><input type="text" class="form-control" ng-model="data.heel_height">
                    <span class="input-group-addon" id="basic-addon1">cm</span></div>
                    </td>
                </tr>
                <tr>
                    <td>Wzór:</td>
                    <td><input type="text" class="form-control" ng-model="data.pattern"></td>
                </tr>
                <tr>
                    <td>Zapięcie:</td>
                    <td><input type="text" class="form-control" ng-model="data.shoe_fastener"></td>
                </tr>
                <tr>
                    <td>Typ noska:</td>
                    <td><input type="text" class="form-control" ng-model="data.shoe_toecap"></td>
                </tr>
                <tr>
                    <td>Podeszwa:</td>
                    <td><input type="text" class="form-control" ng-model="data.sole"></td>
                </tr>
                <tr>
                    <td>Dodatkowe:</td>
                    <td><textarea class="form-control" ng-model="data.extras"
                                  style="resize: vertical">{{data.extras}}</textarea></td>
                </tr>
                <tr>
                    <td>Aktywny?</td>
                    <td>
                        <select class="form-control" ng-model="data.status">
                            <option value="1">Tak</option>
                            <option value="0">Nie</option>
                        </select>
                    </td>
                </tr>
            </table>
        </div> <!-- elementy produktu - kolor, materiał itp -->
    </div>
</div>