<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>BORN2BE.pl | panel administracyjny</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <?= $this->Html->css('login'); ?>
        <?= $this->Html->css('bootstrap.min'); ?>
    </head>
    <body>
        <div class="row">
            <div class="Absolute-Center is-Responsive">
                <div class="white-container">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="logo-container"><span class="helper"></span><?php echo $this->Html->image('logo.png', ['class' => 'img img-responsive']); ?></div>
                        </div>
                    </div>
                    <div class="row form-container">
                        <div class="col-md-12 col-xs-12 col-lg-12 col-sm-12">
                            <?= $this->Form->create(); ?>
                            <?= $this->Flash->render() ?>
                            <div class="form-group input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <?= $this->Form->input('email', ['label' => false, 'class' => 'form-control', 'placeholder' => 'Email']); ?>
                            </div>
                            <div class="form-group input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                <?= $this->Form->input('password', ['label' => false, 'class' => 'form-control', 'placeholder' => 'Hasło']); ?>    
                            </div>
                            <div class="form-group">
                                <?= $this->Form->button('Login', ['label' => false, 'class' => 'btn btn-info']); ?> 
                            </div>
                            <?= $this->Form->end() ?>      
                        </div>
                    </div>
                </div>
            </div>    
        </div>
        <?= $this->Html->script('jquery-2.2.4.min'); ?>
        <?= $this->Html->script('bootstrap.min.js'); ?>
    </body>
</html>