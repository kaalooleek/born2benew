<section>
    <link rel="stylesheet" href="../webroot/css/pdf.css" />
    <div>
        <div>
            <h2>
                <i>Killersites</i>
                <small id="invoice_date"><?= $invoice->invoice_date; ?></small><hr>
            </h2>
        </div>
    </div>
    <table border="0">
        <tr>
        <td id="seller">
            Sprzedawca:
            <address>
                <strong>Killersites Artur Bogucki</strong><br>
                Starowiejska 271<br>
                08-110 Siedlce<br>
                artur.j.bogucki@gmail.com<br>
                +48 22 113 70 70
            </address>
        </td>
        <td>
            Nabywca:
            <address>
                <span><?= $invoice->name ?></span>
                <br>
                <span><?= $invoice->address ?></span>
                <br>
                <span><?= $invoice->town ?></span>
                <br>
                <span><?= $invoice->phone ?></span>
                <br>
                <span><?= $invoice->email ?></span>
                <br>
            </address>
        </td>
        </tr>
    </table>
        <div id="details">
            <b>Faktura <?= $invoice->numer ?></b><br>
            <b>ID Zamówienia:</b> <?= $invoice->id ?><br>
            <b>Termin płatności:</b> <?= $invoice->payment_term ?><br>
        </div>
    <div>
        <div>
            <table>
                <thead>
                <tr>
                    <th class="cell tab">Nazwa</th>
                    <th class="cell tab">Ilość</th>
                    <th class="cell tab">JM</th>
                    <th class="cell tab">Cena</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($invoice->items as $item): ?>
                <tr>
                    <td class="tab2"><?= $item->name ?></td>
                    <td class="tab2"><?= $item->quantity ?></td>
                    <td class="tab2"><?= $item->jm ?></td>
                    <td class="tab2"><?php echo $item->price . " " . $item->currency ?></td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
        <div id="payment">
            <b>Metoda płatności:</b><br>
            <?php echo $invoice->payment_type ?>
        </div>
</section>
