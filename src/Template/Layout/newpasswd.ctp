<?= $this->Html->css('login'); ?>
<?= $this->Html->css('bootstrap.min'); ?>

<div class="container">
    <div class="row">
        <div class="Absolute-Center is-Responsive">
            <div class="white-container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="logo-container"><span class="helper"></span><?php echo $this->Html->image('logo.png', ['class' => 'img img-responsive']); ?></div>
                    </div>
                </div>
                <div class="row form-container">
                    <div class="col-md-12 col-xs-12 col-lg-12 col-sm-12">
                        <?= $this->Form->create(); ?>
                        <?= $this->Flash->render() ?>
                        <div class="form-group">
                            <p>Logujesz się tymczasowo wygenerowanym hasłem, ustaw nowe hasło dla swojego konta</p>
                        </div>
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <?= $this->Form->input('password', ['label' => false, 'class' => 'form-control', 'placeholder' => 'Hasło']); ?>    
                        </div>
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <?= $this->Form->input('password_repeat', ['label' => false, 'class' => 'form-control', 'type' => 'password', 'placeholder' => 'Powtórz hasło']); ?>
                        </div>
                        <div class="form-group">
                            <?= $this->Form->button('Login', ['label' => false, 'class' => 'btn btn-info']); ?> 
                        </div>
                        <?= $this->Form->end() ?>     
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>    