<div class="panel panel-primary" ng-controller="dashboardCtrl">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-4">
                <button class="btn btn-default pull-left" ng-click="$previousStep();bindModels();">Wstecz</button>

            </div>
            <div class="col-md-4"><h3>Dodawanie produktu [2/2]</h3></div>
            <div class="col-md-4">
                <button class="btn btn-default pull-right" ng-click="saveproduct()">Zapisz</button>
            </div>
        </div>
    </div>
    <div class="panel-body" ng-scope="stepsCtrl">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8" style="border: 1px solid #e5e5e5 !important; min-height: 150px;">
                <div class="col-sm-4" ng-repeat="file in files"><a href="javascript:void(0)" class="thumbnail" ng-click="delete_file($index)"><img title="{{file.name}}" src="{{file.link}}" class="img-responsive"></a></div>
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row" style="padding-top: 1%;">
            <div class="col-md-2"></div>
            <div class="col-md-8" style="border: 1px solid #e5e5e5 !important;"><input type="file" ng-model="data.image" onchange="angular.element(this).scope().previewFiles()" multiple name="fileToUpload" id="fileToUpload" style="padding-top: 1%; padding-bottom: 1%;" ng-init="init_files()"></div>
            <div class="col-md-2"></div>
        </div>
    </div>
</div>