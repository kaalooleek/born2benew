<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Categories'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Pgroups'), ['controller' => 'Pgroups', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pgroup'), ['controller' => 'Pgroups', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="categories form large-9 medium-8 columns content">
    <?= $this->Form->create($category) ?>
    <fieldset>
        <legend><?= __('Add Category') ?></legend>
        <?php
            echo $this->Form->input('lft');
            echo $this->Form->input('rght');
            echo $this->Form->input('parent');
            echo $this->Form->input('name');
            echo $this->Form->input('ger_name');
            echo $this->Form->input('eng_name');
            echo $this->Form->input('slug');
            echo $this->Form->input('eng_slug');
            echo $this->Form->input('ger_slug');
            echo $this->Form->input('products._ids', ['options' => $products]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
