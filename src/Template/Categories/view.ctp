<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Category'), ['action' => 'edit', $category->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Category'), ['action' => 'delete', $category->id], ['confirm' => __('Are you sure you want to delete # {0}?', $category->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Categories'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Category'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Pgroups'), ['controller' => 'Pgroups', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pgroup'), ['controller' => 'Pgroups', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="categories view large-9 medium-8 columns content">
    <h3><?= h($category->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($category->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Ger Name') ?></th>
            <td><?= h($category->ger_name) ?></td>
        </tr>
        <tr>
            <th><?= __('Eng Name') ?></th>
            <td><?= h($category->eng_name) ?></td>
        </tr>
        <tr>
            <th><?= __('Slug') ?></th>
            <td><?= h($category->slug) ?></td>
        </tr>
        <tr>
            <th><?= __('Eng Slug') ?></th>
            <td><?= h($category->eng_slug) ?></td>
        </tr>
        <tr>
            <th><?= __('Ger Slug') ?></th>
            <td><?= h($category->ger_slug) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($category->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Lft') ?></th>
            <td><?= $this->Number->format($category->lft) ?></td>
        </tr>
        <tr>
            <th><?= __('Rght') ?></th>
            <td><?= $this->Number->format($category->rght) ?></td>
        </tr>
        <tr>
            <th><?= __('Parent') ?></th>
            <td><?= $this->Number->format($category->parent) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Pgroups') ?></h4>
        <?php if (!empty($category->pgroups)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Category Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($category->pgroups as $pgroups): ?>
            <tr>
                <td><?= h($pgroups->id) ?></td>
                <td><?= h($pgroups->category_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Pgroups', 'action' => 'view', $pgroups->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Pgroups', 'action' => 'edit', $pgroups->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Pgroups', 'action' => 'delete', $pgroups->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pgroups->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Products') ?></h4>
        <?php if (!empty($category->products)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Pgroup Id') ?></th>
                <th><?= __('Category Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Slug') ?></th>
                <th><?= __('Eng Slug') ?></th>
                <th><?= __('Ger Slug') ?></th>
                <th><?= __('Slug2') ?></th>
                <th><?= __('Buy Price') ?></th>
                <th><?= __('Sell Price') ?></th>
                <th><?= __('Price') ?></th>
                <th><?= __('Ger Price') ?></th>
                <th><?= __('Shoelala Price') ?></th>
                <th><?= __('Shoelala Sell Price') ?></th>
                <th><?= __('Sale') ?></th>
                <th><?= __('Shipping Price Courier') ?></th>
                <th><?= __('Shipping Price Cod') ?></th>
                <th><?= __('Shipping Price Inpost') ?></th>
                <th><?= __('Default Sale') ?></th>
                <th><?= __('Code') ?></th>
                <th><?= __('Title') ?></th>
                <th><?= __('Title Allegro') ?></th>
                <th><?= __('Keywords') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Remarks') ?></th>
                <th><?= __('Color') ?></th>
                <th><?= __('Zdjec') ?></th>
                <th><?= __('Status') ?></th>
                <th><?= __('State') ?></th>
                <th><?= __('Shoelala State') ?></th>
                <th><?= __('Ger State') ?></th>
                <th><?= __('Virtual Product') ?></th>
                <th><?= __('Score') ?></th>
                <th><?= __('Popularity') ?></th>
                <th><?= __('Views') ?></th>
                <th><?= __('ProductDetailViews') ?></th>
                <th><?= __('BuyToDetailRate') ?></th>
                <th><?= __('CartToDetailRate') ?></th>
                <th><?= __('ProductAddsToCart') ?></th>
                <th><?= __('ProductCheckouts') ?></th>
                <th><?= __('ProductListClicks') ?></th>
                <th><?= __('ProductListViews') ?></th>
                <th><?= __('Av Time Spent') ?></th>
                <th><?= __('Cart Score') ?></th>
                <th><?= __('Sizing') ?></th>
                <th><?= __('Total') ?></th>
                <th><?= __('Virtual Total') ?></th>
                <th><?= __('Waiting Total') ?></th>
                <th><?= __('Shop 1 Total') ?></th>
                <th><?= __('Shop 2 Total') ?></th>
                <th><?= __('Shelf') ?></th>
                <th><?= __('Mini') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th><?= __('Make') ?></th>
                <th><?= __('Season') ?></th>
                <th><?= __('Title Name') ?></th>
                <th><?= __('Title Extra') ?></th>
                <th><?= __('Ger Title') ?></th>
                <th><?= __('Ger Keywords') ?></th>
                <th><?= __('Ger Description') ?></th>
                <th><?= __('Eng Title') ?></th>
                <th><?= __('Eng Keywords') ?></th>
                <th><?= __('Eng Description') ?></th>
                <th><?= __('Ger Title Name') ?></th>
                <th><?= __('Ger Title Extra') ?></th>
                <th><?= __('Eng Title Extra') ?></th>
                <th><?= __('Eng Title Name') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($category->products as $products): ?>
            <tr>
                <td><?= h($products->id) ?></td>
                <td><?= h($products->pgroup_id) ?></td>
                <td><?= h($products->category_id) ?></td>
                <td><?= h($products->name) ?></td>
                <td><?= h($products->slug) ?></td>
                <td><?= h($products->eng_slug) ?></td>
                <td><?= h($products->ger_slug) ?></td>
                <td><?= h($products->slug2) ?></td>
                <td><?= h($products->buy_price) ?></td>
                <td><?= h($products->sell_price) ?></td>
                <td><?= h($products->price) ?></td>
                <td><?= h($products->ger_price) ?></td>
                <td><?= h($products->shoelala_price) ?></td>
                <td><?= h($products->shoelala_sell_price) ?></td>
                <td><?= h($products->sale) ?></td>
                <td><?= h($products->shipping_price_courier) ?></td>
                <td><?= h($products->shipping_price_cod) ?></td>
                <td><?= h($products->shipping_price_inpost) ?></td>
                <td><?= h($products->default_sale) ?></td>
                <td><?= h($products->code) ?></td>
                <td><?= h($products->title) ?></td>
                <td><?= h($products->title_allegro) ?></td>
                <td><?= h($products->keywords) ?></td>
                <td><?= h($products->description) ?></td>
                <td><?= h($products->remarks) ?></td>
                <td><?= h($products->color) ?></td>
                <td><?= h($products->zdjec) ?></td>
                <td><?= h($products->status) ?></td>
                <td><?= h($products->state) ?></td>
                <td><?= h($products->shoelala_state) ?></td>
                <td><?= h($products->ger_state) ?></td>
                <td><?= h($products->virtual_product) ?></td>
                <td><?= h($products->score) ?></td>
                <td><?= h($products->popularity) ?></td>
                <td><?= h($products->views) ?></td>
                <td><?= h($products->productDetailViews) ?></td>
                <td><?= h($products->buyToDetailRate) ?></td>
                <td><?= h($products->cartToDetailRate) ?></td>
                <td><?= h($products->productAddsToCart) ?></td>
                <td><?= h($products->productCheckouts) ?></td>
                <td><?= h($products->productListClicks) ?></td>
                <td><?= h($products->productListViews) ?></td>
                <td><?= h($products->av_time_spent) ?></td>
                <td><?= h($products->cart_score) ?></td>
                <td><?= h($products->sizing) ?></td>
                <td><?= h($products->total) ?></td>
                <td><?= h($products->virtual_total) ?></td>
                <td><?= h($products->waiting_total) ?></td>
                <td><?= h($products->shop_1_total) ?></td>
                <td><?= h($products->shop_2_total) ?></td>
                <td><?= h($products->shelf) ?></td>
                <td><?= h($products->mini) ?></td>
                <td><?= h($products->created) ?></td>
                <td><?= h($products->modified) ?></td>
                <td><?= h($products->make) ?></td>
                <td><?= h($products->season) ?></td>
                <td><?= h($products->title_name) ?></td>
                <td><?= h($products->title_extra) ?></td>
                <td><?= h($products->ger_title) ?></td>
                <td><?= h($products->ger_keywords) ?></td>
                <td><?= h($products->ger_description) ?></td>
                <td><?= h($products->eng_title) ?></td>
                <td><?= h($products->eng_keywords) ?></td>
                <td><?= h($products->eng_description) ?></td>
                <td><?= h($products->ger_title_name) ?></td>
                <td><?= h($products->ger_title_extra) ?></td>
                <td><?= h($products->eng_title_extra) ?></td>
                <td><?= h($products->eng_title_name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Products', 'action' => 'view', $products->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Products', 'action' => 'edit', $products->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Products', 'action' => 'delete', $products->id], ['confirm' => __('Are you sure you want to delete # {0}?', $products->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
