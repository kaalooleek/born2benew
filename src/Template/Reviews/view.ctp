
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Review'), ['action' => 'edit', $review->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Review'), ['action' => 'delete', $review->id], ['confirm' => __('Are you sure you want to delete # {0}?', $review->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Reviews'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Review'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Websites'), ['controller' => 'Websites', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Website'), ['controller' => 'Websites', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="reviews view large-9 medium-8 columns content">
    <h3><?= h($review->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Product') ?></th>
            <td><?= $review->has('product') ? $this->Html->link($review->product->name, ['controller' => 'Products', 'action' => 'view', $review->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Website') ?></th>
            <td><?= $review->has('website') ? $this->Html->link($review->website->name, ['controller' => 'Websites', 'action' => 'view', $review->website->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Review') ?></th>
            <td><?= h($review->review) ?></td>
        </tr>
        <tr>
            <th><?= __('Language') ?></th>
            <td><?= h($review->language) ?></td>
        </tr>
        <tr>
            <th><?= __('Rating') ?></th>
            <td><?= h($review->rating) ?></td>
        </tr>
        <tr>
            <th><?= __('Rating Comfort') ?></th>
            <td><?= h($review->rating_comfort) ?></td>
        </tr>
        <tr>
            <th><?= __('Rating Style') ?></th>
            <td><?= h($review->rating_style) ?></td>
        </tr>
        <tr>
            <th><?= __('Sizing') ?></th>
            <td><?= h($review->sizing) ?></td>
        </tr>
        <tr>
            <th><?= __('Status') ?></th>
            <td><?= h($review->status) ?></td>
        </tr>
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($review->name) ?></td>
        </tr>
        <tr>
            <th><?= __('City') ?></th>
            <td><?= h($review->city) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($review->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Thumbsup') ?></th>
            <td><?= $this->Number->format($review->thumbsup) ?></td>
        </tr>
        <tr>
            <th><?= __('Thumbsdown') ?></th>
            <td><?= $this->Number->format($review->thumbsdown) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($review->created) ?></td>
        </tr>
    </table>
</div>
