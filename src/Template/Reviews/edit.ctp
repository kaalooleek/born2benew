<div class="panel panel-primary">
    <div class="panel-heading">Edycja recenzji: <?= $review->id ?></div>
    <div class="panel-body">
        <h4>Treść:</h4>
        <textarea style='width:100%;' rows='5' class='form-control'><?= $review->review ?></textarea>
        <h4>Imię:</h4>
        <input type='text' class='form-control' value='<?= $review->name ?>' />
        <h4>Ocena:</h4>
        <select class='form-control'>
            <option value='5'>5/5</option>
            <option value='4'>4/5</option>
            <option value='3'>3/5</option>
            <option value='2'>2/5</option>
            <option value='1'>1/5</option>
        </select>
        <h4>Ocena stylu:</h4>
        <select class='form-control'>
            <option value='5'>5/5</option>
            <option value='4'>4/5</option>
            <option value='3'>3/5</option>
            <option value='2'>2/5</option>
            <option value='1'>1/5</option>
        </select>
        <h4>Ocena komfortu:</h4>
        <select class='form-control'>
            <option value='5'>5/5</option>
            <option value='4'>4/5</option>
            <option value='3'>3/5</option>
            <option value='2'>2/5</option>
            <option value='1'>1/5</option>
        </select>
        <h4>Język:</h4>
        <input type='text' class='form-control' value='<?= $review->language ?>' />
        <h4>Status:</h4>
        <select class='form-control'>
            <option value='5'>Aktywna</option>
            <option value='4'>Nieaktywna</option>
        </select>
        <br>
        <button type="submit" class="btn btn-primary"">Zapisz</button>
    </div>
</div>