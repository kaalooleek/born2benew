<div class="panel panel-primary" ng-controller="reviewCtrl">
    <div class="panel-heading">Dodaj nową recenzje</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <table class="table">
                    <tr>
                        <td>Recenzja: </td>
                        <td><textarea style="resize: vertical" class="form-control" ng-model="data.review" title="Pole wymagane" required></textarea></td>
                    </tr>
                    <tr>
                        <td>Imie:</td>
                        <td><input type="text" class="form-control" ng-model="data.name" title="Pole wymagane" required></td>
                    </tr>
                    <tr>
                        <td>Miasto:</td>
                        <td><input type="text" class="form-control" ng-model="data.city" title="Pole wymagane" required></td>
                    </tr>
                    <tr>
                        <td>Ocena: </td>
                        <td>
                            <select class="form-control" ng-model="data.rating" title="Pole wymagane" required>
                                <option value="1">1/5</option>
                                <option value="2">2/5</option>
                                <option value="3">3/5</option>
                                <option value="4">4/5</option>
                                <option value="5">5/5</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Ocena komfortu: </td>
                        <td>
                            <select class="form-control" ng-model="data.rating_comfort" title="Pole wymagane" required>
                                <option value="1">1/5</option>
                                <option value="2">2/5</option>
                                <option value="3">3/5</option>
                                <option value="4">4/5</option>
                                <option value="5">5/5</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Ocena stylu: </td>
                        <td>
                            <select class="form-control" ng-model="data.rating_style" title="Pole wymagane" required>
                                <option value="1">1/5</option>
                                <option value="2">2/5</option>
                                <option value="3">3/5</option>
                                <option value="4">4/5</option>
                                <option value="5">5/5</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Ocena rozmiaru: </td>
                        <td>
                            <select class="form-control" ng-model="data.rating_sizing" title="Pole wymagane" required>
                                <option value="1">1/5</option>
                                <option value="2">2/5</option>
                                <option value="3">3/5</option>
                                <option value="4">4/5</option>
                                <option value="5">5/5</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-8" style="border-left: 1px solid #e5e5e5 !important;">
                <div class="col-md-12 table-responsive" style="max-height: 500px;">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Box</th>
                            <th>Kategoria</th>
                            <th>Nazwa</th>
                            <th>Cena</th>
                            <th>Kod</th>
                            <th>Słowa</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="x in products" class="">
                            <td>{{ x.id }}</td>
                            <td><input type="checkbox" checklist-model="data.ids" checklist-value="x.id"></td>
                            <td>{{ x.category_id }}</td>
                            <td class="long-text">{{ x.name }}</td>
                            <td>{{ x.sell_price }}</td>
                            <td>{{ x.code }}</td>
                            <td class="long-text">{{ x.keywords }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <button class="btn btn-primary" ng-click="create();" style="margin-top: 10px;">Utwórz</button>
        </div>
    </div>
</div>