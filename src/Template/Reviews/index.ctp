<style>
    .long-text {
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        max-width: 200px;
    }
</style>
<div class="panel panel-primary" ng-controller="reviewCtrl">
    <div class="panel-heading"><?= __('Recenzje') ?></div>
    <div class="panel-body">
        <div class="table-responsive">
            <table cellpadding="0" cellspacing="0" class="table table-hover">
                <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id', 'ID') ?></th>
                    <th><?= $this->Paginator->sort('review','Recenzja') ?></th>
                    <th><?= $this->Paginator->sort('rating','Ocena') ?></th>
                    <th><?= $this->Paginator->sort('rating_comfort','Ocena komfortu') ?></th>
                    <th><?= $this->Paginator->sort('rating_style','Ocena stylu') ?></th>
                    <th><?= $this->Paginator->sort('sizing', 'Ocena rozmiaru') ?></th>
                    <th><?= $this->Paginator->sort('thumbs_up', 'Likes') ?></th>
                    <th><?= $this->Paginator->sort('thumbs_down', 'Dislikes') ?></th>
                    <th><?= $this->Paginator->sort('status', 'Status') ?></th>
                    <th><?= $this->Paginator->sort('name','Imie') ?></th>
                    <th><?= $this->Paginator->sort('city','Miasto') ?></th>
                    <th><?= $this->Paginator->sort('created','Utworzono') ?></th>
                    <th class="actions"></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($reviews as $review): ?>
                <tr>
                    <td><?= $this->Number->format($review->id) ?></td>
                    <td class="long-text" id="review_<?= $this->Number->format($review->id) ?>"><?= h($review->
                        review)?>
                    </td>
                    <td id="rating_<?= $this->Number->format($review->id) ?>"><?= h($review->rating) ?></td>
                    <td id="rating_comfort_<?= $this->Number->format($review->id) ?>"><?= h($review->comfort)?></td>
                    <td id="rating_style_<?= $this->Number->format($review->id) ?>"><?= h($review->style) ?></td>
                    <td id="rating_sizing_<?= $this->Number->format($review->id) ?>"><?= h($review->sizing) ?></td>
                    <td id="thumbs_up_<?= $this->Number->format($review->id) ?>"><?= $this->
                        Number->format($review->thumbs_up) ?>
                    </td>
                    <td id="thumbs_down_<?= $this->Number->format($review->id) ?>"><?= $this->
                        Number->format($review->thumbs_down) ?>
                    </td>
                    <td id="status_<?= $this->Number->format($review->id) ?>"><?= h($review->status) ?></td>
                    <td id="name_<?= $this->Number->format($review->id) ?>"><?= h($review->name) ?></td>
                    <td id="city_<?= $this->Number->format($review->id) ?>"><?= h($review->city) ?></td>
                    <td><?= h($review->created) ?></td>
                    <td>
                        <div class='btn-group' style='width:100px;'>
                            <button class="btn btn-primary"
                                    ng-click="show_review(<?= $review->id ?>)">Zobacz
                            </button>
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" style="padding:0px;border:0px;" role="menu">
                                <li>
                                    <button class="btn btn-primary dropdown-toggle"
                                            style="width: 96px;border:1px;height:34px;margin:0px;padding:0px;"
                                            ng-click="delete_review(<?= $review->id ?>)">Usuń
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                </ul>
                <p><?= $this->Paginator->counter() ?></p>
            </div>
        </div>
    </div>
    <div class="modal modal-primary fade" id="review_modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header"
                     style="background-color: white !important; color: #333 !important; border-bottom: 1px solid #e5e5e5 !important;">
                    <h4 class="modal-title">Szczegóły recenzji</h4>
                </div>
                <div class="modal-body" style="background-color: white !important; color: #333 !important;">
                    <div class="row">
                        <div class="col-md-6" style="">
                            <table class="table">
                                <tr>
                                    <td>
                                        <b>Imie:</b>
                                        <div style="display:inline-block;min-width:50px;min-height:17px;" id="name"
                                             data-toggle="tooltip" title="Dwukrotne kliknięcie - edytuje pole">
                                            {{review.name}}
                                        </div>
                                        <div class="form-group input-group" id="name_group" style="display: none;">
                                    <span class="input-group-addon"
                                          ng-click="save_input(review.id, 'name', name_input);"><i
                                            class="glyphicon glyphicon-save"></i></span>
                                            <input type="text" id="name_input" ng-model="name_input"
                                                   class="form-control">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Miasto:</b>
                                        <div style="display:inline-block;min-width:50px;min-height:17px;" id="city"
                                             title="Dwukrotne kliknięcie - edytuje pole">{{review.city}}
                                        </div>
                                        <div class="form-group input-group" id="city_group" style="display: none;">
                                    <span class="input-group-addon"
                                          ng-click="save_input(review.id, 'city', city_input);"><i
                                            class="glyphicon glyphicon-save"></i></span>
                                            <input type="text" id="city_input" ng-model="city_input"
                                                   class="form-control">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Status:</b>
                                        <div style="display:inline-block;min-width:50px;min-height:17px;" id="status"
                                             title="Dwukrotne kliknięcie - edytuje pole">{{review.status}}
                                        </div>
                                        <div class="form-group input-group" id="status_group" style="display: none;">
                                    <span class="input-group-addon"
                                          ng-click="save_input(review.id, 'status', status_input);"><i
                                            class="glyphicon glyphicon-save"></i></span>
                                            <select id="status_input" ng-model="status_input" class='form-control'>
                                                <option ng-repeat="x in status" {{x.select}} value="{{x.value}}">
                                                    {{x.name}}
                                                </option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Ocena:</b>
                                        <div style="display:inline-block;min-width:50px;min-height:17px;" id="rating"
                                             title="Dwukrotne kliknięcie - edytuje pole">{{review.rating}}
                                        </div>
                                        <div class="form-group input-group" id="rating_group" style="display: none;">
                                    <span class="input-group-addon"
                                          ng-click="save_input(review.id, 'rating', rating_input);"><i
                                            class="glyphicon glyphicon-save"></i></span>
                                            <select id='rating_input' ng-selected="true" ng-model="rating_input"
                                                    class='form-control'>
                                                <option ng-repeat="x in rating" {{x.select}} value="{{x.value}}">
                                                    {{x.name}}
                                                </option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Ocena komfortu:</b>
                                        <div style="display:inline-block;min-width:50px;min-height:17px;"
                                             id="rating_comfort"
                                             title="Dwukrotne kliknięcie - edytuje pole">{{review.comfort}}
                                        </div>
                                        <div class="form-group input-group" id="rating_comfort_group"
                                             style="display: none;">
                                    <span class="input-group-addon"
                                          ng-click="save_input(review.id, 'comfort', rating_comfort_input);"><i
                                            class="glyphicon glyphicon-save"></i></span>
                                            <select id='rating_comfort_input' ng-model="rating_comfort_input"
                                                    class='form-control'>
                                                <option ng-repeat="x in rating" {{x.select}} value="{{x.value}}">
                                                    {{x.name}}
                                                </option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Ocena stylu:</b>
                                        <div style="display:inline-block;min-width:50px;min-height:17px;"
                                             id="rating_style"
                                             title="Dwukrotne kliknięcie - edytuje pole">{{review.style}}
                                        </div>
                                        <div class="form-group input-group" id="rating_style_group"
                                             style="display: none;">
                                    <span class="input-group-addon"
                                          ng-click="save_input(review.id, 'style', rating_style_input);"><i
                                            class="glyphicon glyphicon-save"></i></span>
                                            <select id='rating_style_input' ng-model="rating_style_input"
                                                    class='form-control'>
                                                <option ng-repeat="x in rating" {{x.select}} value="{{x.value}}">
                                                    {{x.name}}
                                                </option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Ocena rozmiaru:</b>
                                        <div style="display:inline-block;min-width:50px;min-height:17px;"
                                             id="rating_sizing"
                                             title="Dwukrotne kliknięcie - edytuje pole">{{review.sizing}}
                                        </div>
                                        <div class="form-group input-group" id="rating_sizing_group"
                                             style="display: none;">
                                    <span class="input-group-addon"
                                          ng-click="save_input(review.id, 'sizing', rating_sizing_input);"><i
                                            class="glyphicon glyphicon-save"></i></span>
                                            <select id='rating_sizing_input' ng-model="rating_sizing_input"
                                                    class='form-control'>
                                                <option ng-repeat="x in rating" {{x.select}} value="{{x.value}}">
                                                    {{x.name}}
                                                </option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="long-text" style="border-bottom: 1px solid #F4F4F4 !important;">
                                        <b>Recenzja:</b>
                                        <div style="display:block;min-width:350px;min-height:17px; white-space: normal;"
                                             id="review"
                                             title="Dwukrotne kliknięcie - edytuje pole">{{review.review}}
                                        </div>
                                        <div class="form-group input-group" id="review_group" style="display: none;">
                                            <div style="padding: 0px" class="input-group-addon">
                                                <div class="input-group-addon" style="border:0px;"
                                                     ng-click="save_input(review.id, 'review', review_input);"><i
                                                        class="glyphicon glyphicon-save"></i></div>
                                            </div>
                                            <div style="padding: 0px" class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                                            <textarea id='review_input' ng-model="review_input" style="height: 100%;"
                                                      class='form-control' rows="5"></textarea>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-6" style="border-left: 1px solid #e5e5e5 !important;">
                            <div>
                                <h4 style="text-align: center;">{{ review.thumbs_up }}</h4>
                                <img class="img-responsive" src="webroot/img/thumb_up.png" height="50%" width="50%"
                                     ng-click="save_input(review.id, 'thumbs_up', review.thumbs_up + 1);"
                                     style="margin: 0 auto">
                            </div>
                            <div>
                                <img class="img-responsive" src="webroot/img/thumb_down.png" height="50%" width="50%"
                                     ng-click="save_input(review.id, 'thumbs_down', review.thumbs_down + 1);"
                                     style="margin: 0 auto">
                                <h4 style="text-align: center;">{{ review.thumbs_down }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"
                     style="background-color: white !important; color: #333 !important; border-top: 1px solid #e5e5e5 !important;">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Zamknij</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/born2benew/js/jquery-2.2.4.min.js"></script>
<script>
    $(document).ready(function () {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "3000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        var inputs = ['name', 'city', 'review', 'status', 'rating', 'rating_comfort', 'rating_style', 'rating_sizing'];
        for (i = 0; i < inputs.length; i++) {
            (function (i) {
                var name = inputs[i];
                $("#" + name).dblclick(function () {
                    $("#" + name).fadeToggle('fast', "linear");
                    $("#" + name + "_group").fadeToggle('fast', "linear");
                    var value = $('#' + name).text();
                    $("#" + name + "_input").val(value);
                    $("body").on('click', function (e) {
                        if (!$('#' + name + '_input').is(e.target)) {
                            var new_value = $("#" + name + "_input").val();
                            $('#' + name + '_group').fadeToggle('fast', "linear");
                            $("#" + name).fadeToggle('fast', "linear");
                            $('#' + name + '_' + review.id + '_table').fadeToggle('fast', "linear");
                            $("body").off("click");
                        }
                    });
                });
            })(i);
        }
        function show_success() {
            toastr.success('Zapisano pomyślnie!');
        }

        function show_error() {
            toastr.error('Wystąpił błąd w zapisywaniu!');
        }
    });
</script>
