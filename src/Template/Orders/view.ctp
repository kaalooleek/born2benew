<style>
    .span{
        margin-left: 30px;
    }
    .action-button{
        font-weight: bold;
        color: #2ba6cb;
        margin-bottom: 10px;

    }
    .line{
        border:solid 2px;
        border-radius: 100px;
        margin-top:50px;
        color:#CFCFCF;
    }

</style>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">Zamówienie numer: <?php echo $order->id ?></h3></div>
            <div class="panel-body">
                <table class="table" cellpadding = "10" cellspacing = "10" style="width:100%;">
                    <tr>
                        <th class="pull-right"><?php echo __('Produkt'); ?></th>
                        <th></th>
                        <th>Status</th>
                        <th><?php echo __('Cena'); ?></th>
                        <th>Akcja</th>
                        <th></th>
                    </tr>
                <?php
                $x = -1;
                foreach ($order['oitems'] as $oitem):
                    $x++;
                    ?>
                    <tr style="height:200px;">
                        <td style="width:70px;margin-left:30px;"><?php echo $photo[$x] ?></td>
                        <td>
                            <span class="span"><?php echo $product[$x]['title']; ?></span></br>
                            <span class="span"><b>Rozmiar: </b><?php echo $size[$x]['size']; ?> (<?php echo $size[$x]['total'] + $size[$x]['debit']; ?>) </span></br>
                            <span class="span"><b>Kod: </b><?php echo $product[$x]['code']; ?></span>

                            <div id="params">
                            </div>
                        </td>
                        <td>
                            <span><?php echo $this->Admin->getOitemStatus($oitem['status']); ?></span>
                        </td>

                        <td>
                            <span><?php echo $oitem['price']; ?></span>
                        </td>
                        <td>

                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> <span style="margin-top:10px;" class="action-button">Edytuj</span></br>
                            <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> <span class="action-button">Podmień</span></br>
                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <span class="action-button">Dodaj</span></br>
                            <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> <span class="action-button">Usuń</span></br>

                        </td>
                        <td>
                            <input type="number" name="lname" id="rcode<?php echo $x; ?>" style="width:70px;;"></br></br>
                            <input type="number" name="lname" id="rsize<?php echo $x; ?>" style="width:60px;"></br></br>
                            <input type="number" name="rprice" id="rprice<?php echo $x; ?>" value="<?php echo $oitem['price']; ?>" style="width:70px;">
                        </td>
                    </tr>
                    <?php
                endforeach;
                ?>    
                </table>
                <div class="panel panel-primary">
                    <div class="panel-body">

                        <span style="float:left;">Sekcja:</span>
                        <span style="float:right;"><?php $this->Admin->getSection($order['section']); ?></span>

                        <div style="clear:both;"></div>

                        <span style="float:left;">Forma przesyłki:</span>
                        <span style="float:right;"><?php echo $order['delivery_type'] ?></span>

                        <div style="clear:both;"></div>

                        <span style="float:left;">Wartość produktów:</span>
                        <span style="float:right;"><?php echo $sum ?></span>

                        <div style="clear:both;"></div>

                        <span style="float:left;">Koszty wysyłki:</span>
                        <span style="float:right;"><?php echo $order['delivery_cost'] ?></span>

                        <div style="clear:both;"></div>

                        <span style="float:left;"><b>Suma:</b></span>
                        <span style="float:right;"><b><?php echo $sum + $order['delivery_cost'] ?></b></span>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class='row'>
    <div class='col-md-6 col-sm-12 col-xs-12'>
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">Dane do wysyłki</h3></div>
            <div class="panel-body">

                <span class="span"><?php echo $order['name']; ?></span></br>
            <?php
            if (!empty($order['street_no2'])) {
                echo '<span class="span">' . $order['street'] . ' ' . $order['street_no'] . '/' . $order['street_no2'] . '</span></br>';
            } else {
                echo '<span class="span">' . $order['street'] . ' ' . $order['street_no'] . '</span></br>';
            }
            ?>
                <span class="span"><?php echo $order['post'] . ' ' . $order['city']; ?></span></br>
                <span class="span"><?php echo $order['country']; ?></span>

            </div>
        </div>
    </div>

    <div class='col-md-6 col-sm-12 col-xs-12'>
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">Dane kontaktowe</h3></div>
            <div class="panel-body">

                <span class="span"><?php echo $order['name']; ?></span></br>
                <span class="span"><?php echo $order['email'] ?></span></br>
                <span class="span"><?php echo $order['phone']; ?></span>

            </div>
        </div>
    </div>
</div>
<div class='row'>

    <?php if (!empty($order['remarks'])): ?>

    <div class='col-md-6 col-sm-12 col-xs-12'>
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">Uwagi do zmówienia</h3></div>
            <div class="panel-body">

                <span class="span"><?php echo $order['remarks']; ?></span></br>


            </div>
        </div>
    </div>
    <?php endif; ?>   

    <?php if (count($orders) > 1): ?>

    <div class='col-md-6 col-sm-12 col-xs-12'>
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">Inne zamówienia</h3></div>
            <div class="panel-body">

                <?php
                $x = 0;
                foreach ($orders as $or):
                    $x++;
                    ?>
                    <?php
                    if ($or['id'] == $order->id) {
                        
                    } else {
                        echo '<span class="span">' . $this->Html->link($or['id'], '/orders/view/' . $or['id'], ['class' => 'button', 'target' => '_blank']) . ' / ' . $or['section'] . ' /  ' . str_replace('/', '-', $or['created']) . '</span></br>';
                    }
                    ?>
                <?php endforeach; ?>
            <?php endif; ?>
            </div>
        </div>
    </div>
    
    <?php if (!empty($order['paczkomat'])): ?>
    <div class='col-md-6 col-sm-12 col-xs-12'>
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">Paczkomat</h3></div>
            <div class="panel-body">

                <span class="span"><?php echo $order['paczkomat'] ?></span>
            </div>
        </div>
    </div>
    <?php endif; ?>  

    <?php if (!empty($order['comp_name'])): ?>
    <div class='col-md-6 col-sm-12 col-xs-12'>
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">Dane do faktury</h3></div>
            <div class="panel-body">

                <?php
                echo $order['comp_name'] . '<br/>NIP: ' . $order['comp_nip'] . '<br/>' . $order['comp_street'] . '<br/>' . $order['comp_post'] . ' ' . $order['comp_city'];
                ?>
            </div>
        </div>
    </div>
    <?php endif; ?>  


    <?php if (!empty($order['notes'])): ?>
    <div class='col-md-6 col-sm-12 col-xs-12'>
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">Notatki</h3></div>
            <div class="panel-body">
                <span class="span"><?php echo $order['notes'] ?></span>
            </div>
        </div>
    </div>
    <?php endif; ?>  

</div>