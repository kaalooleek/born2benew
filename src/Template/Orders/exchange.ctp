<style>
    .span{
        margin-left: 30px;
    }
    .action-button{
        font-weight: bold;
        color: #2ba6cb;
        margin-bottom: 10px;

    }
    .line{
        border:solid 2px;
        border-radius: 100px;
        margin-top:50px;
        color:#CFCFCF;
    }

</style>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">ZamĂłwienie numer: <?php echo $order->id ?></h3></div>
            <div class="panel-body">
                <table class="table" cellpadding = "10" cellspacing = "10" style="width:100%;">
                    <tr>
                        <th class="pull-right"><?php echo __('Produkt'); ?></th>
                        <th></th>
                        <th>Status</th>
                        <th><?php echo __('Cena'); ?></th>
                        <th>Akcja</th>
                        <th></th>
                    </tr>
                    <?php
                    $x = -1;
                    foreach ($order['oitems'] as $oitem):
                        $x++;
                        ?>
                        <tr style="height:200px;">
                            <td style="width:70px;margin-left:30px;"><?php echo $photo[$x] ?></td>
                            <td>
                                <span class="span"><?php echo $product[$x]['title']; ?></span></br>
                                <span class="span"><b>Rozmiar: </b><?php echo $size[$x]['size']; ?> (<?php echo $size[$x]['total'] + $size[$x]['debit']; ?>) </span></br>
                                <span class="span"><b>Kod: </b><?php echo $product[$x]['code']; ?></span>

                                <div id="params">
                                </div>
                            </td>
                            <td>
                                <span><?php echo $this->Admin->getOitemStatus($oitem['status']); ?></span>
                            </td>

                            <td>
                                <span><?php echo $oitem['price']; ?></span>
                            </td>
                            <td>

                                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> <span style="margin-top:10px;" class="action-button">Edytuj</span></br>
                                <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> <span class="action-button">PodmieĹ„</span></br>
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> <span class="action-button">Dodaj</span></br>
                                <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> <span class="action-button">UsuĹ„</span></br>

                            </td>
                            <td>
                                <input type="number" name="lname" id="rcode<?php echo $x; ?>" style="width:70px;;"></br></br>
                                <input type="number" name="lname" id="rsize<?php echo $x; ?>" style="width:60px;"></br></br>
                            </td>
                        </tr>
                        <?php
                    endforeach;
                    ?>    
                </table>

            </div>
        </div> 
    </div> 
</div> 
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title"> Utwórz wymianę </h3></div>
            <div class="panel-body">


                <?= $this->Form->create() ?>
                <fieldset>
                    <?php
                    echo $this->Form->input('send', ['label' => false, 'class' => 'form-control margin-bottom', 'options' => $options]);
                    echo $this->Form->input('email', ['label' => 'Email','class' => 'form-control margin-bottom', 'default' => $order['email']]);
                    echo $this->Form->input('name', ['label' => 'Nazwa','class' => 'form-control margin-bottom', 'default' => $order['name']]);
                    echo $this->Form->input('street', ['label' => 'Ulica','class' => 'form-control margin-bottom', 'default' => $order['street']]);
                    echo $this->Form->input('street_no', ['label' => 'Numer','class' => 'form-control margin-bottom', 'default' => $order['street_no']]);
                    echo $this->Form->input('street_no2', ['label' => 'Mieszkanie','class' => 'form-control margin-bottom', 'default' => $order['street_no2']]);
                    echo $this->Form->input('post', ['label' => 'Kod pocztowy','class' => 'form-control margin-bottom', 'default' => $order['post']]);
                    echo $this->Form->input('city', ['label' => 'Miasto','class' => 'form-control margin-bottom', 'default' => $order['city']]);
                    echo $this->Form->input('country', ['label' => 'Państwo','class' => 'form-control margin-bottom', 'default' => $order['country']]);
                    echo $this->Form->input('first_name', ['label' => 'Imię','class' => 'form-control margin-bottom', 'default' => $order['first_name']]);
                    echo $this->Form->input('second_name', ['label' => 'Nazwisko','class' => 'form-control margin-bottom', 'default' => $order['second_name']]);
                    echo $this->Form->input('phone', ['label' => 'Telefon','class' => 'form-control margin-bottom', 'default' => $order['phone']]);
                    echo $this->Form->input('courier_remarks', ['label' => 'Uwagi do kuriera','class' => 'form-control margin-bottom', 'default' => $order['courier_remarks']]);
                    echo $this->Form->input('camp_name', ['label' => 'FAKTURA / Nazwa','class' => 'form-control margin-bottom', 'default' => $order['comp_name']]);
                    echo $this->Form->input('camp_nip', ['label' => 'FAKTURA / NIP','class' => 'form-control margin-bottom', 'default' => $order['comp_nip']]);
                    echo $this->Form->input('camp_street', ['label' => 'FAKTURA / Adres firmy','class' => 'form-control margin-bottom', 'default' => $order['comp_street']]);
                    echo $this->Form->input('camp_post', ['label' => 'FAKTURA / Kod pocztowy','class' => 'form-control margin-bottom', 'default' => $order['comp_post']]);
                    echo $this->Form->input('camp_city', ['label' => 'FAKTURA / Miasto','class' => 'form-control margin-bottom', 'default' => $order['comp_city']]);
                    echo $this->Form->input('camp_country', ['label' => 'FAKTURA / Pańtwo','class' => 'form-control margin-bottom', 'default' => $order['comp_country']]);
                    echo $this->Form->input('delivery_cost', ['label' => 'Koszt wysyłki','class' => 'form-control margin-bottom', 'default' => $order['delivery_cost']]);
                    ?>
                </fieldset>
                <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-success spacer']) ?>
                <?= $this->Form->end() ?>



            </div>
        </div> 
    </div> 
</div> 