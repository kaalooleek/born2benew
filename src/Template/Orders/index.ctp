<div class="panel panel-primary" ng-controller="ordersCtrl">
    <div class="panel-heading">Zamówienia</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <th>ID</th>
                <th>Imię</th>
                <th>Nazwisko</th>
                <th>Złożone</th>
                <th>Kwota</th>
                <th>Przewoźnik</th>
                <th></th>
                </thead>
                <tbody>
                <?php foreach ($orders as $order): ?>
                <tr>
                    <td><?= $order->id ?></td>
                    <td><?= $order->first_name ?></td>
                    <td><?= $order->last_name ?></td>
                    <td><?= $order->created ?></td>
                    <td><?= $order->to_paid ?> PLN</td>
                    <td><?= $order->carrier['name'] ?> (<?= $order->carrier['payment_type'] ?>)</td>
                    <td>
                        <button class="btn btn-primary" ng-click="orderModal(<?= $order->id ?>)"><i
                                class="fa fa-eye"></i> Zobacz
                        </button>
                    </td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

    <div id="orderModal" class="modal modal-primary fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: auto;">
                <div class="modal-header"
                     style="background-color: white !important; color: #333 !important; border-bottom: 1px solid #e5e5e5 !important;">
                    <h4>Szczegóły zamówienia</h4>
                </div>
                <div class="modal-body" style="background-color: white !important; color: #333 !important;">
                    <div class="row">
                        <div class="col-md-6">
                            <p class="well">
                                <b>Imię:</b> {{ current_order.first_name }}<br>
                                <b>Nazwisko:</b> {{ current_order.last_name }}<br>
                                <b>Ulica:</b> {{ current_order.street }}<br>
                                <b>Numer:</b> {{ current_order.street_no }}<br>
                                <b>Numer 2:</b> {{ current_order.street_no_2 }}<br>
                                <b>Kod:</b> {{ current_order.zip }}<br>
                                <b>Miasto:</b> {{ current_order.city }}<br>
                                <b>Kraj:</b> {{ current_order.country }}<br>
                                <b>Telefon:</b> {{ current_order.phone }}
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p class="well">
                                <b>Nazwa:</b> {{ current_order.comp_name }}<br>
                                <b>NIP:</b> {{ current_order.comp_nip }}<br>
                                <b>Ulica:</b> {{ current_order.comp_street }}<br>
                                <b>Numer:</b> {{ current_order.comp_street_no }}<br>
                                <b>Numer 2:</b> {{ current_order.comp_street_no_2 }}<br>
                                <b>Kod:</b> {{ current_order.comp_zip }}<br>
                                <b>Miasto:</b> {{ current_order.comp_city }}<br>
                                <b>Kraj:</b> {{ current_order.comp_country }}
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="well">
                                <b>Waluta:</b> {{ current_order.currency }}<br>
                                <b>Do zapłacenia:</b> {{ current_order.to_paid }}<br>
                                <b>Zapłacono:</b> {{ current_order.paid_amount }}<br>
                                <b>ID Dostawcy:</b> {{ current_order.carrier_id }}<br>
                                <b>Utworzono:</b> {{ current_order.created }}<br>
                                <b>Modyfikowano:</b> {{ current_order.modified }}<br>
                                <b>Paczkomat:</b> {{ current_order.paczkomat }}<br>
                                <b>Notatki:</b> {{ current_order.notes }}<br>
                                <b>TrackAndTrace:</b> {{ current_order.track_and_trace }}<br>
                                <b>Sprawdzono:</b> {{ current_order.checked }}<br>
                                <b>Wstrzymano:</b> {{ current_order.hold }}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"
                     style="background-color: white !important; color: #333 !important; border-top: 1px solid #e5e5e5 !important;">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Zamknij</button>
                </div>
            </div>
        </div>
    </div>
</div>