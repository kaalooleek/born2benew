<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Orders'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Websites'), ['controller' => 'Websites', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Website'), ['controller' => 'Websites', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Codes'), ['controller' => 'Codes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Code'), ['controller' => 'Codes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Invoices'), ['controller' => 'Invoices', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Invoice'), ['controller' => 'Invoices', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Oitems'), ['controller' => 'Oitems', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Oitem'), ['controller' => 'Oitems', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Transfers'), ['controller' => 'Transfers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Transfer'), ['controller' => 'Transfers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="orders form large-9 medium-8 columns content">
    <?= $this->Form->create($order) ?>
    <fieldset>
        <legend><?= __('Add Order') ?></legend>
        <?php
            echo $this->Form->input('user_id', ['options' => $users]);
            echo $this->Form->input('order_id');
            echo $this->Form->input('website_id', ['options' => $websites]);
            echo $this->Form->input('shelf');
            echo $this->Form->input('oldshelf');
            echo $this->Form->input('email');
            echo $this->Form->input('name');
            echo $this->Form->input('street');
            echo $this->Form->input('street_no');
            echo $this->Form->input('street_no2');
            echo $this->Form->input('post');
            echo $this->Form->input('city');
            echo $this->Form->input('country');
            echo $this->Form->input('first_name');
            echo $this->Form->input('second_name');
            echo $this->Form->input('phone');
            echo $this->Form->input('comp_name');
            echo $this->Form->input('comp_nip');
            echo $this->Form->input('comp_street');
            echo $this->Form->input('comp_post');
            echo $this->Form->input('comp_city');
            echo $this->Form->input('comp_country');
            echo $this->Form->input('status');
            echo $this->Form->input('currency');
            echo $this->Form->input('is_notified');
            echo $this->Form->input('section');
            echo $this->Form->input('payment_type');
            echo $this->Form->input('delivery_type');
            echo $this->Form->input('delivery_cost');
            echo $this->Form->input('carrier');
            echo $this->Form->input('invoice_printed');
            echo $this->Form->input('remarks');
            echo $this->Form->input('courier_remarks');
            echo $this->Form->input('paczkomat');
            echo $this->Form->input('notes');
            echo $this->Form->input('trackandtrace');
            echo $this->Form->input('transfers._ids', ['options' => $transfers]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
