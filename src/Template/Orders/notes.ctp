<div class="row" ng-controller="ordersCtrl">
    <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">Zamówienie numer: <?php echo $order->id ?></h3></div>
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td><strong>Imię i Nazwisko:</strong></td>
                        <td><?php echo $order->name ?></td>
                    </tr>
                    <tr>
                        <td><strong>Email:</strong></td>
                        <td><?php echo $order->email ?></td>
                    </tr>
                    <tr>
                        <td><strong>Numer telefonu:</strong></td>
                        <td><?php echo $order->phone ?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">Obecna notatka</h3></div>
            <div class="panel-body">
                <p id="note_p"><?php if($order->notes == null) {echo "Brak notatek";} else { echo $order->notes; } ?></p>
            </div>
        </div>
    </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h3 class="panel-title">Edytuj notatkę:</h3></div>
            <div class="panel-body">
                    <textarea style="width:100%;" rows="5" id="note_area"><?php echo $order->notes; ?></textarea>
                    <button class="btn btn-primary pull-right" ng-click="zapisz(<?= $order->id;?>)">Zapisz</button>
            </div>
        </div>
    </div>
</div>