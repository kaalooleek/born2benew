<style>
    input:focus:invalid,
    textarea:focus:invalid {
        border: solid 2px #F5192F;
    }

    input:focus:valid,
    textarea:focus:valid {
        border: solid 2px #18E109;
        background-color: #fff;
    }
</style>
<div class="panel panel-primary" ng-controller="notificationsCtrl">
    <div class="panel-heading ">Powiadomienia</div>
    <div class="panel-body">
        <div class="row">
            <form name="notification_form">
                <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="Treść powiadomienia" name="text" ng-model="notification.text" required/>
                </div>
                <div class="col-md-2">
                    <select class="form-control" id="groups_select" name="groups_select" ng-model="notification.group_id" ng-options="group as group.name for group in groups" required>
                        <option value="" ng-selected>Wybierz odbiorców</option>
                    </select>
                </div>
                <div class="col-md-1">
                    <button class="pull-right btn btn-primary" ng-disabled="notification_form.$invalid"
                            ng-click="add_notification()">Dodaj
                    </button>
                </div>
            </form>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="table-responsive box-body no-padding">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="1%">ID</th>
                                <th width="62%">Treść</th>
                                <th>Data</th>
                                <th>Odbiorcy</th>
                                <th>Nadawca</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="notification_list">
                            <?php foreach ($notifications as $notification): ?>
                            <tr id="<?= $notification->id ?>">
                                <td><?= h($notification->id) ?></td>
                                <td><?= h($notification->text) ?></td>
                                <td><?= h($notification->date) ?></td>
                                <td><?= h($notification->group['name']) ?></td>
                                <td><?= h($notification->worker['first_name'])."
                                    ".h($notification->worker['last_name'])?>
                                </td>
                                <td><span><i class="fa fa-remove" ng-click="remove_notification(<?= $notification->id ?>)"></i></span></td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="paginator">
                            <ul class="pagination pull-right">
                                <?= $this->Paginator->prev('< ' . __('')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('') . ' >') ?>
                            </ul>
                        </div>
                    </div>
                    <?= $this->Paginator->counter() ?>
                </div>
            </div>
        </div>
    </div>
