<section class="invoice" id="fakturyCtrl" ng-controller="fakturyCtrl">
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                <i class="fa fa-globe"></i> Killersites
                <small class="pull-right"><?= $invoice->invoice_date; ?></small>
            </h2>
        </div>
        <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            Sprzedawca
            <address>
                <strong>Killersites Artur Bogucki</strong><br>
                Starowiejska 271<br>
                08-110 Siedlce<br>
                artur.j.bogucki@gmail.com<br>
                +48 22 113 70 70
            </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            Nabywca
            <address>
                <span id="name" data-toggle="tooltip" title="Dwukrotne kliknięcie - edytuje pole"><?= $invoice->name ?></span>
                <div class="form-group input-group" id="name_group" style="display: none;">
                    <span class="input-group-addon" onclick="name = 'name'; angular.element('#fakturyCtrl').scope().save_input(<?= $invoice->id ?>, name, $('#' + name + '_input').val());angular.element('#fakturyCtrl').scope().$apply();"><i class="glyphicon glyphicon-save"></i></span>
                    <input type="text" id="name_input" class="form-control">
                </div><br>
                <span id="address" title="Dwukrotne kliknięcie - edytuje pole"><?= $invoice->address ?></span>
                <div class="form-group input-group" id="address_group" style="display: none;">
                    <span class="input-group-addon" onclick="name = 'address'; angular.element('#fakturyCtrl').scope().save_input(<?= $invoice->id ?>, name, $('#' + name + '_input').val());angular.element('#fakturyCtrl').scope().$apply();"><i class="glyphicon glyphicon-save"></i></span>
                    <input type="text" id="address_input" class="form-control">
                </div><br>
                <span id="town" title="Dwukrotne kliknięcie - edytuje pole"><?= $invoice->town ?></span>
                <div class="form-group input-group" id="town_group" style="display: none;">
                    <span class="input-group-addon" onclick="name = 'town'; angular.element('#fakturyCtrl').scope().save_input(<?= $invoice->id ?>, name, $('#' + name + '_input').val());angular.element('#fakturyCtrl').scope().$apply();"><i class="glyphicon glyphicon-save"></i></span>
                    <input type="text" id="town_input" class="form-control">
                </div><br>
                <span id="phone" title="Dwukrotne kliknięcie - edytuje pole"><?= $invoice->phone ?></span>
                <div class="form-group input-group" id="phone_group" style="display: none;">
                    <span class="input-group-addon" onclick="name = 'phone'; angular.element('#fakturyCtrl').scope().save_input(<?= $invoice->id ?>, name, $('#' + name + '_input').val());angular.element('#fakturyCtrl').scope().$apply();"><i class="glyphicon glyphicon-save"></i></span>
                    <input type="text" id="phone_input" class="form-control">
                </div><br>
                <span id="email" title="Dwukrotne kliknięcie - edytuje pole"><?= $invoice->email ?></span>
                <div class="form-group input-group" id="email_group" style="display: none;">
                    <span class="input-group-addon" onclick="name = 'email'; angular.element('#fakturyCtrl').scope().save_input(<?= $invoice->id ?>, name, $('#' + name + '_input').val());angular.element('#fakturyCtrl').scope().$apply();"><i class="glyphicon glyphicon-save"></i></span>
                    <input type="text" id="email_input" class="form-control">
                </div><br>
            </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            <b>Faktura <?= $invoice->numer ?></b><br>
            <br>
            <b>ID Zamówienia:</b> <?= $invoice->id ?><br>
            <b>Termin płatności:</b> <?= $invoice->payment_term ?><br>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Nazwa</th>
                        <th>Ilość</th>
                        <th>JM</th>
                        <th>Cena</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($invoice->items as $item): ?>
                        <tr>
                            <td><?= $item->name ?></td>
                            <td><?= $item->quantity ?></td>
                            <td><?= $item->jm ?></td>
                            <td><?php echo $item->price . " " . $item->currency ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
            <p class="lead">Metoda płatności:</p>
            <h3 id="payment_type" title="Dwukrotne kliknięcie - edytuje pole"><?php echo $invoice->payment_type ?></h3>
            <div class="form-group input-group" id="payment_type_group" style="display: none;">
                <span class="input-group-addon" onclick="name = 'payment_type'; angular.element('#fakturyCtrl').scope().save_input(<?= $invoice->id ?>, name, $('#' + name + '_input').val());angular.element('#fakturyCtrl').scope().$apply();"><i class="glyphicon glyphicon-save"></i></span>
                <input type="text" id="payment_type_input" class="form-control">
            </div>
        </div>
        <!-- /.col -->
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- this row will not appear when printing -->
    <div class="row no-print">
        <div class="col-xs-12">
            <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Drukuj</a>
            <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
                <i class="fa fa-download"></i> Generuj PDF
            </button>
        </div>
    </div>

    <?= $this->Html->script('jquery-2.2.4.min'); ?>
    <script>
                $(document).ready(function () {
                    toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-bottom-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "3000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }
                    var inputs = ['name', 'address', 'town', 'phone', 'email', 'payment_type'];
                    for (i = 0; i < inputs.length; i++) {
                        (function (i) {
                            var name = inputs[i];
                            $("#" + name).dblclick(function () {
                                $("#" + name).fadeToggle('fast', "linear");
                                $("#" + name + "_group").fadeToggle('fast', "linear");
                                var value = $('#' + name).text();
                                $("#" + name + "_input").val(value);
                                $("body").on('click', function (e) {
                                    if (!$('#' + name + '_input').is(e.target)) {
                                        var new_value = $("#" + name + "_input").val();
                                        $('#' + name + '_group').fadeToggle('fast', "linear");
                                        $("#" + name).fadeToggle('fast', "linear");
                                        $("body").off("click");
                                    }
                                });
                            });
                        })(i);
                    }

                    function show_success() {
                        toastr.success('Zapisano pomyślnie!');
                    }
                    function show_error() {
                        toastr.error('Wystąpił błąd w zapisywaniu!');
                    }
                });
    </script>
</section>
