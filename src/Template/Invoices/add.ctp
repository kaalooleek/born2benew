
<style>
    input:focus:invalid,
    textarea:focus:invalid{
        border:solid 2px #F5192F;
    }

    input:focus:valid,
    textarea:focus:valid{
        border:solid 2px #18E109;
        background-color:#fff;
    }
</style>
<form name="faktura_form">   
    <div class="col-md-12 jumbotron" ng-controller="fakturyCtrl">
        <div class="row">
            <div class="col-md-6"><div class="col-md-3"><h4><b>Faktura VAT nr </b></h4></div><div class="col-md-5"><input type="text" class="form-control" ng-model="faktura.typ" placeholder="Seria" required></div><div class="col-md-4"><input type="text" class="form-control" ng-model="faktura.numer" placeholder="Numer (nr/RRRR)" required></div></div>
            <div class="col-md-6">
                <div class="row"><div class="col-md-12"><div class="col-md-1"><span class="glyphicon glyphicon-calendar"></span></div><div class="col-md-4"><input type="text" class="form-control datepicker" id="data_wystawienia" ng-model="faktura.data_wystawienia" placeholder="Data wystawienia"></div></div></div>
                <div class="row"><div class="col-md-12"><div class="col-md-1"><span class="glyphicon glyphicon-calendar"></span></div><div class="col-md-4"><input type="text" class="form-control datepicker" id="data_sprzedazy" ng-model="faktura.data_sprzedazy" placeholder="Data Sprzedaży" required></div></div></div>
                <div class="row"><div class="col-md-12"><div class="col-md-1"><span class="glyphicon glyphicon-home"></span></div><div class="col-md-4"><input type="text" class="form-control" id="miejsce_wystawienia" ng-model="faktura.miejsce_wystawienia" placeholder="Miejsce wystawienia" disabled></div></div></div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-6">
                <h5 ng-click="generuj();">Sprzedawca</h5>
                <hr class="dark">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label" for="nazwa">Nazwa</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control" ng-model="faktura.nazwa_sprzedawcy" placeholder="Nazwa" disabled>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label" for="adres">Adres</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control" ng-model="faktura.adres_sprzedawcy" placeholder="Adres" disabled>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label" for="kod_pocztowy">Kod Pocztowy</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control" ng-model="faktura.kod_pocztowy_sprzedawcy" placeholder="Kod Pocztowy" disabled>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label" for="nip">NIP</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control" ng-model="faktura.nip_sprzedawcy" placeholder="NIP" disabled>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label" for="email">E-Mail</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control" ng-model="faktura.email_sprzedawcy" placeholder="E-Mail" disabled>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label" for="telefon">Telefon</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control" ng-model="faktura.telefon_sprzedawcy" placeholder="Telefon" disabled>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label" for="fax">Fax</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control" ng-model="faktura.fax_sprzedawcy" placeholder="Fax" disabled>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h5>Nabywca</h5>
                <hr class="dark">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label" for="nazwa">Nazwa</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" id="nazwa_nabywcy" class="form-control" ng-model="faktura.name" placeholder="Nazwa" required ng-pattern-restrict pattern="[a-Z]{3,35}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label" for="adres">Adres</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control" ng-model="faktura.address" placeholder="Adres" required maxlength="35">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label" for="adres">Miasto</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control" ng-model="faktura.town" placeholder="Miasto" required maxlength="35">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label" for="kod_pocztowy">Kod Pocztowy</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control" ng-model="faktura.zip" placeholder="Kod Pocztowy" required ng-pattern-restrict pattern="[0-9]{2}-[0-9]{3}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label" for="nip">NIP</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" id="nip_nabywcy" class="form-control" data="{{faktura.nip}}" validnip ng-model="faktura.nip" placeholder="NIP" required ng-pattern-restrict pattern="[0-9]{10}">
                        <span class="help-block" ng-show="faktura.nip_nabywcy.$error.required">NIP jest nie poprawny</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label" for="pesel">PESEL</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control" ng-model="faktura.pesel" placeholder="PESEL" required ng-pattern-restrict pattern="[0-9]{11}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label" for="email">E-Mail</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control" ng-model="faktura.email" placeholder="E-Mail" required ng-pattern-restrict pattern="[a-z]+[a-z0-9._]+@[a-z].[a-z.]{2,8888}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label" for="telefon">Telefon</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control" ng-model="faktura.telefon" placeholder="Telefon" required ng-pattern-restrict pattern="[0-9]{9}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label" for="fax">Fax</label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control" ng-model="faktura.fax" placeholder="Fax">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <hr class="dark">
            <div class="row">
                <div class="col-md-4">
                    <div class="col-md-12">
                        <label for="rodzaj_rabatu">Rodzaj rabatu</label>
                        <select class="form-control" id="rodzaj_rabatu" ng-model="faktura.rodzaj_rabatu">
                            <option value="{{faktura.waluta}}" label="Kwotowy">Kwotowy</option>
                            <option value="%" label="Procentowy"Procentowy</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <label for="waluta">Waluta</label>
                    <select class="form-control" id="waluta" ng-model="faktura.waluta">
                        <option value="PLN" label="PLN">PLN</option>
                        <option value="EUR" label="EUR">EUR</option>
                        <option value="USD" label="USD">USD</option>
                        <option value="GBP" label="GBP">GBP</option>
                        <option value="CHF" label="CHF">CHF</option>
                        <option value="AUD" label="AUD">AUD</option>
                        <option value="CZK" label="CZK">CZK</option>
                        <option value="DKK" label="DKK">DKK</option>
                        <option value="EEK" label="EEK">EEK</option>
                        <option value="HKD" label="HKD">HKD</option>
                        <option value="JPY" label="JPY">JPY</option>
                        <option value="CAD" label="CAD">CAD</option>
                        <option value="LTL" label="LTL">LTL</option>
                        <option value="LVL" label="LVL">LVL</option>
                        <option value="NOK" label="NOK">NOK</option>
                        <option value="RUB" label="RUB">RUB</option>
                        <option value="ZAR" label="ZAR">ZAR</option>
                        <option value="SEK" label="SEK">SEK</option>
                        <option value="UAH" label="UAH">UAH</option>
                        <option value="HUF" label="HUF">HUF</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-12">

                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Lp</th>
                                    <th>Nazwa towaru</th>
                                    <th>Rozmiar</th>
                                    <th>JM</th>
                                    <th>Ilość</th>
                                    <th>Cena netto</th>
                                    <th>Rabat</th>
                                    <th>Stawka VAT</th>
                                    <th>Wartość vat</th>
                                    <th>Wartość netto</th>
                                    <th>Wartość brutto</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="x in towary">
                                    <td>{{x.lp}}</td>
                                    <td><input type="text" class="form-control" id="{{x.lp}}" ng-model="x.nazwa" name="{{x.lp}}"></td>
                                    <td><input type="text" class="form-control" id="{{x.lp}}size" ng-model="x.size" name="{{x.lp}}size"></td>
                                    <td><input type="text" class="form-control" ng-model="x.jm"></td>
                                    <td><input type="text" class="form-control" ng-model="x.ilosc" placeholder="{{x.jm}}"></td>
                                    <td><input type="text" class="form-control" ng-model="x.cena_netto" placeholder="{{faktura.waluta}}"></td>
                                    <td ng-show="faktura.rodzaj_rabatu == faktura.waluta"><input type="text" class="form-control" ng-model="x.rabat" placeholder="{{faktura.waluta}}"></td>
                                    <td ng-show="faktura.rodzaj_rabatu == '%'"><input type="text" class="form-control" ng-model="x.rabat" placeholder="%"></td>
                                    <td>
                                        <select ng-model="x.podatek_vat" class="form-control">
                                            <option value="" label="-" selected="selected">-</option>
                                            <option value="0.23" label="23 %">23 %</option>
                                            <option value="0.08" label="8 %">8 %</option>
                                            <option value="0.05" label="5 %">5 %</option>
                                            <option value="0" label="0 %">0 %</option>
                                        </select>
                                    </td>
                                    <td>{{x.wartosc_vat = (x.wartosc_netto * x.podatek_vat) | number:2}} {{faktura.waluta}}</td>

                                    <td ng-if="faktura.rodzaj_rabatu == '%'">{{x.wartosc_netto = (x.ilosc * (x.cena_netto * (1 - x.rabat / 100))) | number:2}} {{faktura.waluta}}</td>
                                    <td ng-if="faktura.rodzaj_rabatu == faktura.waluta">{{x.wartosc_netto = (x.ilosc * (x.cena_netto - x.rabat)) | number:2}} {{faktura.waluta}}</td>
                                    <td>{{x.wartosc_brutto = ((x.wartosc_netto * 1) + (x.wartosc_vat * 1)) | number
                                                    :2}}  {{faktura.waluta}}</td>
                                    <td><span class="btn glyphicon glyphicon-remove" data="{{x.lp}}" ng-click="remove_row(x.lp)" ng-show="x.lp != '1'"></span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <button class="btn btn-primary" style="margin-left: 15px;" ng-click="add_product()">Dodaj pozycję</button>
                </div>
            </div>
        </div>
        <hr class="dark">
        <div class="row">
            <div class="col-md-2">
                <div class="row">
                    <div class="col-md-12">
                        <label for="forma_zaplaty">Forma zapłaty</label>
                        <select ng-model="faktura.forma_zaplaty" ng-init="faktura.forma_zaplaty = 'za pobraniem'" id='forma_zaplaty' class="form-control">
                            <option value="za pobraniem" label="Za Pobraniem" selected="selected"> Za Pobraniem </option>
                            <option value="przelew" label="Przelew"> Przelew </option>
                        </select>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="termin_zaplaty">Termin zapłaty</label>
                        <input type="text" class="form-control datepicker" id="termin_zaplaty" ng-model="faktura.termin_zaplaty" placeholder="Termin zapłaty">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="termin_zaplaty">Zapłacono</label>
                        <select ng-model="faktura.status" ng-init="faktura.status = '0'" class="form-control">
                            <option value="0" label="Nie" selected="selected">Nie</option>
                            <option value="paid" label="Tak">Tak</option>
                        </select>
                    </div>
                </div>

            </div>
            <div class="col-md-4">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Wartość Netto</th>
                                <th>Wartość VAT</th>
                                <th>Wartość Brutto</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Razem</td>
                                <td>{{faktura.suma_netto| number:2}} {{faktura.waluta}}</td>
                                <td>{{faktura.suma_vat| number:2}} {{faktura.waluta}}</td>
                                <td>{{faktura.suma_brutto| number:2}} {{faktura.waluta}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Podsumowanie</div>
                        <div class="panel-body">
                            <h5><b>Razem: {{faktura.suma_brutto| number:2}} {{faktura.waluta}}</b></h5>
                            <h5><b>Do zapłaty: {{faktura.do_zaplaty = (faktura.suma_brutto - faktura.zaliczka_otrzymana) | number:2}} {{faktura.waluta}}</b></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" ng-show="faktura.forma_zaplaty == 'przelew'">
            <hr class="dark">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <label for="zaliczka_otrzymana">Nazwa banku</label>
                        <input type="text" class="form-control" id="nazwa_banku" ng-model="faktura.nazwa_banku" ng-init=" " placeholder="Nazwa banku">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="zaliczka_otrzymana">Numer konta</label>
                        <input type="text" class="form-control" id="numer_konta" ng-model="faktura.numer_konta" ng-init=" " placeholder="Numer konta">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="zaliczka_otrzymana">BIC/SWIFT</label>
                        <input type="text" class="form-control" id="bic_swift" ng-model="faktura.bic_swift" ng-init=" " placeholder="BIC/SWIFT">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="zaliczka_otrzymana">Uwagi</label>
                        <input type="text" class="form-control" id="przelew_uwagi" ng-model="faktura.przelew_uwagi" ng-init="" placeholder="Uwagi">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <hr class="dark">
            <button type="submit" class="btn btn-primary pull-right" style="margin-right: 15px;" ng-click="wystaw()" ng-disabled="faktura_form.$invalid">Wystaw</button>
        </div>
    </div>
</form>
