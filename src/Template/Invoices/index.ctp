<div class="col-md-12" ng-controller="fakturyCtrl">
    <div class="panel panel-primary">
        <div class="panel-heading ">Faktury</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <input type="text" class="form-control" id="invoice_search"
                           placeholder="Wyszukaj fakturę (nr, nazwa, adres, kod-pocztowy itp.)">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="table-responsive box-body no-padding">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th><?= $this->Paginator->sort('id', 'ID') ?></th>
                                    <th><?= $this->Paginator->sort('numer', 'Numer') ?></th>
                                    <th><?= $this->Paginator->sort('name', 'Nazwa klienta') ?></th>
                                    <th><?= $this->Paginator->sort('address', 'Adres') ?></th>
                                    <th><?= $this->Paginator->sort('zip', 'Kod pocztowy') ?></th>
                                    <th><?= $this->Paginator->sort('town', 'Miejscowość') ?></th>
                                    <th><?= $this->Paginator->sort('nip', 'NIP') ?></th>
                                    <th><?= $this->Paginator->sort('invoice_date', 'Data wystawienia') ?></th>
                                    <th><?= $this->Paginator->sort('payment_type', 'Typ płatności') ?></th>
                                    <th><?= $this->Paginator->sort('status', 'Status') ?></th>
                                    <th class="actions"><?= __('Akcje') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($invoices as $invoice): ?>
                                <tr>
                                    <td><?= $this->Number->format($invoice->id) ?></td>
                                    <td><?= h($invoice->numer) ?></td>
                                    <td id="name_<?=$invoice->id?>"><?= h($invoice->name) ?></td>
                                    <td id="address_<?=$invoice->id?>"><?= h($invoice->address) ?></td>
                                    <td id="zip_<?=$invoice->id?>"><?= h($invoice->zip) ?></td>
                                    <td id="town_<?=$invoice->id?>"><?= h($invoice->town) ?></td>
                                    <td><?= h($invoice->nip) ?></td>
                                    <td><?= h($invoice->invoice_date) ?></td>
                                    <td><?= h($invoice->payment_type) ?></td>
                                    <td><?php echo $invoice->status ?></td>
                                    <td>
                                        <div style="width: 100px;">
                                            <div class="btn-group">
                                                <button class="btn btn-primary"
                                                        ng-click="show_invoice(<?= $invoice->id ?>)">Zobacz
                                                </button>
                                                <button type="button" class="btn btn-primary dropdown-toggle"
                                                        data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li>
                                                        <a href="#" style="margin-right: 5px;" ng-click="generate_pdf(<?= $invoice->id ?>)">
                                                            <i class="fa fa-download"></i> Generuj PDF
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="paginator">
                            <ul class="pagination pull-right">
                                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('next') . ' >') ?>
                            </ul>
                            <p><?= $this->Paginator->counter() ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-primary in" id="invoice_modal" ng-model="invoice_modal">
        <div class="modal-dialog">
            <div class="modal-content" style="padding:30px !important">
                <!-- title row -->
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="page-header">
                            <i class="fa fa-globe"></i> Killersites
                            <small class="pull-right">{{invoice.invoice_date}}</small>
                        </h2>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- info row -->
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                        Sprzedawca
                        <address>
                            <strong>Killersites Artur Bogucki</strong><br>
                            Starowiejska 271<br>
                            08-110 Siedlce<br>
                            artur.j.bogucki@gmail.com<br>
                            +48 22 113 70 70
                        </address>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice">
                        Nabywca
                        <address>
                            <div style="margin:2px;min-width:50px;min-height:17px;" id="name" data-toggle="tooltip" title="Dwukrotne kliknięcie - edytuje pole">{{invoice.name}}</div>
                            <div class="form-group input-group" id="name_group" style="display: none;">
                            <span class="input-group-addon"
                                  ng-click="save_input(invoice.id, 'name', name_input);"><i
                                    class="glyphicon glyphicon-save"></i></span>
                                <input type="text" id="name_input" ng-model="name_input" class="form-control">
                            </div>
                            <div style="margin:2px;min-width:50px;min-height:17px;" id="address" title="Dwukrotne kliknięcie - edytuje pole">{{invoice.address}}</div>
                            <div class="form-group input-group" id="address_group" style="display: none;">
                            <span class="input-group-addon"
                                  ng-click="save_input(invoice.id, 'address', address_input);"><i
                                    class="glyphicon glyphicon-save"></i></span>
                                <input type="text" id="address_input" ng-model="address_input" class="form-control">
                            </div>
                            <div style="margin:2px;min-width:50px;min-height:17px;" id="zip" title="Dwukrotne kliknięcie - edytuje pole">{{invoice.zip}}</div>
                            <div class="form-group input-group" id="zip_group" style="display: none;">
                            <span class="input-group-addon"
                                  ng-click="save_input(invoice.id, 'zip', zip_input);"><i
                                    class="glyphicon glyphicon-save"></i></span>
                                <input type="text" id="zip_input" ng-model="zip_input" class="form-control">
                            </div>
                            <div style="margin:2px;min-width:50px;min-height:17px;" id="town" title="Dwukrotne kliknięcie - edytuje pole">{{invoice.town}}</div>
                            <div class="form-group input-group" id="town_group" style="display: none;">
                            <span class="input-group-addon"
                                  ng-click="save_input(invoice.id, 'town', town_input);"><i
                                    class="glyphicon glyphicon-save"></i></span>
                                <input type="text" id="town_input" ng-model="town_input" class="form-control">
                            </div>
                            <div style="margin:2px;min-width:50px;min-height:17px;" id="phone" title="Dwukrotne kliknięcie - edytuje pole">{{invoice.telefon}}</div>
                            <div class="form-group input-group" id="phone_group" style="display: none;">
                            <span class="input-group-addon"
                                  ng-click="save_input(invoice.id, 'phone', phone_input);"><i
                                    class="glyphicon glyphicon-save"></i></span>
                                <input type="text" id="phone_input" ng-model="phone_input" class="form-control">
                            </div>
                            <div style="margin:2px;min-width:50px;min-height:17px;" id="email" title="Dwukrotne kliknięcie - edytuje pole">{{invoice.email}}</div>
                            <div class="form-group input-group" id="email_group" style="display: none;">
                            <span class="input-group-addon"
                                  ng-click="save_input(invoice.id, 'email', email_input);"><i
                                    class="glyphicon glyphicon-save"></i></span>
                                <input type="text" id="email_input" ng-model="email_input" class="form-control">
                            </div>
                        </address>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col">
                        <b>Faktura {{invoice.numer}}</b><br>
                        <br>
                        <b>ID Zamówienia:</b> {{invoice.id}}<br>
                        <b>Termin płatności:</b> {{invoice.payment_term}} <br>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <!-- Table row -->
                <div class="row">
                    <div class="col-xs-12 table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Nazwa</th>
                                <th>Ilość</th>
                                <th>JM</th>
                                <th>Cena</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="x in invoice.items">
                                <td>{{x.name}}</td>
                                <td>{{x.quantity}}</td>
                                <td>{{x.jm}}</td>
                                <td>{{x.price}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <div class="row">
                    <!-- accepted payments column -->
                    <div class="col-xs-6">
                        <p class="lead">Metoda płatności:</p>
                        <h3 style="min-width: 200px;min-height: 20px;" id="payment_type" title="Dwukrotne kliknięcie - edytuje pole">{{invoice.payment_type}}</h3>
                        <div class="form-group input-group" id="payment_type_group" style="display: none;">
                        <span class="input-group-addon"
                              ng-click="save_input(invoice.id, 'payment_type', payment_type_input);"><i
                                class="glyphicon glyphicon-save"></i></span>
                            <input type="text" id="payment_type_input" ng-model="payment_type_input" class="form-control">
                        </div>
                    </div>
                    <!-- /.col -->
                    <!-- /.col -->
                </div>
                <!-- /.row -->

                <!-- this row will not appear when printing -->
                <div class="row no-print">
                    <div class="col-xs-12">
                        <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;" ng-data="{{invoice.id}}" ng-click="generate_pdf(invoice.id)">
                            <i class="fa fa-download"></i> Generuj PDF
                        </button>
                    </div>
                </div>
                <?= $this->Html->script('jquery-2.2.4.min'); ?>
                <script>
                    $(document).ready(function () {
                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-bottom-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "3000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };
                        var inputs = ['name', 'address', 'town', 'phone', 'email', 'payment_type', 'zip'];
                        for (i = 0; i < inputs.length; i++) {
                            (function (i) {
                                var name = inputs[i];
                                $("#" + name).dblclick(function () {
                                    $("#" + name).fadeToggle('fast', "linear");
                                    $("#" + name + "_group").fadeToggle('fast', "linear");
                                    var value = $('#' + name).text();
                                    $("#" + name + "_input").val(value);
                                    $("body").on('click', function (e) {
                                        if (!$('#' + name + '_input').is(e.target)) {
                                            var new_value = $("#" + name + "_input").val();
                                            $('#' + name + '_group').fadeToggle('fast', "linear");
                                            $("#" + name).fadeToggle('fast', "linear");
                                            $("body").off("click");
                                        }
                                    });
                                });
                            })(i);
                        }


                        function show_success() {
                            toastr.success('Zapisano pomyślnie!');
                        }

                        function show_error() {
                            toastr.error('Wystąpił błąd w zapisywaniu!');
                        }
                    });
                </script>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
